package ru.webmechanic.moneycare.ui;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ru.webmechanic.moneycare.LocationManager;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.content.City;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.database.realm.DBHelper;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.loaders.LoaderContext;
import ru.webmechanic.moneycare.ui.utils.Arguments;
import ru.webmechanic.moneycare.ui.utils.FragmentsStack;
import ru.webmechanic.moneycare.ui.utils.NavigationDrawerStateManager;


public class MainActivity extends AppCompatActivity implements Button.OnClickListener,
        BaseFragment.OnLoaderFragmentFinishedListener,
        AdapterView.OnItemClickListener,
        DataFetcher.OnDataFetchedListener {

    public final static String TAGS = "TAGS";
    private FragmentFactory fragmentFactory;

    private NavigationDrawerStateManager navigationDrawerStateManager;
    private ImageButton navigationDrawerControlButton;
    private Button selectCityButton;
    private TextView selectCityTextView;
    private FragmentsStack history;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DBHelper.init(this);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setDisplayHomeAsUpEnabled(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            actionBar.setBackgroundDrawable(new ColorDrawable(getColor(R.color.actionBarBackground)));
        }else{
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionBarBackground)));
        }
        actionBar.setHomeButtonEnabled(false);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.action_bar_layout, null);

        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT);
        actionBar.setCustomView(v, layoutParams);
        ((Toolbar) v.getParent()).setContentInsetsAbsolute(0, 0);
        navigationDrawerControlButton = (ImageButton) v.findViewById(R.id.navigationDrawerControlButton);
        navigationDrawerControlButton.setOnClickListener(this);
        selectCityButton = (Button) v.findViewById(R.id.selectCityButton);
        selectCityButton.setOnClickListener(this);
        selectCityTextView = (TextView) v.findViewById(R.id.selectCityTextView);

        fragmentFactory = new FragmentFactory(this);

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ListView listView = (ListView) findViewById(R.id.left_drawer);

        navigationDrawerStateManager = new NavigationDrawerStateManager(this, drawerLayout, listView);
        navigationDrawerStateManager.changeState(Settings.getInstance(this).getUserState());
        listView.setOnItemClickListener(this);

        history = new FragmentsStack(getFragmentManager(), this);
        if (savedInstanceState == null) {
            if (!history.empty()) {
                history.showLast();
            } else {
                replaceFragment(FragmentFactory.MAIN_MENU, new Bundle());
            }
            updateUI();
        } else {

            //android for us recreate fragments but we have to find all of its in fragment manager and make it invisible all but last one
            ArrayList<String> tags;
            if (savedInstanceState.containsKey(TAGS)) {
                tags = savedInstanceState.getStringArrayList(TAGS);
                history.restoreByTags(tags);
            }


        }

        checkGeo();
    }

    /**
     * Запуск сервиса геолокации в случае необходимости
     */
    private void checkGeo() {
        final Settings settings = Settings.getInstance(this);
        if (settings.getCurrentCity() == null) {
            LocationManager locationManager = new LocationManager(this);

            locationManager.listen(new LocationManager.LocationChangeListener() {
                @Override
                public void onLocationReceived(Location location) {
                    settings.setLatitude(Double.toString(location.getLatitude()));
                    settings.setLongitude(Double.toString(location.getLongitude()));
                    if (settings.getCurrentCity() == null) {
                        LoaderContext loaderContext = new LoaderContext(MainActivity.this, new Arguments());
                        loaderContext.setLoader(R.id.city_loader);
                        new DataFetcher(MainActivity.this, loaderContext, MainActivity.this).fetchFromWeb();
                    }
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUI();
    }

    protected void replaceFragment(String fragmentName, Bundle args) {
        BaseFragment fragment = fragmentFactory.createFragment(fragmentName, args);
        history.push(fragment, fragmentName);
        checkGeo();
        updateUI();
    }


    private void updateUI() {

        if (!history.empty()
                && history.lastElement().getTag() != null
                && history.lastElement().getTag().equals(FragmentFactory.MAIN_MENU)) {

            City city = Settings.getInstance(this).getCurrentCity();
            if (city != null) {
                selectCityTextView.setVisibility(View.VISIBLE);
                selectCityButton.setVisibility(View.VISIBLE);
                selectCityTextView.setText(R.string.your_city);
                selectCityButton.setText(city.getCity());
            } else {
                selectCityButton.setVisibility(View.VISIBLE);
                selectCityTextView.setVisibility(View.GONE);
                selectCityButton.setText(R.string.choose_city);
            }

            makeNavigationDrawerControlButtonActive(false);
        } else {
            selectCityButton.setVisibility(View.GONE);
            selectCityTextView.setVisibility(View.VISIBLE);
            try {
                String title = history.lastElement().getTitle();
                selectCityTextView.setText(title);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            navigationDrawerStateManager.changeState(Settings.getInstance(this).getUserState());
            makeNavigationDrawerControlButtonActive(true);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navigationDrawerControlButton:
                DrawerLayout drawerLayout = navigationDrawerStateManager.getDrawerLayout();
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
                break;
            case R.id.selectCityButton:
                replaceFragment(FragmentFactory.SELECT_CITY, new Bundle());
                break;
            default:
        }
    }

    private void makeNavigationDrawerControlButtonActive(boolean active) {
        DrawerLayout drawerLayout = navigationDrawerStateManager.getDrawerLayout();
        drawerLayout.closeDrawer(Gravity.LEFT);
        if (active) {
            navigationDrawerControlButton.setVisibility(View.VISIBLE);
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        } else {
            navigationDrawerControlButton.setVisibility(View.GONE);
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    @Override
    public void onLoaderFragmentFinishedAndGoTo(String goToFragmentName, Bundle args) {
        replaceFragment(goToFragmentName, args);

    }


    @Override
    public void onLoaderFragmentFinishedAndGoBack() {
        onBackPressed();
    }


    @Override
    public void onBackPressed() {
        if (history.size() >= 2) {
            history.pop();
        } else {
            super.onBackPressed();
        }
        updateUI();

    }

    /**
     * Обработка кликов по панели навигации
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final int navigationDrawerListSize = parent.getAdapter().getCount();

        if (navigationDrawerListSize < 4)
            return;

        String[] actions = new String[7];
        actions[0] = FragmentFactory.MAIN_MENU;
        actions[1] = FragmentFactory.SALES_COUPONS;

        actions[2] = FragmentFactory.LOAN_COUPONS;
        actions[3] = (navigationDrawerListSize == 7) ? FragmentFactory.CONTRACTS : FragmentFactory.OUTLETS;

        actions[4] = FragmentFactory.OUTLETS;
        actions[5] = FragmentFactory.SELECT_PAY_METHOD;
        actions[6] = FragmentFactory.CONTRACTS_PAYED;
        if (position < actions.length && actions[position] != null) {
            replaceFragment(actions[position], new Bundle());
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putStringArrayList(TAGS, history.getTags());
    }


    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {

        if (loaderid == R.id.city_loader) {
            City city = (City) result.first();
            if (city != null) {
                updateUI();
            }
        }
    }

    @Override
    public void onDataFetchedError(int code, String message) {
        Toast.makeText(this, R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }
}



