package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;

import retrofit.Call;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.Bank;
import ru.webmechanic.moneycare.ui.utils.Arguments;

/**
 * Created by a.bratusenko on 26.05.16.
 */
public class BankLoader extends BaseLoader {
    private String id = null;

    public BankLoader(Context context, Bundle args) {
        super(context);
        if (args.containsKey(Arguments.ARG_ID)) {
            id = args.getString(Arguments.ARG_ID);
        }

    }

    @Override
    protected Response apiCall() throws IOException {
        Bank bank = null;
        String[] params = Settings.getInstance(getContext()).prepareParams();
        Response response = new Response();
        response.setCache(false);

        Call<Bank> call = service.bank(id, params[0], params[1], params[2], params[3], params[4]);
        retrofit.Response<Bank> retrofitResponse = call.execute();

        if (retrofitResponse != null) {
            if (!retrofitResponse.isSuccess()) {
                response.setCode(retrofitResponse.code());
                if (retrofitResponse.errorBody() != null) {
                    response.setErrorMessage(retrofitResponse.message());
                }
            } else {
                bank = retrofitResponse.body();
            }
        }
        return response.setAnswer(bank);

    }
}
