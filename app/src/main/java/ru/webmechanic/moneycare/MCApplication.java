package ru.webmechanic.moneycare;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

/**
 * Created by a.bratusenko on 01.07.16.
 */
public class MCApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
//        LeakCanary.install(this);
    }
}
