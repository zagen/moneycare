package ru.webmechanic.moneycare.ui.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;

/**
 * Created by a.bratusenko on 05.07.16.
 */
public class DetailsView extends LinearLayout {
    protected View view;

    protected boolean isOpen = false;

    @BindView(R.id.detailsViewTitle)
    TextView titleTextView;

    @BindView(R.id.detailsViewTitleArrowImageView)
    ImageView arrowImageView;

    protected OnClickListener clickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isOpen) {
                open();
            } else {
                close();
            }
        }
    };
    @BindView(R.id.detailsContainer)
    LinearLayout detailsContainer;

    public DetailsView(Context context) {
        super(context);
        init();
    }

    public DetailsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        getTitleFromAttr(context, attrs);
    }

    public DetailsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
        getTitleFromAttr(context, attrs);
    }

    private void getTitleFromAttr(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.DetailsView,
                0, 0);
        try {
            titleTextView.setText(a.getString(R.styleable.DetailsView_header));
        } finally {
            a.recycle();
        }
    }

    private void init() {
        inflate(getContext(), R.layout.customviews_details, this);
        ButterKnife.bind(this);
        setOnClickListener(this.clickListener);
    }

    public void setView(View view) {
        this.view = view;
        if (view != null) {
            if (detailsContainer.getChildCount() > 1) {
                detailsContainer.removeViews(1, detailsContainer.getChildCount() - 1);
            }

            this.setVisibility(VISIBLE);
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            detailsContainer.addView(view, layoutParams);
        } else {
            this.setVisibility(GONE);
        }

    }

    public void open() {
        isOpen = true;
        try {
            view.setVisibility(VISIBLE);
            arrowImageView.setImageResource(R.drawable.arrow_up);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close() {
        isOpen = false;
        try {
            view.setVisibility(GONE);
            arrowImageView.setImageResource(R.drawable.arrow_down);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getTitle() {
        return titleTextView.getText().toString();
    }

    public void setTitle(String title) {
        titleTextView.setText(title);
    }
}
