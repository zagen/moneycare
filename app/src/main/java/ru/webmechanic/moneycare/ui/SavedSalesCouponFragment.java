package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.SalesCoupon;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.ui.utils.Arguments;
import ru.webmechanic.moneycare.ui.utils.SavedSalesCouponAdapter;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class SavedSalesCouponFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener {

    @BindView(R.id.savedSalesCouponsListView)
    ListView savedCouponsListView;

    @BindView(R.id.noDataTextView)
    TextView noDataTextView;
    Result<SalesCoupon> salesCouponResult;

    public SavedSalesCouponFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_saved_sales_coupon, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    protected void fetchData() {
        isLoading = true;
        new DataFetcher(this, loaderContext, this)
                .inCollection("id", settings.getStorage().getSavedSalesCoupons(), false)
                .fetch();
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.saved_sales_coupons);
    }

    @OnItemClick(R.id.savedSalesCouponsListView)
    public void showCoupon(AdapterView<?> parent, int position) {
        String couponId = ((SalesCoupon) parent.getAdapter().getItem(position)).getId();
        Arguments args = new Arguments();
        args.setId(couponId);
        gotoFragment(FragmentFactory.SALES_COUPON, args);
    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        if (loaderid == R.id.sales_coupons_loader) {
            try {
                this.salesCouponResult = (Result<SalesCoupon>) result;
            } catch (Exception e) {
            }
        }
        isLoading = false;
        updateUI();
    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        if (isLoading)
            return;
        if (salesCouponResult != null && salesCouponResult.size() != 0) {
            noDataTextView.setVisibility(View.GONE);
            savedCouponsListView.setAdapter(new SavedSalesCouponAdapter(getActivity(), salesCouponResult, new SavedSalesCouponAdapter.OnItemDeletedListener() {
                @Override
                public void onItemDeleted() {
                    fetchData();
                    updateUI();
                }
            }));
        } else {
            savedCouponsListView.setAdapter(null);
            noDataTextView.setVisibility(View.VISIBLE);
        }
    }
}
