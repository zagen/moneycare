package ru.webmechanic.moneycare.maputils;

import java.util.List;

/**
 * Created by a.bratusenko on 15.06.16.
 * Абстрактный класс для предоставления данных для создания MapInfo
 *
 * @see MapInfo
 */
public abstract class MapInfoAdapter {

    public abstract double getLongitude();

    public abstract double getLatitude();

    public abstract List<Balloon> getBalloons();

    public abstract boolean isOneActive();

    public abstract String getName();
}
