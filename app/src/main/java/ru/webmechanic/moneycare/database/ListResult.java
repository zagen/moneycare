package ru.webmechanic.moneycare.database;

import java.util.List;

/**
 * Created by a.bratusenko on 30.05.16.
 * Класс для хранения списка данных
 *
 * @see Result
 */
public class ListResult<T> extends Result<T> {

    private List<T> list;

    public ListResult(List<T> list) {
        this.list = list;
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public T getItem(int position) {
        return list.get(position);
    }

    @Override
    public void removeAt(int position) {

    }

    @Override
    public List<T> getList() {
        return list;
    }

    @Override
    public T first() {
        if (size() > 0) {
            return getItem(0);
        }
        return null;
    }
}
