package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.webmechanic.moneycare.LocationManager;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.content.NearestTerminal;
import ru.webmechanic.moneycare.content.PaymentSystem;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.maputils.MapDataProcessor;
import ru.webmechanic.moneycare.maputils.mapinfoadapters.MapInfoAdapterNearestTerminals;
import ru.webmechanic.moneycare.maputils.mapinfoadapters.MapInfoAdapterPaymentSystems;
import ru.webmechanic.moneycare.ui.customviews.ContractView;
import ru.webmechanic.moneycare.ui.utils.Arguments;
import ru.webmechanic.moneycare.ui.utils.NearestTerminalsAdapter;
import ru.webmechanic.moneycare.ui.utils.PaymentSystemsAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentSystemsFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener{

    @BindView(R.id.psListView)
    ListView listView;

    @BindView(R.id.psContractView)
    ContractView contractView;

    @BindView(R.id.switchNearestAllPsButton)
    Button switchNearestAllButton;

    @BindView(R.id.noDataTextView)
    TextView noDataTextView;

    private Result<NearestTerminal> nearestTerminalResult;
    private Result<PaymentSystem> paymentSystemResult;

    public PaymentSystemsFragment() {
        // Required empty public constructor
    }

    @Override
    protected void fetchData() {
        isLoading = true;
        if (loaderContext.getLoader() == R.id.nearest_terminals_loader && settings.isInternetAvailable()) {
            new DataFetcher(this, loaderContext, this).fetchFromWeb();
        } else {
            new DataFetcher(this, loaderContext, this).fetch();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment_systems, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.payment_systems);
    }


    @OnClick(R.id.switchNearestAllPsButton)
    void switchNearestAll() {
        if (loaderContext.getLoader() == R.id.payment_systems_loader) {
            loaderContext.setLoader(R.id.nearest_terminals_loader);

        } else if (loaderContext.getLoader() == R.id.nearest_terminals_loader) {
            loaderContext.setLoader(R.id.payment_systems_loader);
        }
        fetchData();
        updateUI();
    }

    @OnClick(R.id.mapPsButton)
    void showMap() {
        if (switchNearestAllButton.getText().toString().equals(getString(R.string.nearest))) {
            Result result = new DataFetcher(this, PaymentSystem.class).fetchFromCache();
            gotoFragment(FragmentFactory.MAP,
                    MapDataProcessor.prepareData(new Bundle(),
                            new MapInfoAdapterPaymentSystems(getActivity(),
                                    Double.parseDouble(settings.getLongitude()),
                                    Double.parseDouble(settings.getLatitude()), result)));

        } else {
            Result result = new DataFetcher(this, NearestTerminal.class).fetchFromCache();
            gotoFragment(FragmentFactory.MAP,
                    MapDataProcessor.prepareData(new Bundle(),
                            new MapInfoAdapterNearestTerminals(getActivity(),
                                    Double.parseDouble(settings.getLongitude()),
                                    Double.parseDouble(settings.getLatitude()), result)));

        }
    }


    @Override
    protected void updateUI() {
        super.updateUI();
        new LocationManager(getActivity()).requestBestLocation();
        if (arguments.isSetContract()) {
            Contract contract = null;
            try {
                contract = (Contract) new DataFetcher(this, Contract.class).one(arguments.getContract()).fetchFromCache().first();
            } catch (Exception e) {
            }
            contractView.setContract(contract);
            contractView.setVisibility(View.VISIBLE);
        }

        switchNearestAllButton.setEnabled(!isLoading);
        int noDataStringId = R.string.no_data_ps;
        if (loaderContext.getLoader() == R.id.payment_systems_loader) {
            switchNearestAllButton.setText(getString(R.string.nearest));
        } else {
            noDataStringId = R.string.no_data_nearestterminals;
            switchNearestAllButton.setText(getString(R.string.all));
        }
        if (isLoading) {
            return;
        }
        if (paymentSystemResult != null && paymentSystemResult.size() != 0) {
            noDataTextView.setVisibility(View.GONE);
            setPSAdapter(paymentSystemResult);
        } else if (nearestTerminalResult != null && nearestTerminalResult.size() != 0) {
            settings.getStorage().saveNearestTerminalsDistances(nearestTerminalResult);
            noDataTextView.setVisibility(View.GONE);
            setNearestTerminalsAdapter(nearestTerminalResult);
        } else {
            noDataTextView.setVisibility(View.VISIBLE);
            noDataTextView.setText(noDataStringId);
            listView.setAdapter(null);
        }
    }

    private void setPSAdapter(Result<PaymentSystem> result) {

        PaymentSystemsAdapter adapter = new PaymentSystemsAdapter(getActivity(), result);
        adapter.setListeners(new PaymentSystemsAdapter.PaymentsSystemAdapterListeners() {
            @Override
            public void onTerminalClicked(String psId) {
                Arguments args = new Arguments();
                args.setId(psId);
                args.setContract(arguments.getContract());
                gotoFragment(FragmentFactory.PAYMENT_SYSTEM, args);
            }

            @Override
            public void onCommissionClicked(String psId) {
                Arguments args = new Arguments();
                args.setId(psId);
                args.setContract(arguments.getContract());
                gotoFragment(FragmentFactory.PAYMENT_SYSTEM, args);
            }
        });
        listView.setAdapter(adapter);
        listView.setVisibility(View.VISIBLE);

    }

    private void setNearestTerminalsAdapter(final Result<NearestTerminal> result) {
        NearestTerminalsAdapter adapter = new NearestTerminalsAdapter(getActivity(), result);
        adapter.setListener(new NearestTerminalsAdapter.NearestTerminalAdapterListener() {
            @Override
            public void onNearestTerminalsClicked(NearestTerminal terminal) {
                Bundle args = MapDataProcessor.prepareData(new Bundle(),
                        new MapInfoAdapterNearestTerminals(getActivity(), terminal, result));
                gotoFragment(FragmentFactory.MAP, args);
            }
        });
        listView.setAdapter(adapter);
    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        paymentSystemResult = null;
        nearestTerminalResult = null;
        if (loaderContext.getLoader() == R.id.payment_systems_loader) {
            try {
                paymentSystemResult = (Result<PaymentSystem>) result;
            } catch (Exception e) {
            }

        } else if (loaderContext.getLoader() == R.id.nearest_terminals_loader) {
            try {
                nearestTerminalResult = (Result<NearestTerminal>) result;
            } catch (Exception e) {
            }
        }
        isLoading = false;
        updateUI();
    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }
}
