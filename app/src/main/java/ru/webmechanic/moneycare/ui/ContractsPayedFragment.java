package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.ui.utils.ContractsPayedAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContractsPayedFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener{
    @BindView(R.id.payedContractsListView)
    ListView listView;

    @BindView(R.id.noDataTextView)
    TextView noDataTextView;

    private Result<Contract> contractResult;

    public ContractsPayedFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contracts_payed, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.payedContractsGoToArchiveButton)
    void gotoContractsFragment() {
        gotoFragment(FragmentFactory.CONTRACTS);
    }


    @Override
    protected void fetchData() {
        isLoading = true;
        new DataFetcher(this, loaderContext, this).inCollection("id", settings.getStorage().getPayedContracts(), false).fetch();
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.payed_contracts);
    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        if (loaderid == R.id.contracts_loader) {
            try {
                contractResult = (Result<Contract>) result;
            } catch (Exception e) {
            }

        }
        isLoading = false;
        updateUI();

    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void updateUI() {
        super.updateUI();

        if (contractResult != null && contractResult.size() != 0) {
            noDataTextView.setVisibility(View.GONE);
            listView.setAdapter(new ContractsPayedAdapter(this, contractResult));
        } else {
            noDataTextView.setVisibility(View.VISIBLE);
        }
    }
}
