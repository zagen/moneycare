package ru.webmechanic.moneycare.maputils.mapinfoadapters;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.ATM;
import ru.webmechanic.moneycare.content.Address;
import ru.webmechanic.moneycare.content.Department;
import ru.webmechanic.moneycare.content.NearestDepartment;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.maputils.Balloon;
import ru.webmechanic.moneycare.maputils.MapInfoAdapter;

/**
 * Created by a.bratusenko on 15.06.16.
 * Извлечение из списка ближайших отделений/банкоматов данных необходимых для карты
 */
public class MapInfoAdapterNearestDepartments extends MapInfoAdapter {

    private Context context;
    private double longitude;
    private double latitude;
    private boolean oneActive = false;
    private String title;
    private List<Balloon> balloons = new ArrayList<>();

    public MapInfoAdapterNearestDepartments(Context context, double longitude, double latitude, Result<NearestDepartment> nearestDepartmentResult) {
        init(context, longitude, latitude, nearestDepartmentResult);
    }

    public MapInfoAdapterNearestDepartments(Context context, NearestDepartment nearestDepartment, Result<NearestDepartment> nearestDepartmentResult) {
        Address address;
        if (nearestDepartment.getAtm() != null)
            address = nearestDepartment.getAtm().getAddress();
        else {
            address = nearestDepartment.getDepartment().getAddress();
        }
        init(context, Double.parseDouble(address.getLongitude()), Double.parseDouble(address.getLatitude()), nearestDepartmentResult);
        oneActive = true;

    }

    private void init(Context context, double longitude, double latitude, Result<NearestDepartment> nearestDepartmentResult) {
        this.context = context;
        this.longitude = longitude;
        this.latitude = latitude;
        for (int i = 0; i < nearestDepartmentResult.size(); i++) {
            Balloon balloon = new Balloon();
            NearestDepartment nearestDepartment = nearestDepartmentResult.getItem(i);
            Address address;
            String title;
            if (nearestDepartment.getAtm() != null) {
                ATM atm = nearestDepartment.getAtm();
                address = atm.getAddress();
                title = String.format(context.getString(R.string.atm_of_bank), nearestDepartment.getBankName());
            } else {
                Department department = nearestDepartment.getDepartment();
                address = department.getAddress();
                title = String.format(context.getString(R.string.department_of_bank), nearestDepartment.getBankName());
            }
            balloon.setAddress(address.getFullAddress());
            balloon.setLatitude(Double.parseDouble(address.getLatitude()));
            balloon.setLongitude(Double.parseDouble(address.getLongitude()));
            balloon.setTitle(title);
            this.balloons.add(balloon);
        }

    }

    @Override
    public double getLongitude() {
        return longitude;
    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    @Override
    public List<Balloon> getBalloons() {
        return balloons;
    }

    @Override
    public boolean isOneActive() {
        return oneActive;
    }

    @Override
    public String getName() {
        return context.getString(R.string.departments_and_atms_on_map);
    }
}
