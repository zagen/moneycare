package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;

import retrofit.Call;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.User;
import ru.webmechanic.moneycare.ui.utils.Arguments;

/**
 * Created by a.bratusenko on 26.05.16.
 */
public class ConfirmPinLoader extends BaseLoader {
    private String pin = null;

    public ConfirmPinLoader(Context context, Bundle bundle) {
        super(context);
        if (bundle != null && bundle.containsKey(Arguments.ARG_PIN))
            this.pin = bundle.getString(Arguments.ARG_PIN);

    }

    @Override
    protected Response apiCall() throws IOException {
        Settings settings = Settings.getInstance(getContext());
        User user = null;
        Response response = new Response();
        String token = settings.getUserToken();
        String phone = settings.getPhone();

        Call<User> call = service.confirmPin(phone, token, pin);

        retrofit.Response<User> retrofitResponse = call.execute();

        if (retrofitResponse != null) {
            if (!retrofitResponse.isSuccess()) {
                response.setCode(retrofitResponse.code());
                if (retrofitResponse.errorBody() != null) {
                    response.setErrorMessage(retrofitResponse.message());
                }
            } else {
                user = retrofitResponse.body();
            }
        }

        if (user != null && user.getStatus() == 1) {
            settings.setPhone(user.getPhone());
            settings.setUserToken(user.getToken());
            settings.setUserState(Settings.USER_STATE_LOGGED);
        }

        return response.setCache(false)
                .setRequestResult(RequestResult.SUCCESS)
                .setAnswer(user);
    }

}
