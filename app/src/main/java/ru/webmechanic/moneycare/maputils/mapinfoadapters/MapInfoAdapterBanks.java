package ru.webmechanic.moneycare.maputils.mapinfoadapters;

import android.content.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.ATM;
import ru.webmechanic.moneycare.content.Bank;
import ru.webmechanic.moneycare.content.Department;
import ru.webmechanic.moneycare.database.ListResult;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.maputils.Balloon;
import ru.webmechanic.moneycare.maputils.MapInfoAdapter;

/**
 * Created by a.bratusenko on 15.06.16.
 * Извлекает информацию из объектов класса Bank в MapInfo
 *
 * @see Bank
 * @see MapInfoAdapter
 * @see ru.webmechanic.moneycare.maputils.MapInfo
 */
public class MapInfoAdapterBanks extends MapInfoAdapter {
    public final static int SHOW_ONLY_ATMS = -1;
    public final static int SHOW_ONLY_DEPARTMENTS = 1;
    public final static int SHOW_ALL = 0;
    private double longitude;
    private double latitude;
    private List<Balloon> balloons = new ArrayList<>();
    private boolean oneActive = false;
    private String name;

    public MapInfoAdapterBanks(Context context, double longitude, double latitude, Result<Bank> bankResult) {
        this(context, longitude, latitude, bankResult, SHOW_ALL);
    }

    public MapInfoAdapterBanks(Context context, double longitude, double latitude, Result<Bank> bankResult, int whatShow) {

        this.longitude = longitude;
        this.latitude = latitude;
        for (int i = 0; i < bankResult.size(); i++) {
            for (Department department : bankResult.getItem(i).getDepartments()) {
                if (whatShow != SHOW_ONLY_ATMS) {
                    Balloon balloon = new Balloon();
                    balloon.setLatitude(Double.parseDouble(department.getAddress().getLatitude()));
                    balloon.setLongitude(Double.parseDouble(department.getAddress().getLongitude()));
                    balloon.setTitle(String.format(context.getString(R.string.department_of_bank), bankResult.getItem(i).getTitle()));
                    balloon.setAddress(department.getAddress().getFullAddress());
                    this.balloons.add(balloon);
                }
            }
            for (ATM atm : bankResult.getItem(i).getAtms()) {
                if (whatShow != SHOW_ONLY_DEPARTMENTS) {
                    Balloon balloon = new Balloon();
                    balloon.setLatitude(Double.parseDouble(atm.getAddress().getLatitude()));
                    balloon.setLongitude(Double.parseDouble(atm.getAddress().getLongitude()));
                    balloon.setTitle(String.format(context.getString(R.string.atm_of_bank), bankResult.getItem(i).getTitle()));
                    balloon.setAddress(atm.getAddress().getFullAddress());
                    this.balloons.add(balloon);
                }
            }
        }
        this.name = context.getString(R.string.departments_and_atms_on_map);
    }

    public MapInfoAdapterBanks(Context context, Department department, Bank bank) {
        this(context,
                Double.parseDouble(department.getAddress().getLongitude()),
                Double.parseDouble(department.getAddress().getLatitude()),
                new ListResult<>(Arrays.asList(bank)),
                SHOW_ONLY_DEPARTMENTS);
        oneActive = true;
        this.name = context.getString(R.string.departments_and_atms_on_map);
    }

    public MapInfoAdapterBanks(Context context, ATM atm, Bank bank) {
        this(context,
                Double.parseDouble(atm.getAddress().getLongitude()),
                Double.parseDouble(atm.getAddress().getLatitude()),
                new ListResult<>(Arrays.asList(bank)),
                SHOW_ONLY_ATMS);
        oneActive = true;
        this.name = context.getString(R.string.departments_and_atms_on_map);
    }

    @Override
    public double getLongitude() {
        return longitude;
    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    @Override
    public List<Balloon> getBalloons() {
        return balloons;
    }

    @Override
    public boolean isOneActive() {
        return oneActive;
    }

    @Override
    public String getName() {
        return name;
    }
}
