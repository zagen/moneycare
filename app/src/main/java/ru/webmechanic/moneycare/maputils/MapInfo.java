package ru.webmechanic.moneycare.maputils;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by a.bratusenko on 15.06.16.
 * Класс данных для хранения нужной для карты информации
 */
public class MapInfo {
    @SerializedName("name")
    private String name;
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("latitude")
    private double latitude;

    @SerializedName("oneActive")
    private boolean oneActive = false;
    @SerializedName("balloons")
    private List<Balloon> balloons;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public List<Balloon> getBalloons() {
        return balloons;
    }

    public void setBalloons(List<Balloon> balloons) {
        this.balloons = balloons;
    }

    public boolean isOneActive() {
        return oneActive;
    }

    public void setOneActive(boolean oneActive) {
        this.oneActive = oneActive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
