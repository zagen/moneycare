package ru.webmechanic.moneycare.ui.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.content.PaymentSystem;
import ru.webmechanic.moneycare.content.Terminal;
import ru.webmechanic.moneycare.ui.customviews.PaymentSystemView;

/**
 * Created by a.bratusenko on 10.06.16.
 */
public class PaymentSystemAdapter extends BaseAdapter {

    public final static int TYPE_TERMINAL = 2;
    public final static int TYPE_BUTTON = 1;
    public final static int TYPE_PAYMENT_SYSTEM = 0;

    private Context context;
    private PaymentSystem paymentSystem;
    private LayoutInflater inflater;
    private Settings settings;
    private PaymentSystemAdapterListener listener;

    public PaymentSystemAdapter(Context context, PaymentSystem paymentSystem) {
        this.context = context;
        this.paymentSystem = paymentSystem;
        this.inflater = LayoutInflater.from(context);
        this.settings = Settings.getInstance(context);
    }

    public void setListener(PaymentSystemAdapterListener listener) {
        this.listener = listener;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        int type = TYPE_TERMINAL;
        if (position == 0) {
            type = TYPE_PAYMENT_SYSTEM;
        } else if (position == 1) {
            type = TYPE_BUTTON;
        }
        return type;
    }

    @Override
    public int getCount() {
        int count = 2;
        if (paymentSystem != null && paymentSystem.getTerminals() != null) {
            count += paymentSystem.getTerminals().size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (paymentSystem != null && paymentSystem.getTerminals() != null && position > 1) {
            return paymentSystem.getTerminals().get(position - 2);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        int type = getItemViewType(position);
        if (convertView == null) {
            if (type == TYPE_PAYMENT_SYSTEM) {
                convertView = inflater.inflate(R.layout.list_item_ps, null);
                ViewHolderPaymentSystem viewHolder = new ViewHolderPaymentSystem(convertView);
                convertView.setTag(viewHolder);
            } else if (type == TYPE_BUTTON) {
                convertView = inflater.inflate(R.layout.list_item_ps_button, null);
                ViewHolderMapButton viewHolder = new ViewHolderMapButton(convertView);
                convertView.setTag(viewHolder);
            } else {
                convertView = inflater.inflate(R.layout.list_item_nearest_departments_atms, null);
                ViewHolderTerminal viewHolder = new ViewHolderTerminal(convertView);
                convertView.setTag(viewHolder);
            }
        }
        if (type == TYPE_PAYMENT_SYSTEM) {
            ViewHolderPaymentSystem viewHolder = (ViewHolderPaymentSystem) convertView.getTag();
            viewHolder.psView.setPaymentSystem(paymentSystem);
        } else if (type == TYPE_BUTTON) {
            ViewHolderMapButton viewHolder = (ViewHolderMapButton) convertView.getTag();
            viewHolder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.OnNeedShowMapForAll(paymentSystem.getId());
                    }
                }
            });
        } else {
            Terminal terminal = paymentSystem.getTerminals().get(position - 2);
            ViewHolderTerminal viewHolder = (ViewHolderTerminal) convertView.getTag();
            final String terminalId = terminal.getId();
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.OnNeedShowMapForTerminal(terminalId);
                    }
                }
            });
            viewHolder.titleTextView.setVisibility(View.GONE);
            viewHolder.addressTextView.setText(terminal.getAddress().getFullAddress());
            viewHolder.workTimeTextView.setText(String.format(context.getString(R.string.time_of_work), "N/A"));
            int distance = settings.getStorage().getNearestDistance(Terminal.class.getSimpleName(), terminalId);
            viewHolder.distanceTextView.setText(Utils.asDistance(distance));


        }
        return convertView;
    }

    public interface PaymentSystemAdapterListener {
        void OnNeedShowMapForTerminal(String terminalId);

        void OnNeedShowMapForAll(String psId);
    }

    static class ViewHolderPaymentSystem {
        @BindView(R.id.psViewItem)
        PaymentSystemView psView;

        public ViewHolderPaymentSystem(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class ViewHolderMapButton {
        @BindView(R.id.psOnMapButton)
        Button button;

        public ViewHolderMapButton(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class ViewHolderTerminal {
        @BindView(R.id.nearestDepATMTitleTextViewItem)
        TextView titleTextView;

        @BindView(R.id.nearestDepATMAddressTextViewItem)
        TextView addressTextView;

        @BindView(R.id.nearestDepATMWorkTimeTextViewItem)
        TextView workTimeTextView;

        @BindView(R.id.stickerFlagNearestDepATMDistanceTextViewItem)
        TextView distanceTextView;

        public ViewHolderTerminal(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
