package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;
import java.util.List;

import retrofit.Call;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.Contract;

/**
 * Created by a.bratusenko on 27.05.16.
 */
public class ContractsLoader extends BaseLoader {
    public ContractsLoader(Context context, Bundle args) {
        super(context);
    }

    @Override
    protected Response apiCall() throws IOException {
        Response response = new Response();
        Settings settings = Settings.getInstance(getContext());
        String[] params = settings.prepareParams();

        List<Contract> contracts = null;

        Call<List<Contract>> call = service.contracts(params[3], params[4]);
        retrofit.Response<List<Contract>> retrofitResponse = call.execute();

        if (retrofitResponse != null) {
            if (!retrofitResponse.isSuccess()) {
                response.setCode(retrofitResponse.code());
                if (retrofitResponse.errorBody() != null) {
                    response.setErrorMessage(retrofitResponse.message());
                }
            } else {
                contracts = retrofitResponse.body();
            }
        }

        response.setRequestResult(RequestResult.SUCCESS)
                .setAnswer(contracts);
        return response;
    }
}
