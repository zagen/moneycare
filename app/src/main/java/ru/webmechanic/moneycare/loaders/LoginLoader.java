package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;

import retrofit.Call;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.User;
import ru.webmechanic.moneycare.ui.utils.Arguments;

/**
 * Created by a.bratusenko on 26.05.16.
 */
public class LoginLoader extends BaseLoader {
    private String phone = null;
    private String password = null;
    private Settings settings;

    public LoginLoader(Context context, Bundle args) {
        super(context);
        settings = Settings.getInstance(context);
        if (args.containsKey(Arguments.ARG_PHONE) && args.containsKey(Arguments.ARG_PASSWORD)) {
            this.phone = args.getString(Arguments.ARG_PHONE);
            this.password = args.getString(Arguments.ARG_PASSWORD);
        }
    }

    @Override
    protected Response apiCall() throws IOException {
        User user = null;
        Response response = new Response();

        Call<User> call = service.login(phone, password);

        retrofit.Response<User> retrofitResponse = call.execute();

        if (retrofitResponse != null) {
            if (!retrofitResponse.isSuccess()) {
                response.setCode(retrofitResponse.code());
                if (retrofitResponse.errorBody() != null) {
                    response.setErrorMessage(retrofitResponse.message());
                }
            } else {
                user = retrofitResponse.body();
            }
        }

        if (user != null) {
            settings.setPhone(user.getPhone());
            settings.setUserToken(user.getToken());

            if (user.getStatus() == 1) {
                settings.setUserState(Settings.USER_STATE_LOGGED);
            } else {
                settings.setUserState(Settings.USER_STATE_PIN_AWAIT);
            }

        }
        return response.setCache(false)
                .setRequestResult(RequestResult.SUCCESS)
                .setAnswer(user);
    }
}
