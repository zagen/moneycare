package ru.webmechanic.moneycare.content;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by a.bratusenko on 30.05.16.
 * Содержит информацию о ближайшей точке продаж
 */
public class NearestOutlet extends RealmObject {

    @SerializedName("outlet")
    private Outlet outlet;

    @SerializedName("distance")
    private int distance;

    public Outlet getOutlet() {
        return outlet;
    }

    public void setOutlet(Outlet outlet) {
        this.outlet = outlet;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
}
