package ru.webmechanic.moneycare.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import ru.webmechanic.moneycare.content.Outlet;

/**
 * Created by a.bratusenko on 06.07.16.
 */
public class OutletsView extends LinearLayout {

    private OnContentViewClickListener listener;

    public OutletsView(Context context) {
        super(context);
        init();
    }

    public OutletsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OutletsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setOrientation(VERTICAL);
    }

    public void setOutlets(List<Outlet> outlets) {
        removeAllViews();
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        for (int i = 0; i < outlets.size(); i++) {
            Outlet outlet = outlets.get(i);
            OutletItemView outletItemView = new OutletItemView(getContext());
            outletItemView.setOutlet(outlet);
            final String id = outlets.get(i).getId();

            outletItemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onContentViewClicked(id);
                    }
                }
            });
            addView(outletItemView, layoutParams);
        }
    }

    public void setListener(OnContentViewClickListener listener) {
        this.listener = listener;
    }

    public interface OnContentViewClickListener {
        void onContentViewClicked(String id);
    }

}
