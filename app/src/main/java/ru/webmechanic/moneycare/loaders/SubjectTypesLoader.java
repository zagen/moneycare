package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;
import java.util.List;

import retrofit.Call;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.SubjectType;
import ru.webmechanic.moneycare.ui.utils.Arguments;

/**
 * Created by a.bratusenko on 27.05.16.
 */
public class SubjectTypesLoader extends BaseLoader {
    private String query;

    public SubjectTypesLoader(Context context, Bundle args) {
        super(context);
        if (args.containsKey(Arguments.ARG_QUERY)) {
            query = args.getString(Arguments.ARG_QUERY);
        }
    }

    @Override
    protected Response apiCall() throws IOException {
        List<SubjectType> subjectTypes = null;
        Response response = new Response();
        Settings settings = Settings.getInstance(getContext());
        String[] params = settings.prepareParams();

        Call<List<SubjectType>> call = service.searchType(query, params[0], params[1], params[2], params[3], params[4]);
        retrofit.Response<List<SubjectType>> retrofitResponse = call.execute();

        if (retrofitResponse != null) {
            if (!retrofitResponse.isSuccess()) {
                response.setCode(retrofitResponse.code());
                if (retrofitResponse.errorBody() != null) {
                    response.setErrorMessage(retrofitResponse.message());
                }
            } else {
                subjectTypes = retrofitResponse.body();
            }
        }

        return response.setRequestResult(RequestResult.SUCCESS)
                .setAnswer(subjectTypes);
    }
}
