package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.content.LoanCoupon;
import ru.webmechanic.moneycare.content.LocalData.LoanCouponFixData;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.ui.customviews.OutletsDetailsView;
import ru.webmechanic.moneycare.ui.customviews.OutletsView;
import ru.webmechanic.moneycare.ui.customviews.SimpleTextDetailsView;
import ru.webmechanic.moneycare.ui.utils.Arguments;
import ru.webmechanic.moneycare.ui.utils.Utils;
import ru.webmechanic.moneycare.ui.utils.imageloader.ImageLoader;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class LoanCouponFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener, OutletsView.OnContentViewClickListener {
    @BindView(R.id.saveLoanCouponButton)
    Button saveMarkedButton;

    @BindView(R.id.deleteLoanCouponButton)
    Button deleteSavedButton;

    @BindView(R.id.repayLoanCouponButton)
    Button repayButton;

    @BindView(R.id.imageCouponImageViewItem)
    ImageView imageView;

    @BindView(R.id.titleCouponTextViewItem)
    TextView categoryTextView;

    @BindView(R.id.validDateCouponTextViewItem)
    TextView validTextView;

    @BindView(R.id.firmCouponTextViewItem)
    TextView subjectTextView;

    @BindView(R.id.labelConditionsLoanCouponTextView)
    TextView conditionLabelTextView;

    @BindView(R.id.conditionsCouponTextViewItem)
    TextView conditionTextView;

    @BindView(R.id.codeLoanCouponEditText)
    EditText codeEditText;

    @BindView(R.id.outletsDetailsView)
    OutletsDetailsView outletsDetailsView;

    @BindView(R.id.descriptionDetailsView)
    SimpleTextDetailsView descriptionView;

    @BindView(R.id.organizationDetailsView)
    SimpleTextDetailsView organizationView;

    private LoanCoupon coupon;
    private ImageLoader imageLoader;

    public LoanCouponFragment() {
        // Required empty public constructor
    }


    @Override
    protected void fetchData() {
        isLoading = true;
        new DataFetcher(LoanCouponFragment.this, loaderContext, LoanCouponFragment.this).one(arguments.getId()).fetch();
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.coupon);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_loan_coupon, container, false);
        ButterKnife.bind(this, view);
        imageLoader = new ImageLoader(getActivity());
        outletsDetailsView.setListener(this);

        return view;
    }

    @OnClick(R.id.saveLoanCouponButton)
    void saveLoanCoupon() {
        if (arguments.isSetId()) {
            settings.getStorage().saveLoanCoupon(arguments.getId(), true);
            updateUI();
        }
    }

    @OnClick(R.id.deleteLoanCouponButton)
    void deleteLoanCoupon() {
        if (arguments.isSetId()) {
            settings.getStorage().saveLoanCoupon(arguments.getId(), false);
            updateUI();
        }
    }

    @OnClick(R.id.repayLoanCouponButton)
    void repay() {
        String idexternal = codeEditText.getText().toString().trim();
        if (idexternal.length() > 0) {
            if (getArguments() == null) {
                setArguments(new Bundle());
            }
            arguments.setIdexternal(idexternal);
            loaderContext.setLoader(R.id.fix_loan_coupon_loader);
            new DataFetcher(this, loaderContext, this).fetchFromWeb();
            updateUI();
        }
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        //all magic is here
        boolean inputEnabled = (loaderContext.getLoader() != R.id.fix_loan_coupon_loader);
        if (!settings.getUserState().equals(Settings.USER_STATE_LOGGED)) {
            inputEnabled = false;
            conditionLabelTextView.setText(getString(R.string.input_coupon_label_user_unauth));
        } else if (!settings.isInternetAvailable()) {
            inputEnabled = false;
            conditionLabelTextView.setText(getString(R.string.input_coupon_label_no_internet));
        }
        if (coupon != null) {
            categoryTextView.setText(coupon.getSubjectCategory());
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            validTextView.setText(getString(R.string.until_only) + " " + sdf.format(coupon.getEndDate()));
            subjectTextView.setText(coupon.getSubject());
            conditionTextView.setText(coupon.getValue());
            imageLoader.DisplayImage(coupon.getImagePath(), imageView);

            if (coupon.getDescription() != null && coupon.getDescription().length() > 0) {
                descriptionView.setVisibility(View.VISIBLE);
                descriptionView.setText(coupon.getDescription());
            } else {
                descriptionView.setVisibility(View.GONE);
            }

            if (settings.getStorage().isLoanCouponSaved(arguments.getId())) {
                saveMarkedButton.setEnabled(false);
                deleteSavedButton.setEnabled(true);
            } else {
                saveMarkedButton.setEnabled(true);
                deleteSavedButton.setEnabled(false);
            }
            inputEnabled = inputEnabled && !Utils.isFixed(settings, coupon);
            if (coupon.getBank() != null && coupon.getBank().length() > 0) {
                organizationView.setVisibility(View.VISIBLE);
                organizationView.setText(coupon.getBank());
            } else {
                organizationView.setVisibility(View.GONE);
            }
            if (coupon.getOutlets().size() > 0) {
                outletsDetailsView.setVisibility(View.VISIBLE);
                outletsDetailsView.setOutlets(coupon.getOutlets());
            } else {
                outletsDetailsView.setVisibility(View.GONE);
            }
        }
        codeEditText.setEnabled(inputEnabled);
        repayButton.setEnabled(inputEnabled);
    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        if (loaderid == R.id.loan_coupons_loader) {
            if (result.size() > 0) {
                this.coupon = (LoanCoupon) result.first();
            }

        } else if (loaderid == R.id.fix_loan_coupon_loader) {
            LoanCouponFixData fixLocalData = (LoanCouponFixData) result.first();
            if (fixLocalData != null) {
                gotoBack();
                gotoFragment(FragmentFactory.THANKS);
            } else {
                AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
                adb.setMessage(R.string.wrong_code)
                        .setPositiveButton("OK", null)
                        .show();
            }
            loaderContext.setLoader(R.id.loan_coupons_loader);
            arguments.setIdexternal(null);
        }
        isLoading = false;
        updateUI();

    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onContentViewClicked(String id) {
        Arguments arguments = new Arguments();
        arguments.setId(id);
        gotoFragment(FragmentFactory.OUTLET, arguments);
    }
}
