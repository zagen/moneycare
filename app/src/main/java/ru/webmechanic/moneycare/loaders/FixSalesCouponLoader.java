package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;

import retrofit.Call;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.LocalData.SalesCouponFixData;
import ru.webmechanic.moneycare.ui.utils.Arguments;

/**
 * Created by a.bratusenko on 03.06.16.
 */
public class FixSalesCouponLoader extends BaseLoader {

    private String id;
    private String idexternal;
    private String phone;
    private String token;
    private Settings settings;

    public FixSalesCouponLoader(Context context, Bundle args) {
        super(context);
        this.settings = Settings.getInstance(context);
        if (args != null
                && args.containsKey(Arguments.ARG_ID)
                && args.containsKey(Arguments.ARG_IDEXTERNAL)) {
            this.id = args.getString(Arguments.ARG_ID);
            this.idexternal = args.getString(Arguments.ARG_IDEXTERNAL);
        }
        String[] params = settings.prepareParams();
        phone = params[3];
        token = params[4];
    }

    @Override
    protected Response apiCall() throws IOException {
        SalesCouponFixData fixLocalData = null;
        Response response = new Response();
        Call<SalesCouponFixData> call = service.fixSalesCoupon(id, idexternal, phone, token);
        retrofit.Response<SalesCouponFixData> retrofitResponse = call.execute();

        if (retrofitResponse != null) {
            if (!retrofitResponse.isSuccess()) {
                response.setCode(retrofitResponse.code());
                if (retrofitResponse.errorBody() != null) {
                    response.setErrorMessage(retrofitResponse.message());
                }
            } else {
                fixLocalData = retrofitResponse.body();
            }
        }

        if (fixLocalData != null) {
            settings.getStorage().fixSalesCoupon(fixLocalData.getCouponId());
            settings.getStorage().saveSalesCoupon(fixLocalData.getCouponId(), false);
            settings.getStorage().markSalesCoupon(fixLocalData.getCouponId(), false);
        }
        return response
                .setCache(false)
                .setRequestResult(RequestResult.SUCCESS)
                .setAnswer(fixLocalData);

    }
}
