package ru.webmechanic.moneycare.ui.utils;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.content.ATM;
import ru.webmechanic.moneycare.content.Bank;
import ru.webmechanic.moneycare.content.Department;
import ru.webmechanic.moneycare.database.ListResult;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.maputils.MapDataProcessor;
import ru.webmechanic.moneycare.maputils.MapInfoAdapter;
import ru.webmechanic.moneycare.maputils.mapinfoadapters.MapInfoAdapterBanks;
import ru.webmechanic.moneycare.ui.BaseFragment;
import ru.webmechanic.moneycare.ui.FragmentFactory;
import ru.webmechanic.moneycare.ui.customviews.BankView;

/**
 * Created by a.bratusenko on 09.06.16.
 * Класс для отображения информации о банке в виде списка
 */
public class BankAdapter extends BaseAdapter {
    private static final int TYPE_BANK = 0;
    private static final int TYPE_DOUBLE_BUTTONS = 1;
    private static final int TYPE_DEPARTMENT = 2;

    private BaseFragment baseFragment;
    private Bank bank;
    private boolean showATMs;
    private Settings settings;

    public BankAdapter(BaseFragment baseFragment, Bank bank, boolean showATMs) {
        this.baseFragment = baseFragment;
        this.bank = bank;
        this.showATMs = showATMs;
        this.settings = Settings.getInstance(baseFragment.getActivity());
    }

    @Override
    public int getCount() {
        int count = 2;
        if (showATMs) {
            if (bank.getAtms() != null) {
                count = bank.getAtms().size() + 2;
            }
        } else {
            if (bank.getDepartments() != null) {
                count = bank.getDepartments().size() + 2;
            }
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (position > 1) {
            if (showATMs) {
                if (bank.getAtms() != null) {
                    return bank.getAtms().get(position - 2);
                }
            } else {
                if (bank.getDepartments() != null) {
                    return bank.getDepartments().get(position - 2);
                }
            }
        }
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        int type = TYPE_DEPARTMENT;
        if (position == 0) {
            type = TYPE_BANK;
        } else if (position == 1) {
            type = TYPE_DOUBLE_BUTTONS;
        }
        return type;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(baseFragment.getActivity());
        int type = getItemViewType(position);
        //если вида еще нет создаем
        if (convertView == null) {
            if (type == TYPE_BANK) {
                convertView = inflater.inflate(R.layout.list_item_bank, null);
                ViewHolderBankInfo viewHolder = new ViewHolderBankInfo(convertView);
                convertView.setTag(viewHolder);
            } else if (type == TYPE_DOUBLE_BUTTONS) {
                convertView = inflater.inflate(R.layout.list_item_double_button, null);
                ViewHolderDoubleButton viewHolder = new ViewHolderDoubleButton(convertView);
                convertView.setTag(viewHolder);
            } else {
                convertView = inflater.inflate(R.layout.list_item_nearest_departments_atms, null);
                ViewHolderNearestInfo viewHolder = new ViewHolderNearestInfo(convertView);
                convertView.setTag(viewHolder);
            }

        }
        //заполняем вид
        if (type == TYPE_BANK) {
            ViewHolderBankInfo viewHolder = (ViewHolderBankInfo) convertView.getTag();
            viewHolder.bankView.setOnDepartmentClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showATMs = false;
                    notifyDataSetChanged();
                }
            });
            viewHolder.bankView.setOnAtmClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showATMs = true;
                    notifyDataSetChanged();
                }
            });
            viewHolder.bankView.setBank(bank);

        } else if (type == TYPE_DOUBLE_BUTTONS) {
            ViewHolderDoubleButton viewHolder = (ViewHolderDoubleButton) convertView.getTag();
            if (showATMs) {
                viewHolder.firstButton.setText(R.string.departments_of_bank);
            } else {
                viewHolder.firstButton.setText(R.string.atms_of_bank);
            }
            viewHolder.firstButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showATMs = !showATMs;
                    notifyDataSetChanged();
                }
            });
            viewHolder.secondButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int whatShow = MapInfoAdapterBanks.SHOW_ONLY_DEPARTMENTS;
                    if (showATMs) {
                        whatShow = MapInfoAdapterBanks.SHOW_ONLY_ATMS;
                    }
                    MapInfoAdapter adapter = new MapInfoAdapterBanks(baseFragment.getActivity(),
                            Double.parseDouble(settings.getLongitude()),
                            Double.parseDouble(settings.getLatitude()),
                            new ListResult<>(Arrays.asList(bank)),
                            whatShow);
                    Bundle mapData = MapDataProcessor.prepareData(null, adapter);
                    baseFragment.gotoFragment(FragmentFactory.MAP, mapData);
                }
            });
        } else {
            ViewHolderNearestInfo viewHolder = (ViewHolderNearestInfo) convertView.getTag();
            viewHolder.titleTextView.setVisibility(View.GONE);
            if (showATMs) {
                if (bank.getAtms() != null) {
                    String address = bank.getAtms().get(position - 2).getAddress().getFullAddress();
                    final String atmId = bank.getAtms().get(position - 2).getId();
                    viewHolder.addressTextView.setText(address);
                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ATM atm = null;
                            try {
                                atm = (ATM) new DataFetcher(baseFragment, ATM.class).one(atmId).fetchFromCache().first();
                            } catch (Exception e) {
                            }
                            Bundle mapData = MapDataProcessor.prepareData(null, new MapInfoAdapterBanks(baseFragment.getActivity(), atm, bank));
                            baseFragment.gotoFragment(FragmentFactory.MAP, mapData);
                        }
                    });
                    int distance = Settings.getInstance(baseFragment.getActivity()).getStorage().getNearestDistance(ATM.class.getSimpleName(), atmId);
                    viewHolder.distanceTextView.setText(Utils.asDistance(distance));
                }
            } else {
                if (bank.getDepartments() != null) {
                    String address = bank.getDepartments().get(position - 2).getAddress().getFullAddress();
                    final String depId = bank.getDepartments().get(position - 2).getId();
                    viewHolder.addressTextView.setText(address);

                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Department department = null;
                            try {
                                department = (Department) new DataFetcher(baseFragment, Department.class).one(depId).fetchFromCache().first();
                            } catch (Exception e) {
                            }
                            Bundle mapData = MapDataProcessor.prepareData(null, new MapInfoAdapterBanks(baseFragment.getActivity(), department, bank));
                            baseFragment.gotoFragment(FragmentFactory.MAP, mapData);
                        }
                    });
                    int distance = Settings.getInstance(baseFragment.getActivity()).getStorage().getNearestDistance(Department.class.getSimpleName(), depId);
                    viewHolder.distanceTextView.setText(Utils.asDistance(distance));

                }

            }
            TextView workTimeTextView = (TextView) convertView.findViewById(R.id.nearestDepATMWorkTimeTextViewItem);
            workTimeTextView.setText(String.format(baseFragment.getActivity().getString(R.string.time_of_work), "N/A"));

        }

        return convertView;
    }

    static class ViewHolderDoubleButton {
        @BindView(R.id.doubleButtonFirstButton)
        Button firstButton;

        @BindView(R.id.doubleButtonSecondButton)
        Button secondButton;

        public ViewHolderDoubleButton(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class ViewHolderBankInfo {
        @BindView(R.id.bankViewItem)
        BankView bankView;

        public ViewHolderBankInfo(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class ViewHolderNearestInfo {
        @BindView(R.id.nearestDepATMTitleTextViewItem)
        TextView titleTextView;

        @BindView(R.id.nearestDepATMAddressTextViewItem)
        TextView addressTextView;

        @BindView(R.id.stickerFlagNearestDepATMDistanceTextViewItem)
        TextView distanceTextView;

        public ViewHolderNearestInfo(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
