package ru.webmechanic.moneycare.ui.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.widget.DrawerLayout;
import android.widget.ListView;

import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.Settings;

/**
 * Created by a.bratusenko on 27.05.16.
 * Класс для переключения состояния навигационной панели
 */
public class NavigationDrawerStateManager {

    private Context context;
    private DrawerLayout drawerLayout;
    private ListView listView;

    public NavigationDrawerStateManager(Context context, DrawerLayout drawerLayout, ListView listView) {
        this.context = context;
        this.drawerLayout = drawerLayout;
        this.listView = listView;
    }

    /**
     * Переключает состояние панели, в зависимости от наличия интернета и статуса авторизации пользователя
     *
     * @param state
     */
    public void changeState(String state) {
        String[] stringValues;
        TypedArray imgIdValues;
        if (!Settings.getInstance(context).isInternetAvailable()) {
            stringValues = context.getResources().getStringArray(R.array.no_internet_connection_navigation_drawer_string_array);
            imgIdValues = null;
        } else if (state.equals(Settings.USER_STATE_LOGGED)) {
            stringValues = context.getResources().getStringArray(R.array.user_logged_navigation_drawer_string_array);
            imgIdValues = context.getResources().obtainTypedArray(R.array.user_logged_navigation_drawer_imgid_array);
        } else {
            stringValues = context.getResources().getStringArray(R.array.user_guest_navigation_drawer_string_array);
            imgIdValues = context.getResources().obtainTypedArray(R.array.user_guest_navigation_drawer_imgid_array);
        }
        listView.setAdapter(new NavigationDrawerListAdapter(context, stringValues, imgIdValues));
    }

    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }
}
