package ru.webmechanic.moneycare;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

/**
 * Created by a.bratusenko on 20.06.16.
 * Класс,опеределяющий положение пользователя
 */
public class LocationManager {

    private String locationPermission = Manifest.permission.ACCESS_FINE_LOCATION;
    private Activity activity;
    private android.location.LocationManager locationManager;
    private Settings settings;

    public LocationManager(Activity activity) {
        this.activity = activity;
        this.settings = Settings.getInstance(activity);

        locationManager = (android.location.LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
    }

    /**
     * Возвращает последнее лучшее по точности положение пользователя
     */
    public void requestBestLocation() {
        if (checkLocationPermission()) {

            Criteria criteria = new Criteria();
            criteria.setSpeedRequired(true);
            String provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);
            if (location != null) {
                settings.setLatitude(Double.toString(location.getLatitude()));
                settings.setLongitude(Double.toString(location.getLongitude()));
            }

        } else {
            ActivityCompat.requestPermissions(activity,
                    new String[]{locationPermission},
                    10);


        }
    }

    /**
     * Проверка разрешения определения местоположения
     *
     * @return истина если разрешение имеется
     */
    private boolean checkLocationPermission() {
        String permission = locationPermission;
        int res = activity.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    /**
     * устанавливает слушателя на изменение положения/определения геолокации пользователя
     * запускает процесс определения местоположения если это возможно
     *
     * @param listener
     */

    public void listen(final LocationChangeListener listener) {
        if (checkLocationPermission()) {
            locationManager = (android.location.LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            locationManager.requestLocationUpdates(provider, 0, 1, new LocationListener() {

                public void onLocationChanged(Location location) {

                    if (checkLocationPermission()) {
                        locationManager.removeUpdates(this);
                        if (listener != null) {
                            listener.onLocationReceived(location);
                        }
                    }
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            });
        } else {
            ActivityCompat.requestPermissions(activity,
                    new String[]{locationPermission},
                    10);
        }

    }

    public interface LocationChangeListener {
        void onLocationReceived(Location location);
    }
}
