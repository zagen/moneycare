package ru.webmechanic.moneycare.maputils.mapinfoadapters;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.PaymentSystem;
import ru.webmechanic.moneycare.content.Terminal;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.maputils.Balloon;
import ru.webmechanic.moneycare.maputils.MapInfoAdapter;

/**
 * Created by a.bratusenko on 15.06.16.
 * Извлечение данных для карты из списка платежных систем
 */
public class MapInfoAdapterPaymentSystems extends MapInfoAdapter {
    private double longitude;
    private Context context;
    private double latitude;
    private Result<PaymentSystem> paymentSystems;
    private List<Balloon> balloons = new ArrayList<>();
    private boolean oneActive = false;

    public MapInfoAdapterPaymentSystems(Context context, double longitude, double latitude, Result<PaymentSystem> paymentSystems) {
        this.context = context;
        this.longitude = longitude;
        this.latitude = latitude;
        this.paymentSystems = paymentSystems;
        for (int i = 0; i < paymentSystems.size(); i++) {
            for (Terminal terminal : paymentSystems.getItem(i).getTerminals()) {
                Balloon balloon = new Balloon();
                balloon.setAddress(terminal.getAddress().getFullAddress());
                balloon.setTitle(context.getString(R.string.terminal) + " \"" + paymentSystems.getItem(i).getTitle() + "\"");
                balloon.setLongitude(Double.parseDouble(terminal.getAddress().getLongitude()));
                balloon.setLatitude(Double.parseDouble(terminal.getAddress().getLatitude()));
                balloons.add(balloon);
            }
        }
    }

    public MapInfoAdapterPaymentSystems(Context context, Terminal terminal, Result<PaymentSystem> paymentSystems) {
        this(context, Double.parseDouble(terminal.getAddress().getLongitude()), Double.parseDouble(terminal.getAddress().getLatitude()), paymentSystems);
        oneActive = true;

    }

    @Override
    public double getLongitude() {
        return longitude;
    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    @Override
    public List<Balloon> getBalloons() {
        return balloons;
    }

    @Override
    public boolean isOneActive() {
        return oneActive;
    }

    @Override
    public String getName() {
        return context.getString(R.string.terminals_on_map);
    }
}
