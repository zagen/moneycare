package ru.webmechanic.moneycare.api.deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import ru.webmechanic.moneycare.content.NearestOutlet;
import ru.webmechanic.moneycare.content.Outlet;

/**
 * Created by a.bratusenko on 30.05.16.
 * Класс, осуществляющий десериализацию ближайших точек продаж
 */
public class NearestOutletDeserializer implements JsonDeserializer<NearestOutlet> {
    @Override
    public NearestOutlet deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        NearestOutlet nearestOutlet = new NearestOutlet();
        JsonObject jsonOutlet = (JsonObject) json;

        int distance;
        try {
            distance = jsonOutlet.get("distance").getAsInt();
        } catch (Exception e) {
            distance = 0;
        }
        Outlet outlet;
        try {
            outlet = new OutletDeserializer().deserialize(json, typeOfT, context);
        } catch (Exception e) {
            outlet = null;
        }
        nearestOutlet.setDistance(distance);
        nearestOutlet.setOutlet(outlet);
        return nearestOutlet;

    }
}
