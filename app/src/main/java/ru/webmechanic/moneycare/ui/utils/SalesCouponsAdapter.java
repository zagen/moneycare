package ru.webmechanic.moneycare.ui.utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.content.SalesCoupon;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.ui.customviews.SalesCouponItemView;

/**
 * Created by a.bratusenko on 30.05.16.
 */
public class SalesCouponsAdapter extends BaseAdapter {
    protected Settings settings;
    private Result<SalesCoupon> salesCouponsResult;
    private LayoutInflater inflater;

    public SalesCouponsAdapter(Context context, Result<SalesCoupon> result) {
        this.salesCouponsResult = result;
        this.inflater = ((Activity) context).getLayoutInflater();
        this.settings = Settings.getInstance(context);
    }

    @Override
    public int getCount() {
        return salesCouponsResult.size();
    }

    @Override
    public Object getItem(int position) {
        return salesCouponsResult.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_sales_coupon_layout, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.salesCouponView.setSalesCoupon(this.salesCouponsResult.getItem(position));


        final String couponId = this.salesCouponsResult.getItem(position).getId();

        updateSticker(viewHolder.salesCouponView.getSticker(), couponId);
        viewHolder.salesCouponView.getSticker().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!settings.getStorage().isSalesCouponSaved(couponId)) {
                    settings.getStorage().markSalesCoupon(couponId, !settings.getStorage().isSalesCouponMarked(couponId));
                }
                updateSticker((ImageButton) v, couponId);
            }
        });

        return convertView;
    }

    private void updateSticker(ImageButton stickerImageButton, String id) {
        int stickerDrawableId = R.drawable.sticker;
        if (settings.getStorage().isSalesCouponSaved(id)) {
            stickerDrawableId = R.drawable.sticker_saved;
        } else if (settings.getStorage().isSalesCouponMarked(id)) {
            stickerDrawableId = R.drawable.sticker_marked;
        }
        stickerImageButton.setImageResource(stickerDrawableId);
    }

    static class ViewHolder {
        @BindView(R.id.salesCouponView)
        SalesCouponItemView salesCouponView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
