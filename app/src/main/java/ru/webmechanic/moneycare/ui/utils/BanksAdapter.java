package ru.webmechanic.moneycare.ui.utils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Bank;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.ui.BaseFragment;
import ru.webmechanic.moneycare.ui.FragmentFactory;
import ru.webmechanic.moneycare.ui.customviews.BankView;

/**
 * Created by a.bratusenko on 07.06.16.
 * Адаптер для отображения списка банков
 */
public class BanksAdapter extends BaseAdapter {
    private Result<Bank> bankResult;
    private BaseFragment baseFragment;


    public BanksAdapter(BaseFragment baseFragment, Result<Bank> bankResult) {
        this.baseFragment = baseFragment;
        this.bankResult = bankResult;
    }

    @Override
    public int getCount() {
        return bankResult.size();
    }

    @Override
    public Object getItem(int position) {
        return bankResult.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(baseFragment.getActivity()).inflate(R.layout.list_item_bank, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Bank bank = bankResult.getItem(position);
        final String bankId = bank.getId();

        viewHolder.bankView.setBank(bank);
        viewHolder.bankView.setOnDepartmentClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Arguments args = new Arguments();
                args.setId(bankId);
                args.setShowAtm(false);
                args.setContract(baseFragment.getArgs().getContract());
                baseFragment.gotoFragment(FragmentFactory.BANK, args);

            }
        });
        viewHolder.bankView.setOnAtmClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Arguments args = new Arguments();
                args.setId(bankId);
                args.setShowAtm(true);
                args.setContract(baseFragment.getArgs().getContract());
                baseFragment.gotoFragment(FragmentFactory.BANK, args);
            }
        });
        viewHolder.bankView.setOnCommissionClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Arguments args = new Arguments();
                args.setId(bankId);
                args.setShowAtm(true);
                args.setContract(baseFragment.getArgs().getContract());
                baseFragment.gotoFragment(FragmentFactory.BANK, args);
            }
        });

        return convertView;
    }


    static class ViewHolder {
        @BindView(R.id.bankViewItem)
        BankView bankView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
