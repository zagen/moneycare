package ru.webmechanic.moneycare.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.content.User;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.ui.utils.RussiaCellPhoneNumberTextWatcher;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class RegisterFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener {

    @BindView(R.id.passwordRegisterEditText)
    EditText passwordEditText;

    @BindView(R.id.passwordRepeatRegisterEditText)
    EditText passwordRepeatEditText;

    @BindView(R.id.phoneRegisterEditText)
    EditText phoneEditText;

    @BindView(R.id.doRegisterButton)
    Button doRegisterButton;

    public RegisterFragment() {
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.app_name);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);

        phoneEditText.addTextChangedListener(new RussiaCellPhoneNumberTextWatcher(phoneEditText));

        return view;
    }

    protected void updateUI() {
        super.updateUI();
        this.doRegisterButton.setEnabled(!isLoading);
    }

    @Override
    protected void fetchData() {
    }

    private boolean isInputCorrect() {
        String phone = phoneEditText.getText().toString().replaceAll("[^\\d]", "");
        return phone.trim().length() == 11 &&
                passwordEditText.getText().toString().trim().length() >= 1 &&
                passwordEditText.getText().toString().equals(passwordRepeatEditText.getText().toString());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @OnClick(R.id.gotoLoginRegisterButton)
    void login() {
        gotoBack();
    }

    @OnClick(R.id.doRegisterButton)
    void register() {
        if (!isInputCorrect()) {
            Toast.makeText(getActivity(), getString(R.string.fill_fields), Toast.LENGTH_SHORT).show();

        } else if (!settings.isInternetAvailable()) {
            onDataFetchedError(300, "");
        } else {
            hideKeyboard();
            arguments.setPhone(phoneEditText.getText().toString().replaceAll("[^\\d]", "").substring(1));
            arguments.setPassword(passwordEditText.getText().toString());
            isLoading = true;
            updateUI();
            new DataFetcher(this, loaderContext, this).fetchFromWeb();

        }

    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        User user = (User) result.first();
        if (user != null) {
            settings.setPhone(user.getPhone());
            settings.setUserToken(user.getToken());

            if (user.getStatus() == 1) {
                settings.setUserState(Settings.USER_STATE_LOGGED);
            } else {
                settings.setUserState(Settings.USER_STATE_PIN_AWAIT);
            }

            gotoFragment(FragmentFactory.MAIN_MENU);
        }
        arguments.setPassword(null);
        arguments.setPhone(null);
        isLoading = false;
        updateUI();

    }

    @Override
    public void onDataFetchedError(int code, String message) {
        if (code == 417) {
            Toast.makeText(getActivity(), "Не удалось зарегистрироваться, возможно такой телефон уже есть в базе данных", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Не удалось зарегистрироваться, сетевая ошибка", Toast.LENGTH_SHORT).show();
        }
        isLoading = false;
        updateUI();
    }
}
