package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;
import java.util.List;

import retrofit.Call;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.PaymentSystem;

/**
 * Created by a.bratusenko on 27.05.16.
 */
public class PaymentSystemsLoader extends BaseLoader {
    public PaymentSystemsLoader(Context context, Bundle args) {
        super(context);
    }

    @Override
    protected Response apiCall() throws IOException {
        Response response = new Response();
        List<PaymentSystem> paymentSystems = null;
        Settings settings = Settings.getInstance(getContext());
        String[] params = settings.prepareParams();

        Call<List<PaymentSystem>> call = service.paymentSystems(params[0], params[1], params[2], params[3], params[4]);
        retrofit.Response<List<PaymentSystem>> retrofitResponse = call.execute();

        if (retrofitResponse != null) {
            if (!retrofitResponse.isSuccess()) {
                response.setCode(retrofitResponse.code());
                if (retrofitResponse.errorBody() != null) {
                    response.setErrorMessage(retrofitResponse.message());
                }
            } else {
                paymentSystems = retrofitResponse.body();
            }
        }
        response.setRequestResult(RequestResult.SUCCESS)
                .setAnswer(paymentSystems);
        return response;
    }
}
