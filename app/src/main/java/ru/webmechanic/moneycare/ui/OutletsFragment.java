package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import ru.webmechanic.moneycare.LocationManager;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.NearestOutlet;
import ru.webmechanic.moneycare.content.Outlet;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.maputils.MapDataProcessor;
import ru.webmechanic.moneycare.maputils.mapinfoadapters.MapInfoAdapterNearestOutlets;
import ru.webmechanic.moneycare.maputils.mapinfoadapters.MapInfoAdapterOutlets;
import ru.webmechanic.moneycare.ui.utils.Arguments;
import ru.webmechanic.moneycare.ui.utils.NearestOutletsAdapter;
import ru.webmechanic.moneycare.ui.utils.OutletsAdapter;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class OutletsFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener {


    @BindView(R.id.outletsListView)
    ListView outletsListView;
    @BindView(R.id.nearestOrAllOutletsButton)
    Button switchOutlestListButton;
    @BindView(R.id.noDataTextView)
    TextView noDataTextView;

    private Result<Outlet> outletsResult;
    private Result<NearestOutlet> nearestOutletsResult;

    public OutletsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_outlets, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void fetchData() {
        isLoading = true;
        if (loaderContext.getLoader() == R.id.nearest_outlets_loader && settings.isInternetAvailable()) {
            new DataFetcher(this, loaderContext, this).fetchFromWeb();
        } else {
            new DataFetcher(this, loaderContext, this).fetch();
        }
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.outlets);
    }


    @OnItemClick(R.id.outletsListView)
    void outletSelected(int position) {
        Object object = outletsListView.getAdapter().getItem(position);
        Bundle args = new Bundle();
        if (object instanceof Outlet) {
            Outlet outlet = (Outlet) object;
            args.putString(Arguments.ARG_ID, outlet.getId());
        } else if (object instanceof NearestOutlet) {
            NearestOutlet nearestOutlet = (NearestOutlet) object;
            args.putString(Arguments.ARG_ID, nearestOutlet.getOutlet().getId());
        }
        gotoFragment(FragmentFactory.OUTLET);
    }

    @OnClick(R.id.nearestOrAllOutletsButton)
    void switchNearestAll() {
        if (loaderContext.getLoader() == R.id.nearest_outlets_loader) {
            loaderContext.setLoader(R.id.outlets_loader);
        } else {
            loaderContext.setLoader(R.id.nearest_outlets_loader);
        }
        fetchData();
        updateUI();
    }

    @OnClick(R.id.mapOutletsButton)
    void showMap() {
        Result result = new DataFetcher(this, loaderContext, null).fetchFromCache();
        if (loaderContext.getLoader() == R.id.nearest_outlets_loader) {
            gotoFragment(FragmentFactory.MAP, MapDataProcessor.prepareData(new Bundle(),
                    new MapInfoAdapterNearestOutlets(
                            getActivity(),
                            Double.parseDouble(settings.getLongitude()),
                            Double.parseDouble(settings.getLatitude()),
                            result)));
        } else {
            gotoFragment(FragmentFactory.MAP, MapDataProcessor.prepareData(new Bundle(),
                    new MapInfoAdapterOutlets(
                            getActivity(),
                            Double.parseDouble(settings.getLongitude()),
                            Double.parseDouble(settings.getLatitude()),
                            result)));

        }
    }


    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        nearestOutletsResult = null;
        outletsResult = null;
        if (loaderid == R.id.nearest_outlets_loader) {
            try {
                nearestOutletsResult = (Result<NearestOutlet>) result;
            } catch (Exception e) {
            }
        } else if (loaderid == R.id.outlets_loader) {
            try {
                outletsResult = (Result<Outlet>) result;
            } catch (Exception e) {

            }
        }
        isLoading = false;
        updateUI();
    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        new LocationManager(getActivity()).requestBestLocation();
        int errorStringId = R.string.no_data_outlets;
        switchOutlestListButton.setEnabled(!isLoading);
        if (loaderContext.getLoader() == R.id.outlets_loader) {
            switchOutlestListButton.setText(getString(R.string.nearest));
        } else {
            errorStringId = R.string.no_data_nearest_outlet;
            switchOutlestListButton.setText(getString(R.string.all));
        }
        if (nearestOutletsResult != null && nearestOutletsResult.size() != 0) {
            noDataTextView.setVisibility(View.GONE);
            outletsListView.setAdapter(new NearestOutletsAdapter(getActivity(), nearestOutletsResult));
        } else if (outletsResult != null && outletsResult.size() != 0) {
            noDataTextView.setVisibility(View.GONE);
            outletsListView.setAdapter(new OutletsAdapter(getActivity(), outletsResult));
        } else {
            noDataTextView.setText(errorStringId);
            noDataTextView.setVisibility(View.VISIBLE);
            outletsListView.setAdapter(null);
        }

    }
}
