package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;
import java.util.List;

import retrofit.Call;
import ru.webmechanic.moneycare.api.ApiFactory;
import ru.webmechanic.moneycare.api.ApiService;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.City;

/**
 * Created by a.bratusenko on 23.05.16.
 */
public class CitiesLoader extends BaseLoader {

    public CitiesLoader(Context context, Bundle args) {
        super(context);
    }

    @Override
    protected Response apiCall() throws IOException {
        Response response = new Response();
        List<City> cities = null;

        ApiService service = ApiFactory.getApiService();
        Call<List<City>> call = service.activeCities();

        retrofit.Response<List<City>> retrofitResponse = call.execute();

        if (retrofitResponse != null) {
            if (!retrofitResponse.isSuccess()) {
                response.setCode(retrofitResponse.code());
                if (retrofitResponse.errorBody() != null) {
                    response.setErrorMessage(retrofitResponse.message());
                }
            } else {
                cities = retrofitResponse.body();
            }
        }
        response.setRequestResult(RequestResult.SUCCESS)
                .setAnswer(cities);
        return response;
    }
}
