package ru.webmechanic.moneycare.api.deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import ru.webmechanic.moneycare.content.PaymentSystem;

/**
 * Created by a.bratusenko on 27.05.16.
 * Класс для десериализации отдельной платежной системы.Вычленяет элемент содержащий информацию о платежной системе
 * и делегирует его объекту класса PaymentSystemBasicDeserializer
 *
 * @see PaymentSystemBasicDeserializer
 */
public class PaymentSystemDeserializer implements JsonDeserializer<PaymentSystem> {
    @Override
    public PaymentSystem deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonPs = ((JsonObject) json).getAsJsonObject("ps");
        PaymentSystem ps;
        try {
            ps = new PaymentSystemBasicDeserializer().deserialize(jsonPs, typeOfT, context);
        } catch (Exception e) {
            ps = null;
        }
        return ps;
    }

}
