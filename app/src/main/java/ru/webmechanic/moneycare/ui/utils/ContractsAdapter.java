package ru.webmechanic.moneycare.ui.utils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.ui.BaseFragment;
import ru.webmechanic.moneycare.ui.FragmentFactory;
import ru.webmechanic.moneycare.ui.customviews.ContractView;

/**
 * Created by a.bratusenko on 02.06.16.
 * Адаптер для отображения договоров в виде списка
 */
public class ContractsAdapter extends BaseAdapter {

    private BaseFragment baseFragment;
    private Result<Contract> contracts;
    private LayoutInflater inflater;

    public ContractsAdapter(BaseFragment baseFragment, Result<Contract> contractResult) {
        this.baseFragment = baseFragment;
        this.contracts = contractResult;
        this.inflater = LayoutInflater.from(baseFragment.getActivity());
    }

    @Override
    public int getCount() {
        return contracts.size();
    }

    @Override
    public Object getItem(int position) {
        return contracts.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {

            convertView = inflater.inflate(R.layout.list_item_contract, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Contract contract = contracts.getItem(position);
        final String id = contract.getId();
        viewHolder.contractView.setDetailsVisible(false);

        viewHolder.contractView.setOnGoodsClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Arguments args = new Arguments();
                    args.setId(id);
                    baseFragment.gotoFragment(FragmentFactory.CONTRACT, args);
                } catch (Exception e) {
                }
            }
        });
        viewHolder.contractView.setContract(contract);
        viewHolder.contractView.setFirstButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    baseFragment.gotoFragment(FragmentFactory.CONTRACTS_PAYED);
                } catch (Exception e) {
                }
            }
        });

        viewHolder.contractView.setSecondButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Arguments args = new Arguments();
                    args.setContract(id);
                    baseFragment.gotoFragment(FragmentFactory.SELECT_PAY_METHOD, args);
                } catch (Exception e) {
                }
            }
        });
        viewHolder.contractView.setExpansible(true);
        return convertView;
    }
    static class ViewHolder {
        @BindView(R.id.contractViewItem)
        ContractView contractView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


}
