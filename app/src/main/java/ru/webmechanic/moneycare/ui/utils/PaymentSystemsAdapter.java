package ru.webmechanic.moneycare.ui.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.PaymentSystem;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.ui.customviews.PaymentSystemView;

/**
 * Created by a.bratusenko on 08.06.16.
 */
public class PaymentSystemsAdapter extends BaseAdapter {
    private Result<PaymentSystem> psResult;
    private Context context;
    private PaymentsSystemAdapterListeners listeners;


    public PaymentSystemsAdapter(Context context, Result<PaymentSystem> paymentSystemResult) {
        this.psResult = paymentSystemResult;
        this.context = context;
    }

    @Override
    public int getCount() {
        return psResult.size();
    }

    @Override
    public Object getItem(int position) {
        return psResult.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_ps, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final String psId = psResult.getItem(position).getId();
        viewHolder.psView.setPaymentSystem(psResult.getItem(position));
        viewHolder.psView.setOnCommissionClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listeners != null) {
                    listeners.onCommissionClicked(psId);
                }
            }
        });
        viewHolder.psView.setOnTerminalsClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listeners != null) {
                    listeners.onTerminalClicked(psId);
                }
            }
        });
        return convertView;
    }

    public void setListeners(PaymentsSystemAdapterListeners listeners) {
        this.listeners = listeners;
    }

    public interface PaymentsSystemAdapterListeners {
        void onTerminalClicked(String psId);

        void onCommissionClicked(String psId);
    }

    static class ViewHolder {
        @BindView(R.id.psViewItem)
        PaymentSystemView psView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
