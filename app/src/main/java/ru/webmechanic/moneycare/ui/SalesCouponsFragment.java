package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import ru.webmechanic.moneycare.LocalStorage;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Category;
import ru.webmechanic.moneycare.content.SalesCoupon;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.ui.customviews.CategoriesView;
import ru.webmechanic.moneycare.ui.utils.Arguments;
import ru.webmechanic.moneycare.ui.utils.SalesCouponsAdapter;
import ru.webmechanic.moneycare.ui.utils.Utils;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class SalesCouponsFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener {

    private final static String CURRENT_CATEGORY = "current category";

    @BindView(R.id.salesCouponsListView)
    ListView couponsListView;

    @BindView(R.id.noDataTextView)
    TextView noDataTextView;

    @BindView(R.id.categoriesViewSalesCoupons)
    CategoriesView categoriesView;

    private Result<SalesCoupon> salesCouponResult;
    private Result<Category> categoryResult;

    @Override
    protected void fetchData() {
        isLoading = true;
        if (loaderContext.getLoader() == R.id.categories_loader) {
            new DataFetcher(SalesCouponsFragment.this, loaderContext, SalesCouponsFragment.this).fetch();
        } else {
            new DataFetcher(this, loaderContext, this).containes("subjectCategory", categoriesView.getCurrentCategory()).fetch();
        }
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.sales_coupons);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sales_coupons, container, false);
        ButterKnife.bind(this, view);

        categoriesView.setCategorySelectedListener(new CategoriesView.OnCategorySelectedListener() {
            @Override
            public void onCategorySelected(String category) {
                loaderContext.setLoader(R.id.sales_coupons_loader);
                fetchData();
                updateUI();
            }
        });
        categoriesView.setShowAllCategoriesListener(new CategoriesView.OnShowAllCategoriesListener() {
            @Override
            public void onShowAllCategory() {
                loaderContext.setLoader(R.id.categories_loader);
                fetchData();
                updateUI();
            }
        });
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(CURRENT_CATEGORY)) {
                String currentCategory = savedInstanceState.getString(CURRENT_CATEGORY);
                categoriesView.setCurrentCategory(currentCategory);
            }
        }

        return view;
    }

    @OnItemClick(R.id.salesCouponsListView)
    void couponSelected(AdapterView<?> parent, int position) {
        SalesCoupon coupon = (SalesCoupon) parent.getAdapter().getItem(position);
        if (!Utils.isFixed(settings, coupon)) {
            Arguments args = new Arguments();
            args.setId(coupon.getId());
            gotoFragment(FragmentFactory.SALES_COUPON, args);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (categoriesView != null && categoriesView.getCurrentCategory() != null) {
            outState.putString(CURRENT_CATEGORY, categoriesView.getCurrentCategory());
        }
    }

    @OnClick(R.id.salesCouponsSaveMarkedButton)
    void saveMarked() {
        LocalStorage storage = settings.getStorage();
        if (storage.hasMarkedSalesCoupons()) {
            storage.saveAllMarkedSalesCoupons();
            gotoFragment(FragmentFactory.SAVED_SALES_COUPONS);
        }
    }

    @OnClick(R.id.salesCouponsSavedButton)
    void showSaved() {
        gotoFragment(FragmentFactory.SAVED_SALES_COUPONS);
    }

    @OnClick(R.id.salesCouponsFilterButton)
    void filter() {
        if (!categoriesView.isVisible()) {
            loaderContext.setLoader(R.id.categories_loader);
            fetchData();
        } else {
            if (categoriesView.getCurrentCategory() != null && categoriesView.getCategories() == null) {
                loaderContext.setLoader(R.id.categories_loader);
                fetchData();
            }
        }
        updateUI();
    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        categoryResult = null;
        salesCouponResult = null;
        if (loaderid == R.id.sales_coupons_loader) {
            try {
                this.salesCouponResult = (Result<SalesCoupon>) result;
            } catch (Exception e) {

            }
        } else if (loaderid == R.id.categories_loader) {
            try {
                this.categoryResult = (Result<Category>) result;
            } catch (Exception e) {

            }
        }
        isLoading = false;
        updateUI();
    }

    @Override
    public void onDataFetchedError(int code, String message) {
        categoryResult = null;
        salesCouponResult = null;
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        if (isLoading)
            return;
        if (salesCouponResult != null && salesCouponResult.size() != 0) {
            noDataTextView.setVisibility(View.GONE);
            couponsListView.setAdapter(new SalesCouponsAdapter(getActivity(), salesCouponResult));
        } else if (categoryResult != null && categoryResult.size() != 0) {
            couponsListView.setAdapter(null);
            noDataTextView.setVisibility(View.GONE);
            noDataTextView.setVisibility(View.GONE);
            categoriesView.setCategories(categoryResult);
        } else {
            couponsListView.setAdapter(null);
            if (categoriesView.getCurrentCategory() != null) {
                noDataTextView.setText(R.string.no_data_sales_coupons_categories);
            } else {
                noDataTextView.setText(R.string.no_data_sales_coupons);
            }
            noDataTextView.setVisibility(View.VISIBLE);
        }
    }
}
