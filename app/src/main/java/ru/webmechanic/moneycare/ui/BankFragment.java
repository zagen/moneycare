package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.LocationManager;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Bank;
import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.ui.customviews.ContractView;
import ru.webmechanic.moneycare.ui.utils.BankAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class BankFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener {

    @BindView(R.id.bankListView)
    ListView listView;
    @BindView(R.id.bankContractView)
    ContractView contractView;

    private Bank bank = null;


    public BankFragment() {
        // Required empty public constructor
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.bank);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bank, container, false);

        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void fetchData() {
        isLoading = true;
        new DataFetcher(this, loaderContext, this).one(arguments.getId()).fetch();
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        new LocationManager(getActivity()).requestBestLocation();
        if (arguments.isSetContract()) {
            Contract contract = (Contract) new DataFetcher(this, Contract.class).one(arguments.getContract()).fetchFromCache().first();
            if (contract != null) {
                contractView.setContract(contract);
                contractView.setVisibility(View.VISIBLE);
            } else {
                contractView.setVisibility(View.GONE);
            }
        } else {
            contractView.setVisibility(View.GONE);
        }
        if (isLoading) {
            return;
        }

        if (bank != null) {
            listView.setAdapter(new BankAdapter(this, bank, arguments.isShowAtm()));
        }
    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        if (loaderid == R.id.bank_loader) {

            Result<Bank> bankResult = null;
            try {
                bankResult = (Result<Bank>) result;
            } catch (Exception exception) {
            }
            if (bankResult != null && bankResult.size() != 0) {
                this.bank = (Bank) result.first();
            }


        }
        isLoading = false;
        updateUI();
    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }
}
