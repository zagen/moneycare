package ru.webmechanic.moneycare.api.deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;

import ru.webmechanic.moneycare.content.Address;
import ru.webmechanic.moneycare.content.Outlet;

/**
 * Created by a.bratusenko on 30.05.16.
 * Класс,  осуществляющий десериализацию класса данных точки продаж.
 * Необходим поскольку используется в десериализации ближайших точек
 */
public class OutletDeserializer implements JsonDeserializer<Outlet> {
    @Override
    public Outlet deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Outlet outlet = new Outlet();
        JsonObject jsonOutlet = (JsonObject) json;

        String id, organization, title;

        try {
            id = jsonOutlet.get("id").getAsString();
        } catch (Exception e) {
            id = null;
        }
        outlet.setId(id);
        Address address;
        try {
            address = new AddressDeserializer().deserialize(jsonOutlet.get("address").getAsJsonObject(), typeOfT, context);
        } catch (Exception e) {
            address = null;
        }
        outlet.setAddress(address);

        try {
            title = jsonOutlet.get("title").getAsString();
        } catch (Exception e) {
            title = null;
        }
        outlet.setTitle(title);

        try {
            organization = jsonOutlet.get("organization").getAsString();
        } catch (Exception e) {
            organization = null;

        }
        outlet.setOrganization(organization);

        try {
            outlet.setPhone(jsonOutlet.get("phone").getAsString());
        } catch (Exception e) {
            outlet.setPhone(null);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
        try {
            outlet.setCreatingDate(sdf.parse(jsonOutlet.get("created_at").getAsString()));
        } catch (Exception e) {
            outlet.setCreatingDate(null);
        }
        try {
            outlet.setUpdatingDate(sdf.parse(jsonOutlet.get("updated_at").getAsString()));
        } catch (Exception e) {
            outlet.setUpdatingDate(null);
        }

        return outlet;
    }
}
