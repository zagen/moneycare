package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;
import java.util.List;

import retrofit.Call;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.Category;

/**
 * Created by a.bratusenko on 27.05.16.
 */
public class CategoriesLoader extends BaseLoader {
    public CategoriesLoader(Context context, Bundle args) {
        super(context);
    }

    @Override
    protected Response apiCall() throws IOException {
        Response response = new Response();
        Settings settings = Settings.getInstance(getContext());

        String[] params = settings.prepareParams();
        Call<List<Category>> call = service.categories(params[0], params[1], params[2], params[3], params[4]);
        List<Category> categories = call.execute().body();


        return response.setRequestResult(RequestResult.SUCCESS)
                .setAnswer(categories);
    }
}
