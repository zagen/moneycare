package ru.webmechanic.moneycare.maputils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by a.bratusenko on 15.06.16.
 * Класс данных содержащих информацию для отображения одной точки на карте.
 */
public class Balloon {
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("latitude")
    private double latitude;
    @SerializedName("title")
    private String title;
    @SerializedName("imageUrl")
    private String imageUrl;
    @SerializedName("address")
    private String address;
    @SerializedName("phone")
    private String phone;
    @SerializedName("site")
    private String site;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (getTitle() != null && getTitle().length() > 0) {
            sb.append("<big>");
            sb.append(getTitle());
            sb.append("</big><br>");
        }
        if (getAddress() != null && getAddress().length() > 0) {
            sb.append("<b>Адрес:</b> ");
            sb.append(getAddress());
            sb.append("<br>");
        }
        if (getPhone() != null && getPhone().length() > 0) {
            sb.append("<b>Телефон:</b> +7");
            sb.append(getPhone());
        }

        return sb.toString();
    }
}
