package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;
import java.util.List;

import retrofit.Call;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.Bank;

/**
 * Created by a.bratusenko on 26.05.16.
 */
public class BanksLoader extends BaseLoader {
    public BanksLoader(Context context, Bundle args) {
        super(context);
    }

    @Override
    protected Response apiCall() throws IOException {
        Response response = new Response();
        Settings settings = Settings.getInstance(getContext());
        List<Bank> banks = null;

        String[] params = settings.prepareParams();
        Call<List<Bank>> call = service.banks(params[0], params[1], params[2], params[3], params[4]);
        retrofit.Response<List<Bank>> retrofitResponse = call.execute();

        if (retrofitResponse != null) {
            if (!retrofitResponse.isSuccess()) {
                response.setCode(retrofitResponse.code());
                if (retrofitResponse.errorBody() != null) {
                    response.setErrorMessage(retrofitResponse.message());
                }
            } else {
                banks = retrofitResponse.body();
            }
        }
        response.setRequestResult(RequestResult.SUCCESS)
                .setAnswer(banks);
        return response;
    }
}
