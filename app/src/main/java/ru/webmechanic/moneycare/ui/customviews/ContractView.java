package ru.webmechanic.moneycare.ui.customviews;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.content.Goods;

/**
 * Created by a.bratusenko on 07.06.16.
 */
public class ContractView extends RelativeLayout {
    @BindView(R.id.dateContractTextViewItem)
    TextView dateTextView;

    @BindView(R.id.contractTitleTextViewItem)
    TextView titleTextView;

    @BindView(R.id.stickerFlagContractAmountTextViewItem)
    TextView contractAmountTextView;

    @BindView(R.id.detailesContractsContainerLinearLayout)
    LinearLayout detailsLinearLayout;

    @BindView(R.id.buttonsPanelContract)
    LinearLayout buttonsPanel;

    @BindView(R.id.firstButtonContract)
    Button firstButton;

    @BindView(R.id.secondButtonContract)
    Button secondButton;

    private Settings settings;
    private boolean isExpansible;
    private Contract contract;

    private OnClickListener onGoodsClickListener = null;


    public ContractView(Context context) {
        super(context);
        init();
    }

    public ContractView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ContractView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setFirstButtonOnClickListener(final OnClickListener firstButtonOnClickListener) {

        firstButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (contract != null) {
                    new AlertDialog.Builder(getContext())
                            .setTitle(R.string.confirmation)
                            .setMessage(R.string.are_you_sure_move_contract)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    final String contractId = contract.getId();
                                    settings.getStorage().markContractAsPayed(contractId, true);
                                    if (firstButtonOnClickListener != null) {
                                        firstButtonOnClickListener.onClick(v);
                                    }

                                }
                            })
                            .setNegativeButton(R.string.cancel, null)
                            .show();
                }
            }
        });
    }

    public void setSecondButtonOnClickListener(final OnClickListener secondButtonOnClickListener) {
        secondButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (secondButtonOnClickListener != null) {
                    v.setTag(contract.getId());
                    secondButtonOnClickListener.onClick(v);
                }
            }
        });
    }

    private void init() {
        settings = Settings.getInstance(getContext());
        inflate(getContext(), R.layout.customviews_contract, this);
        ButterKnife.bind(this, this);
    }

    public void setExpansible(boolean expansible) {
        isExpansible = expansible;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        try {
            this.contract = contract;
            titleTextView.setText("№ " + contract.getId());
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            dateTextView.setText(sdf.format(contract.getCreatingDate()) + " - " + sdf.format(contract.getEndDate()));

            contractAmountTextView.setText(contract.getAmount() + " Р");
            putGoods();
        } catch (Exception e) {
        }
    }

    @OnClick(R.id.contentContainerContractsLinearLayout)
    void expanse() {
        if (isExpansible) {
            detailsLinearLayout.setVisibility(detailsLinearLayout.getVisibility() == GONE ? VISIBLE : GONE);
        }

    }

    public void setDetailsVisible(boolean visible) {
        detailsLinearLayout.setVisibility(visible ? VISIBLE : GONE);

    }

    private void putGoods() {
        detailsLinearLayout.removeViews(0, detailsLinearLayout.getChildCount() - 1);

        if (contract != null && contract.getGoods() != null) {
            for (int i = 0; i < contract.getGoods().size(); i++) {
                Goods goods = contract.getGoods().get(i);
                String details = String.format("%s / %s \n%s", goods.getGroup(), goods.getGoods(), goods.getBrand());
                View view = inflate(getContext(), R.layout.list_item_goods_layout, null);
                final TextView detailsTextView = (TextView) view.findViewById(R.id.goodsTextViewItem);
                detailsTextView.setText(details);
                if (i == contract.getGoods().size() - 1) {
                    ImageView separatorImageView = (ImageView) view.findViewById(R.id.goodsSeparatorImageView);
                    separatorImageView.setVisibility(GONE);
                }
                view.setOnClickListener(onGoodsClickListener);

                detailsLinearLayout.addView(view, detailsLinearLayout.getChildCount() - 1);
            }
        }

    }

    public void setOnGoodsClickListener(OnClickListener onGoodsClickListener) {
        this.onGoodsClickListener = onGoodsClickListener;
    }

    public void setButtonsShown(boolean buttonsShown) {
        buttonsPanel.setVisibility(buttonsShown ? VISIBLE : GONE);
    }
}
