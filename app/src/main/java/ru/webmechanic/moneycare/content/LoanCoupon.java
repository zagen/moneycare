package ru.webmechanic.moneycare.content;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import ru.webmechanic.moneycare.content.LocalData.LoanCouponFixData;

/**
 * Created by a.bratusenko on 25.05.16.
 * Класс данных "Акция".
 * Содержит всю информацию об акции, в том числе о точках продаж где она применима, и информацию о погашении пользователем
 */
public class LoanCoupon extends RealmObject {

    @SerializedName("id")
    private String id;

    @SerializedName("val")
    private String value;

    @SerializedName("bank")
    private String bank;

    @SerializedName("subject_category")
    private String subjectCategory;

    @SerializedName("subject_type")
    private String subjectType;

    @SerializedName("subject")
    private String subject;

    @SerializedName("image")
    private String imagePath;

    @SerializedName("code")
    private String code;

    @SerializedName("active")
    private boolean active;

    @SerializedName("description")
    private String description;

    @SerializedName("created_at")
    private Date creatingDate;

    @SerializedName("updated_at")
    private Date updatingDate;

    @SerializedName("end_date")
    private Date endDate;

    @SerializedName("outlets")
    private RealmList<Outlet> outlets;

    @SerializedName("loan_fixeds")
    private RealmList<LoanCouponFixData> fixDatas;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getSubjectCategory() {
        return subjectCategory;
    }

    public void setSubjectCategory(String subjectCategory) {
        this.subjectCategory = subjectCategory;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Date getCreatingDate() {
        return creatingDate;
    }

    public void setCreatingDate(Date creatingDate) {
        this.creatingDate = creatingDate;
    }

    public Date getUpdatingDate() {
        return updatingDate;
    }

    public void setUpdatingDate(Date updatingDate) {
        this.updatingDate = updatingDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public RealmList<Outlet> getOutlets() {
        return outlets;
    }

    public void setOutlets(RealmList<Outlet> outlets) {
        this.outlets = outlets;
    }

    public String getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(String subjectType) {
        this.subjectType = subjectType;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }


    public RealmList<LoanCouponFixData> getFixDatas() {
        return fixDatas;
    }

    public void setFixDatas(RealmList<LoanCouponFixData> fixDatas) {
        this.fixDatas = fixDatas;
    }
}
