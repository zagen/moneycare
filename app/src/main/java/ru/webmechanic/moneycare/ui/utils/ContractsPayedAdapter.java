package ru.webmechanic.moneycare.ui.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.ui.BaseFragment;

/**
 * Created by a.bratusenko on 07.06.16.
 * Адаптер для отображения списка оплаченных контрактов
 */
public class ContractsPayedAdapter extends ContractsAdapter {


    public ContractsPayedAdapter(BaseFragment baseFragment, Result<Contract> contractResult) {
        super(baseFragment, contractResult);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public Object getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = super.getView(position, convertView, parent);
        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.contractView.setButtonsShown(false);
        viewHolder.contractView.setExpansible(true);
        return convertView;
    }


}
