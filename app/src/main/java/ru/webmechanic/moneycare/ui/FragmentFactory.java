package ru.webmechanic.moneycare.ui;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;

import java.util.HashMap;

import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.ui.utils.Arguments;

/**
 * Created by a.bratusenko on 25.05.16.
 */
public class FragmentFactory {
    public final static String LOGIN = "login";
    public final static String MAIN_MENU = "main menu";
    public final static String PIN = "pin";
    public final static String REGISTER = "register";
    public final static String SELECT_CITY = "select city";
    public final static String SALES_COUPONS = "sales coupons";
    public final static String SALES_COUPON = "sales coupon";
    public final static String SAVED_SALES_COUPONS = "saved sales coupons";
    public final static String OUTLETS = "outlets";
    public final static String OUTLET = "outlet";
    public final static String LOAN_COUPONS = "loan coupons";
    public final static String SAVED_LOAN_COUPONS = "saved loan coupons";
    public final static String LOAN_COUPON = "loan coupon";

    public final static String SELECT_PAY_METHOD = "select pay method";
    public final static String CONTRACTS = "contracts";
    public final static String THANKS = "thanks";
    public final static String CONTRACT = "contract";
    public final static String CONTRACTS_PAYED = "contract payed";
    public final static String BANKS = "banks";
    public final static String BANK = "bank";
    public final static String PAYMENT_SYSTEMS = "payment systems";
    public final static String PAYMENT_SYSTEM = "payment system";
    public final static String SEARCH = "search";
    public final static String MAP = "map";


    private Context context;
    private HashMap<String, String> fragmentClasses = new HashMap<>();
    private HashMap<String, Integer> fragmentLoaders = new HashMap<>();

    {
        fragmentClasses.put(MAIN_MENU, MainMenuFragment.class.getName());
        fragmentClasses.put(SELECT_CITY, SelectCityFragment.class.getName());
        fragmentClasses.put(LOGIN, LoginFragment.class.getName());
        fragmentClasses.put(PIN, PinFragment.class.getName());
        fragmentClasses.put(REGISTER, RegisterFragment.class.getName());
        fragmentClasses.put(SALES_COUPONS, SalesCouponsFragment.class.getName());
        fragmentClasses.put(SALES_COUPON, SalesCouponFragment.class.getName());
        fragmentClasses.put(OUTLETS, OutletsFragment.class.getName());
        fragmentClasses.put(OUTLET, OutletFragment.class.getName());
        fragmentClasses.put(LOAN_COUPON, LoanCouponFragment.class.getName());
        fragmentClasses.put(LOAN_COUPONS, LoanCouponsFragment.class.getName());
        fragmentClasses.put(SELECT_PAY_METHOD, PaymentMethodsFragment.class.getName());
        fragmentClasses.put(CONTRACTS, ContractsFragment.class.getName());
        fragmentClasses.put(CONTRACTS_PAYED, ContractsPayedFragment.class.getName());
        fragmentClasses.put(CONTRACT, ContractFragment.class.getName());
        fragmentClasses.put(THANKS, ThanksFragment.class.getName());
        fragmentClasses.put(SAVED_LOAN_COUPONS, SavedLoanCouponsFragment.class.getName());
        fragmentClasses.put(SAVED_SALES_COUPONS, SavedSalesCouponFragment.class.getName());
        fragmentClasses.put(BANKS, BanksFragment.class.getName());
        fragmentClasses.put(BANK, BankFragment.class.getName());
        fragmentClasses.put(PAYMENT_SYSTEM, PaymentSystemFragment.class.getName());
        fragmentClasses.put(PAYMENT_SYSTEMS, PaymentSystemsFragment.class.getName());
        fragmentClasses.put(SEARCH, SearchFragment.class.getName());
        fragmentClasses.put(MAP, MapFragment.class.getName());
    }

    {
        fragmentLoaders.put(MAIN_MENU, R.id.city_loader);
        fragmentLoaders.put(SELECT_CITY, R.id.cities_loader);
        fragmentLoaders.put(LOGIN, R.id.login_loader);
        fragmentLoaders.put(PIN, R.id.resend_pin_loader);
        fragmentLoaders.put(REGISTER, R.id.create_user_loader);
        fragmentLoaders.put(SALES_COUPONS, R.id.sales_coupons_loader);
        fragmentLoaders.put(SALES_COUPON, R.id.sales_coupons_loader);
        fragmentLoaders.put(OUTLETS, R.id.outlets_loader);
        fragmentLoaders.put(OUTLET, R.id.outlet_loader);
        fragmentLoaders.put(LOAN_COUPON, R.id.loan_coupons_loader);
        fragmentLoaders.put(LOAN_COUPONS, R.id.loan_coupons_loader);
        fragmentLoaders.put(SELECT_PAY_METHOD, 0);
        fragmentLoaders.put(CONTRACTS, R.id.contracts_loader);
        fragmentLoaders.put(CONTRACTS_PAYED, R.id.contracts_loader);
        fragmentLoaders.put(CONTRACT, R.id.contracts_loader);
        fragmentLoaders.put(THANKS, 0);
        fragmentLoaders.put(SAVED_LOAN_COUPONS, R.id.loan_coupons_loader);
        fragmentLoaders.put(SAVED_SALES_COUPONS, R.id.sales_coupons_loader);
        fragmentLoaders.put(BANKS, R.id.banks_loader);
        fragmentLoaders.put(BANK, R.id.bank_loader);
        fragmentLoaders.put(PAYMENT_SYSTEM, R.id.payment_system_loader);
        fragmentLoaders.put(PAYMENT_SYSTEMS, R.id.payment_systems_loader);
        fragmentLoaders.put(SEARCH, R.id.sales_coupons_loader);
        fragmentLoaders.put(MAP, 0);
    }

    public FragmentFactory(Context context) {
        this.context = context;
    }

    public BaseFragment createFragment(String fragmentName, Bundle args) {
        BaseFragment fragment = null;
        try {
            args.putInt(Arguments.ARG_LOADER, fragmentLoaders.get(fragmentName));
            fragment = (BaseFragment) Fragment.instantiate(context, fragmentClasses.get(fragmentName), args);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fragment;
    }

}
