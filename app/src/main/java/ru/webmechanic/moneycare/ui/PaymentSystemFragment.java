package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.LocationManager;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.content.PaymentSystem;
import ru.webmechanic.moneycare.content.Terminal;
import ru.webmechanic.moneycare.database.ListResult;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.maputils.MapDataProcessor;
import ru.webmechanic.moneycare.maputils.mapinfoadapters.MapInfoAdapterPaymentSystems;
import ru.webmechanic.moneycare.ui.customviews.ContractView;
import ru.webmechanic.moneycare.ui.utils.PaymentSystemAdapter;

public class PaymentSystemFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener {

    @BindView(R.id.psContractView)
    ContractView contractView;

    @BindView(R.id.psListView)
    ListView listView;

    private PaymentSystem paymentSystem;

    public PaymentSystemFragment() {
        // Required empty public constructor
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.payment_system);
    }

    @Override
    protected void fetchData() {
        isLoading = true;
        new DataFetcher(this, loaderContext, this).one(arguments.getId()).fetch();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment_system, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        new LocationManager(getActivity()).requestBestLocation();
        if (arguments.isSetContract()) {

            Contract contract = null;
            try {
                contract = (Contract) new DataFetcher(this, Contract.class).one(arguments.getContract()).fetchFromCache().first();
            } catch (Exception e) {
            }
            if (contract != null) {
                contractView.setContract(contract);
                contractView.setVisibility(View.VISIBLE);
            } else {
                contractView.setVisibility(View.GONE);
            }
        } else {
            contractView.setVisibility(View.GONE);
        }
        if (isLoading) {
            return;
        }
        if (paymentSystem != null) {
            PaymentSystemAdapter adapter = new PaymentSystemAdapter(getActivity(), paymentSystem);
            adapter.setListener(new PaymentSystemAdapter.PaymentSystemAdapterListener() {
                @Override
                public void OnNeedShowMapForTerminal(String terminalId) {
                    Terminal terminal = (Terminal) new DataFetcher(PaymentSystemFragment.this, Terminal.class)
                            .one(terminalId)
                            .fetchFromCache()
                            .first();
                    gotoFragment(FragmentFactory.MAP,
                            MapDataProcessor.prepareData(new Bundle(),
                                    new MapInfoAdapterPaymentSystems(
                                            getActivity(),
                                            terminal,
                                            new ListResult<>(Arrays.asList(paymentSystem))
                                    )));
                }

                @Override
                public void OnNeedShowMapForAll(String psId) {
                    gotoFragment(FragmentFactory.MAP,
                            MapDataProcessor.prepareData(new Bundle(),
                                    new MapInfoAdapterPaymentSystems(
                                            getActivity(),
                                            Double.parseDouble(settings.getLongitude()),
                                            Double.parseDouble(settings.getLatitude()),
                                            new ListResult<>(Arrays.asList(paymentSystem))
                                    )));
                }
            });
            listView.setAdapter(adapter);
        }
    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        if (loaderid == R.id.payment_system_loader) {
            PaymentSystem paymentSystem = (PaymentSystem) result.first();
            if (paymentSystem != null) {
                this.paymentSystem = paymentSystem;
            }
        }
        isLoading = false;
        updateUI();
    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }
}
