package ru.webmechanic.moneycare.content;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by a.bratusenko on 26.05.16.
 * Класс польозвателя.
 */
public class User extends RealmObject {
    @SerializedName("id")
    private String id;

    @SerializedName("phone")
    private String phone;

    /**
     * содержит статус пользователя
     * 0-не подтвержден
     * 1-подтвержден
     */
    @SerializedName("status")
    private int status;

    @SerializedName("token")
    private String token;

    @SerializedName("created_at")
    private Date creatingDate;

    @SerializedName("updated_at")
    private Date updatingDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getCreatingDate() {
        return creatingDate;
    }

    public void setCreatingDate(Date creatingDate) {
        this.creatingDate = creatingDate;
    }

    public Date getUpdatingDate() {
        return updatingDate;
    }

    public void setUpdatingDate(Date updatingDate) {
        this.updatingDate = updatingDate;
    }
}
