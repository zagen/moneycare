package ru.webmechanic.moneycare.maputils.mapinfoadapters;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.NearestTerminal;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.maputils.Balloon;
import ru.webmechanic.moneycare.maputils.MapInfoAdapter;

/**
 * Created by a.bratusenko on 15.06.16.
 * Извлечение данных из списка ближайших терминалов необходимых для карты
 */
public class MapInfoAdapterNearestTerminals extends MapInfoAdapter {
    private double longitude;
    private double latitude;
    private Context context;
    private boolean oneActive = false;
    private List<Balloon> balloons = new ArrayList<>();

    public MapInfoAdapterNearestTerminals(Context context, double longitude, double latitude, Result<NearestTerminal> nearestTerminalResult) {
        init(context, longitude, latitude, nearestTerminalResult);
    }

    public MapInfoAdapterNearestTerminals(Context context, NearestTerminal terminal, Result<NearestTerminal> result) {
        this.context = context;
        double longitude = Double.parseDouble(terminal.getTerminal().getAddress().getLongitude());
        double latitude = Double.parseDouble(terminal.getTerminal().getAddress().getLatitude());
        init(context, longitude, latitude, result);
        oneActive = true;
    }

    private void init(Context context, double longitude, double latitude, Result<NearestTerminal> result) {
        this.context = context;
        this.longitude = longitude;
        this.latitude = latitude;
        for (int i = 0; i < result.size(); i++) {
            NearestTerminal nearestTerminal = result.getItem(i);
            Balloon balloon = new Balloon();
            balloon.setLatitude(Double.parseDouble(nearestTerminal.getTerminal().getAddress().getLatitude()));
            balloon.setLongitude(Double.parseDouble(nearestTerminal.getTerminal().getAddress().getLongitude()));
            balloon.setAddress(nearestTerminal.getTerminal().getAddress().getFullAddress());
            balloon.setTitle(context.getString(R.string.terminal) + " \"" + nearestTerminal.getPsTitle() + "\"");
            balloons.add(balloon);
        }

    }

    @Override
    public double getLongitude() {
        return longitude;
    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    @Override
    public List<Balloon> getBalloons() {
        return balloons;
    }

    @Override
    public boolean isOneActive() {
        return oneActive;
    }

    @Override
    public String getName() {
        return context.getString(R.string.terminals_on_map);
    }
}
