package ru.webmechanic.moneycare.api.response;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import io.realm.RealmObject;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.database.realm.DBHelper;

/**
 * Created by a.bratusenko on 23.05.16.
 * Класс для ответа сети.Содержит в себе полученные данные, в случае их наличия, либо сообщение об ошибке
 */
public class Response {

    /**
     * Объект данных, десериализованный из данных сети.
     */
    @Nullable
    private Object answer;

    /**
     * Результат сетевого запроса
     */
    private RequestResult requestResult;

    /**
     * флаг отмечающий необходимость кеширования данных в локальной базе данных
     */
    private boolean cache = true;

    /**
     * сетевой код запроса
     */
    private int code = 200;

    /**
     * Сообщение об ошибке
     */
    private String errorMessage = "";

    public Response() {
        requestResult = RequestResult.ERROR;
    }

    @NonNull
    public RequestResult getRequestResult() {
        return requestResult;
    }

    public Response setRequestResult(RequestResult requestResult) {
        this.requestResult = requestResult;
        return this;
    }

    public boolean needCache() {
        return cache;
    }

    /**
     * Преобразует объект данных к его классу.
     *
     * @param <T> класс к которому преобразуется объект данных
     * @return типизированный ответ
     */
    @Nullable
    public <T> T getTypedAnswer() {
        if (answer == null) {
            return null;
        }
        //noinspection unchecked
        return (T) answer;
    }

    public Response setAnswer(@Nullable Object answer) {
        this.answer = answer;
        return this;
    }

    /**
     * Сохраняет объект в локальной базе данных/кэше
     *
     * @param context
     * @param tClass  класс данных
     * @param <T>     класс данных
     */
    public <T extends RealmObject> void save(Context context, Class<T> tClass) {
        if (answer instanceof List<?>) {
            List<T> objects = getTypedAnswer();
            if (objects != null && cache) {

                DBHelper.save(objects, tClass);
                Settings.getInstance(context).getStorage().setCacheUpdateTime(tClass.getSimpleName());
            }
        } else {
            T object = getTypedAnswer();
            if (object != null && cache) {
                DBHelper.save(object, tClass);
                Settings.getInstance(context).getStorage().setCacheUpdateTime(tClass.getSimpleName());
            }
        }


    }

    public Response setCache(boolean cache) {
        this.cache = cache;
        return this;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
