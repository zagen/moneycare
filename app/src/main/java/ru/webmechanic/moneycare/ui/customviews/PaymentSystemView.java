package ru.webmechanic.moneycare.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.PaymentSystem;
import ru.webmechanic.moneycare.ui.utils.Utils;
import ru.webmechanic.moneycare.ui.utils.imageloader.ImageLoader;

/**
 * Created by a.bratusenko on 07.06.16.
 */
public class PaymentSystemView extends LinearLayout {
    @BindView(R.id.psTerminalsContainer)
    LinearLayout terminalsContainer;

    @BindView(R.id.psCommissionsContainer)
    LinearLayout commissionContainer;

    @BindView(R.id.psTerminalTitleTextView)
    TextView terminalsTitleTextView;

    @BindView(R.id.psTerminalValueTextView)
    TextView terminalsValueTextView;

    @BindView(R.id.psCommissionTitleTextView)
    TextView commissionsTitleTextView;

    @BindView(R.id.psCommissionValueTextView)
    TextView commissionsValueTextView;

    @BindView(R.id.psLogoImageView)
    ImageView logoImageView;

    private ImageLoader loader;

    public PaymentSystemView(Context context) {
        super(context);
        init();
    }

    public PaymentSystemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PaymentSystemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setOnTerminalsClickListener(OnClickListener listener) {
        this.terminalsContainer.setOnClickListener(listener);
    }

    public void setOnCommissionClickListener(OnClickListener listener) {
        this.commissionContainer.setOnClickListener(listener);
    }

    private void init() {
        inflate(getContext(), R.layout.customviews_paymentsystem, this);
        ButterKnife.bind(this);
        loader = new ImageLoader(getContext());
    }

    public void setPaymentSystem(PaymentSystem ps) {
        String terminalNumber = Integer.toString(ps.getTerminals().size());
        terminalsTitleTextView.setText(Utils.formatTerminals(terminalNumber));
        terminalsValueTextView.setText(terminalNumber);

        commissionsValueTextView.setText(String.format(getResources().getString(R.string.commission_from), ps.getPercent()));
        commissionsTitleTextView.setText(getResources().getString(R.string.commissions));

        loader.DisplayImage(ps.getLogo(), logoImageView);
    }
}
