package ru.webmechanic.moneycare.maputils;

import android.os.Bundle;

import com.google.gson.Gson;

/**
 * Created by a.bratusenko on 15.06.16.
 * Преобразователь данных для карты в строковый формат json с помещением в Bundle и обратно
 *
 * @see Bundle
 */
public class MapDataProcessor {
    private final static Gson GSON = new Gson();
    private final static String MAP_DATA_JSON = "map data json";

    public static Bundle prepareData(Bundle args, MapInfoAdapter adapter) {
        if (args == null) {
            args = new Bundle();
        }
        MapInfo mapInfo = new MapInfo();
        mapInfo.setBalloons(adapter.getBalloons());
        mapInfo.setLatitude(adapter.getLatitude());
        mapInfo.setLongitude(adapter.getLongitude());
        mapInfo.setOneActive(adapter.isOneActive());
        mapInfo.setName(adapter.getName());

        args.putString(MAP_DATA_JSON, GSON.toJson(mapInfo));
        return args;
    }

    public static MapInfo extractData(Bundle args) {
        MapInfo mapInfo = null;
        if (args != null && args.containsKey(MAP_DATA_JSON)) {
            mapInfo = GSON.fromJson(args.getString(MAP_DATA_JSON), MapInfo.class);
        }
        return mapInfo;
    }
}
