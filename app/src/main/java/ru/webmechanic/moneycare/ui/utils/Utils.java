package ru.webmechanic.moneycare.ui.utils;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Pattern;

import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.content.LoanCoupon;
import ru.webmechanic.moneycare.content.SalesCoupon;

/**
 * Created by a.bratusenko on 30.05.16.
 * Класс содержащий вспомогательные функции
 */
public class Utils {
    /**
     * Копирует из одного потока в другой
     *
     * @param is исходящий поток
     * @param os поток вывода
     */
    public static void copyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {

            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                //Read byte from input stream

                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;

                //Write byte from output stream
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    /**
     * Форматирование расстояния до объекта
     *
     * @param value в метрах
     * @return отформатированное расстояние в виде строки
     */
    public static String asDistance(int value) {

        if (value < 5000) {
            return Integer.toString(value) + " м";
        } else {
            return Integer.toString(value / 1000) + " км";
        }
    }

    /**
     * Русификация отделений
     *
     * @param number
     * @return
     */
    public static String formatDepartments(String number) {
        String[] russificator = new String[]{"отделени", "й", "е", "я"};
        String result = russificator[0];

        if (Pattern.matches("^[0,2-9]?[1]$", number)) {
            result += russificator[2];
        } else {
            if (Pattern.matches("^[0,2-9]?[2-4]$", number)) {
                result += russificator[3];
            } else {
                result += russificator[1];
            }
        }
        return result;
    }

    /**
     * Русификация банкоматов
     *
     * @param number
     * @return
     */
    public static String formatAtms(String number) {
        String[] russificator = new String[]{"банкомат", "ов", "", "а"};
        String result = russificator[0];

        if (Pattern.matches("^[0,2-9]?[1]$", number)) {
            result += russificator[2];
        } else {
            if (Pattern.matches("^[0,2-9]?[2-4]$", number)) {
                result += russificator[3];
            } else {
                result += russificator[1];
            }
        }
        return result;
    }

    /**
     * Русификация терминалов
     *
     * @param number
     * @return
     */
    public static String formatTerminals(String number) {
        String[] russificator = new String[]{"терминал", "ов", "", "а"};
        String result = russificator[0];

        if (Pattern.matches("^[0,2-9]?[1]$", number)) {
            result += russificator[2];
        } else {
            if (Pattern.matches("^[0,2-9]?[2-4]$", number)) {
                result += russificator[3];
            } else {
                result += russificator[1];
            }
        }
        return result;
    }

    /**
     * Проверяет, оплачен ли купон
     *
     * @param settings файл с настройками приложения
     * @param coupon   Купон который необходимо проверить
     * @return истину, если купон уже оплачен пользователем
     */
    public static boolean isFixed(Settings settings, SalesCoupon coupon) {

        boolean isFixed;
        try {
            isFixed = settings.getStorage().isSalesCouponFixed(coupon.getId());
            if (coupon.getFixDatas() != null && coupon.getFixDatas().size() > 0) {
                if (!isFixed) {
                    isFixed = true;
                    settings.getStorage().fixSalesCoupon(coupon.getId());
                }
            }
        } catch (Exception e) {
            isFixed = false;
        }
        return isFixed;
    }

    /**
     * Проверяет, использована ли акция
     *
     * @param settings файл с настройками приложения
     * @param coupon   Акция которую необходимо проверить
     * @return истину, если акция уже использована пользователем
     */
    public static boolean isFixed(Settings settings, LoanCoupon coupon) {

        boolean isFixed;
        try {
            isFixed = settings.getStorage().isLoanCouponFixed(coupon.getId());
            if (coupon.getFixDatas() != null && coupon.getFixDatas().size() > 0) {
                if (!isFixed) {
                    isFixed = true;
                    settings.getStorage().fixLoanCoupon(coupon.getId());
                }
            }
        } catch (Exception e) {
            isFixed = false;
        }
        return isFixed;
    }
}