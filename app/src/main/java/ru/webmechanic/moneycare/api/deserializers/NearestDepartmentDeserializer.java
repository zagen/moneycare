package ru.webmechanic.moneycare.api.deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;

import ru.webmechanic.moneycare.content.ATM;
import ru.webmechanic.moneycare.content.Address;
import ru.webmechanic.moneycare.content.Department;
import ru.webmechanic.moneycare.content.NearestDepartment;

/**
 * Created by a.bratusenko on 26.05.16.
 * Класс, осуществляющий десериализацию ближайших точек оплаты банка
 */
public class NearestDepartmentDeserializer implements JsonDeserializer<NearestDepartment> {
    @Override
    public NearestDepartment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        NearestDepartment nearestDepartment = new NearestDepartment();

        JsonObject jsonObject = (JsonObject) json;
        JsonObject jsAddress;
        try {
            jsAddress = jsonObject.getAsJsonObject("address");
        } catch (Exception e) {
            jsAddress = null;
        }
        boolean isDepartment;
        try {
            isDepartment = jsonObject.get("is_department").getAsBoolean();
        } catch (Exception e) {
            isDepartment = false;
        }
        int distance;
        try {
            distance = jsonObject.get("distance").getAsInt();
        } catch (Exception e) {
            distance = 0;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");

        Address address;
        try {
            address = new AddressDeserializer().deserialize(jsAddress, typeOfT, context);
        } catch (Exception e) {
            address = null;
        }
        String id, bankName, bankId;
        try {
            id = jsonObject.get("id").getAsString();
        } catch (Exception e) {
            id = null;
        }
        try {
            bankName = jsonObject.get("bank").getAsJsonObject().get("title").getAsString();
        } catch (Exception e) {
            bankName = null;
        }
        try {
            bankId = jsonObject.get("bank_id").getAsString();
        } catch (Exception e) {
            bankId = null;
        }

        Date creatingDate = null;
        Date updatingDate = null;
        try {
            creatingDate = sdf.parse(jsonObject.get("created_at").getAsString());
        } catch (Exception ignored) {

        }
        try {
            updatingDate = sdf.parse(jsonObject.get("updated_at").getAsString());
        } catch (Exception ignored) {
        }
        nearestDepartment.setBankName(bankName);
        nearestDepartment.setDistance(distance);
        nearestDepartment.setBankId(bankId);

        if (isDepartment) {
            if (id != null) {
                Department department = new Department();
                department.setId(id);
                department.setAddress(address);
                department.setCreatingDate(creatingDate);
                department.setUpdatingDate(updatingDate);
                nearestDepartment.setDepartment(department);
            } else {
                nearestDepartment.setDepartment(null);
            }

        } else {
            if (id != null) {
                ATM atm = new ATM();
                atm.setId(id);
                atm.setUpdatingDate(updatingDate);
                atm.setCreatingDate(creatingDate);
                atm.setAddress(address);
                nearestDepartment.setAtm(atm);
            } else {
                nearestDepartment.setAtm(null);
            }
        }
        return nearestDepartment;
    }
}
