package ru.webmechanic.moneycare.content;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by a.bratusenko on 25.05.16.
 * Класс данных, содержащих информацию о товаре, входит в состав договора
 *
 * @see Contract
 */
public class Goods extends RealmObject {
    private String group;

    @SerializedName("goods")
    private String goods;

    @SerializedName("brand")
    private String brand;

    @SerializedName("model")
    private String model;

    @SerializedName("amount")
    private double amount;

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGoods() {
        return goods;
    }

    public void setGoods(String goods) {
        this.goods = goods;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
