package ru.webmechanic.moneycare.api.response;

/**
 * Created by a.bratusenko on 23.05.16.
 * Перечисление, показывающее результат сетевого запроса
 */
public enum RequestResult {

    SUCCESS,
    ERROR

}