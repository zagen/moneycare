package ru.webmechanic.moneycare.ui.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.City;
import ru.webmechanic.moneycare.database.ListResult;
import ru.webmechanic.moneycare.database.Result;

/**
 * Created by a.bratusenko on 02.06.16.
 */
public class CityAdapter extends BaseAdapter implements Filterable {

    Filter filter = null;

    private Context context;

    private Result<City> cities;

    private ListView listView;

    public CityAdapter(Context context, Result<City> list, ListView listView) {
        this.context = context;
        this.listView = listView;
        cities = list;
    }

    @Override
    public int getCount() {
        return cities.size();
    }

    @Override
    public Object getItem(int position) {
        return cities.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.simple_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        City item = (City) getItem(position);
        if (item != null) {
            viewHolder.itemView.setText(item.getCity());
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new SimpleFilter();
        }
        return filter;
    }

    static class ViewHolder {
        @BindView(R.id.listitem_header)
        TextView itemView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    class SimpleFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            return new FilterResults();
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            constraint = constraint.toString().toLowerCase();
            Result<City> resultData;
            try {
                if (constraint.length() > 0) {
                    ArrayList<City> auxData = new ArrayList<>();
                    for (int i = 0; i < cities.size(); i++) {
                        if (cities.getItem(i).getCity().toLowerCase().contains(constraint))
                            auxData.add(cities.getItem(i));
                    }
                    resultData = new ListResult<>(auxData);
                } else {
                    resultData = cities;
                }

            } catch (Exception e) {
                resultData = new ListResult<>(new ArrayList<City>());
            }

            CityAdapter adapter = new CityAdapter(context, resultData, listView);
            listView.setAdapter(adapter);
        }
    }
}