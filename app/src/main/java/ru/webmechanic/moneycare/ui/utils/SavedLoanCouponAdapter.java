package ru.webmechanic.moneycare.ui.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.LoanCoupon;
import ru.webmechanic.moneycare.database.Result;

/**
 * Created by a.bratusenko on 03.06.16.
 */
public class SavedLoanCouponAdapter extends LoanCouponsAdapter {
    private OnItemDeletedListener deletedListener;

    public SavedLoanCouponAdapter(Context context, Result<LoanCoupon> result, OnItemDeletedListener listener) {
        super(context, result);
        this.deletedListener = listener;

    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public Object getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = super.getView(position, convertView, parent);
        ViewHolder viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.loanCouponItemView.setFlagVisible(true);

        viewHolder.loanCouponItemView.getSticker().setImageResource(R.drawable.sticker_remove);

        viewHolder.loanCouponItemView.getSticker().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String couponId = ((LoanCoupon) getItem(position)).getId();
                settings.getStorage().saveLoanCoupon(couponId, false);

                if (deletedListener != null) {
                    deletedListener.onItemDeleted();
                }
            }
        });

        return convertView;

    }

    public interface OnItemDeletedListener {
        public void onItemDeleted();
    }
}
