package ru.webmechanic.moneycare.api.deserializers;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import io.realm.RealmList;
import ru.webmechanic.moneycare.content.Goods;

/**
 * Created by a.bratusenko on 27.05.16.
 * Класс для десериализации списка товаров внутри договора. Если какой то из элементов описания отсутствует, создаются строки с пустыми значениями.
 */
public class GoodsDeserializer implements JsonDeserializer<RealmList<Goods>> {
    private Goods extractGoodsFromObject(JsonObject goodsObject) {
        Goods goods = new Goods();
        try {
            goods.setAmount(goodsObject.get("amount").getAsDouble());
        } catch (Exception e) {
            goods.setAmount(0);
        }

        try {
            goods.setBrand(goodsObject.get("brand").getAsJsonObject().get("caption").getAsString());
        } catch (Exception e) {
            goods.setBrand("");
        }
        try {
            goods.setGoods(goodsObject.get("goods").getAsJsonObject().get("caption").getAsString());
        } catch (Exception e) {
            goods.setGoods("");
        }

        try {
            goods.setGroup(goodsObject.get("group").getAsJsonObject().get("caption").getAsString());
        } catch (Exception e) {
            goods.setGroup("");
        }
        try {
            goods.setModel(goodsObject.get("model").getAsString());
        } catch (Exception e) {
            goods.setModel("");
        }
        return goods;
    }

    @Override
    public RealmList<Goods> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        RealmList<Goods> goods = new RealmList<>();
        if (json.isJsonArray()) {
            JsonArray jsonGoodsArray = (JsonArray) json;
            for (int i = 0; i < jsonGoodsArray.size(); i++) {
                JsonObject jsonGoodsObject = jsonGoodsArray.get(i).getAsJsonObject();
                Goods goodsItem = extractGoodsFromObject(jsonGoodsObject);
                goods.add(goodsItem);
            }
        } else {
            goods.add(extractGoodsFromObject((JsonObject) json));
        }
        return goods;
    }
}
