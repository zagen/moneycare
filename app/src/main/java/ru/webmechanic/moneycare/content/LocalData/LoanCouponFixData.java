package ru.webmechanic.moneycare.content.LocalData;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by a.bratusenko on 03.06.16.
 * Класс данных, входят в состав класса LoanCoupon. Содержит информацию о погашении купона
 *
 * @see ru.webmechanic.moneycare.content.LoanCoupon
 */
public class LoanCouponFixData extends RealmObject {
    @SerializedName("id")
    private String id;

    @SerializedName("loan_coupon_id")
    private String couponId;

    @SerializedName("outlet_id")
    private String outletId;

    @SerializedName("client_id")
    private String clientId;

    @SerializedName("created_at")
    private Date creatingDate;

    @SerializedName("updated_at")
    private Date updatingDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Date getCreatingDate() {
        return creatingDate;
    }

    public void setCreatingDate(Date creatingDate) {
        this.creatingDate = creatingDate;
    }

    public Date getUpdatingDate() {
        return updatingDate;
    }

    public void setUpdatingDate(Date updatingDate) {
        this.updatingDate = updatingDate;
    }
}
