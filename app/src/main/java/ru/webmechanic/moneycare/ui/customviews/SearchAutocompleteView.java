package ru.webmechanic.moneycare.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.SubjectType;
import ru.webmechanic.moneycare.database.Result;

/**
 * Created by a.bratusenko on 14.06.16.
 */
public class SearchAutocompleteView extends LinearLayout {
    @BindView(R.id.searchautocompleteContainerLinearLayout)
    LinearLayout searchautocompleteContainer;

    @BindView(R.id.searchautocompleteContainerScrollView)
    ScrollView scrollView;
    private OnSearchItemClickListener listener;

    public SearchAutocompleteView(Context context) {
        super(context);
        init();
    }

    public SearchAutocompleteView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SearchAutocompleteView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.customviews_searchautocomplete, this);
        ButterKnife.bind(this);
    }

    public void setResult(final Result<SubjectType> result) {
        searchautocompleteContainer.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(getContext());
        for (int i = 0; i < result.size(); i++) {
            View view = inflater.inflate(R.layout.list_item_autocomplete_result, null);
            TextView textView = (TextView) view.findViewById(R.id.searchautocompleteItemTextView);
            textView.setText(result.getItem(i).getTitle());
            searchautocompleteContainer.addView(view);

            final int position = i;
            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onSearchItemClick(result.getItem(position).getTitle());
                    }
                }
            });

        }
        if (result.size() > 0)
            setVisible(true);
    }

    public boolean isVisible() {
        return getVisibility() == VISIBLE;
    }

    public void setVisible(boolean visible) {
        scrollView.setVisibility(visible ? VISIBLE : GONE);
    }

    public void setOnSearchItemClickListener(OnSearchItemClickListener onSearchItemClickListener) {
        this.listener = onSearchItemClickListener;
    }

    public interface OnSearchItemClickListener {
        void onSearchItemClick(String value);
    }
}
