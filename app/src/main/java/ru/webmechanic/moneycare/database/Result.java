package ru.webmechanic.moneycare.database;

import java.util.List;

/**
 * Created by a.bratusenko on 30.05.16.
 * Базовый класс обертки для данных, полученных из какого либо источника ( сети или локального кэша)
 */
public abstract class Result<T> {
    /**
     * @return количество элементов
     */
    abstract public int size();

    /**
     * Получение элемента, находящегося в указаном положении
     *
     * @param position Позиция элемента
     * @return Элемент данных на указанной позиции
     */
    abstract public T getItem(int position);

    /**
     * Удаляет элемент в указанной позиции
     *
     * @param position Позиция удаляемого элемента
     */
    abstract public void removeAt(int position);

    /**
     * Преобразует данные в список
     *
     * @return Список с объектами класса T
     */
    abstract public List<T> getList();

    /**
     * Возвращает первый элемент
     *
     * @return первый элемент в коллекции
     */
    abstract public T first();
}
