package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import ru.webmechanic.moneycare.LocalStorage;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Category;
import ru.webmechanic.moneycare.content.LoanCoupon;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.ui.customviews.CategoriesView;
import ru.webmechanic.moneycare.ui.utils.Arguments;
import ru.webmechanic.moneycare.ui.utils.LoanCouponsAdapter;
import ru.webmechanic.moneycare.ui.utils.Utils;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class LoanCouponsFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener{


    private final static String CURRENT_CATEGORY = "current category";

    @BindView(R.id.categoriesViewLoanCoupons)
    CategoriesView categoriesView;

    @BindView(R.id.loanCouponsListView)
    ListView loanCouponsListView;

    @BindView(R.id.noDataTextView)
    TextView noDataTextView;

    private Result<LoanCoupon> loanCouponResult;
    private Result<Category> categoryResult;

    public LoanCouponsFragment() {
    }


    @Override
    protected void fetchData() {
        isLoading = true;
        if (loaderContext.getLoader() == R.id.categories_loader) {
            new DataFetcher(this, loaderContext, this).fetch();
        } else if (loaderContext.getLoader() == R.id.loan_coupons_loader) {
            new DataFetcher(this, loaderContext, this)
                    .containes("subjectCategory", categoriesView.getCurrentCategory())
                    .fetch();
        }
        updateUI();
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.loan_coupons);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_loan_coupons, container, false);
        ButterKnife.bind(this, view);

        categoriesView.setCategorySelectedListener(new CategoriesView.OnCategorySelectedListener() {
            @Override
            public void onCategorySelected(String category) {
                loaderContext.setLoader(R.id.loan_coupons_loader);
                fetchData();

            }
        });
        categoriesView.setShowAllCategoriesListener(new CategoriesView.OnShowAllCategoriesListener() {
            @Override
            public void onShowAllCategory() {
                loaderContext.setLoader(R.id.categories_loader);
                fetchData();
            }
        });


        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(CURRENT_CATEGORY)) {
                String currentCategory = savedInstanceState.getString(CURRENT_CATEGORY);
                categoriesView.setCurrentCategory(currentCategory);
            }
        }
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (categoriesView != null && categoriesView.getCurrentCategory() != null) {
            outState.putString(CURRENT_CATEGORY, categoriesView.getCurrentCategory());
        }
    }

    @OnItemClick(R.id.loanCouponsListView)
    void couponSelected(AdapterView<?> parent, int position) {
        LoanCoupon coupon = (LoanCoupon) parent.getAdapter().getItem(position);
        if (!Utils.isFixed(settings, coupon)) {
            Arguments args = new Arguments();
            args.setId(coupon.getId());
            gotoFragment(FragmentFactory.LOAN_COUPON, args);
        }
    }

    @OnClick(R.id.loanCouponsSaveMarkedButton)
    void saveMarkedCoupons() {
        LocalStorage storage = settings.getStorage();
        if (storage.hasMarkedLoanCoupons()) {
            storage.saveAllMarkedLoanCoupons();
            gotoFragment(FragmentFactory.SAVED_LOAN_COUPONS);
        }
    }

    @OnClick(R.id.loanCouponsSavedButton)
    void showSaved() {
        gotoFragment(FragmentFactory.SAVED_LOAN_COUPONS);
    }

    @OnClick(R.id.loanCouponsFilterButton)
    void filter() {
        if (!categoriesView.isVisible()) {
            loaderContext.setLoader(R.id.categories_loader);
            fetchData();
        } else {
            if (categoriesView.getCurrentCategory() != null && categoriesView.getCategories() == null) {
                loaderContext.setLoader(R.id.categories_loader);
                fetchData();
            }
        }
    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        categoryResult = null;
        loanCouponResult = null;

        if (loaderid == R.id.loan_coupons_loader) {
            try {
                loanCouponResult = (Result<LoanCoupon>) result;
            } catch (Exception e) {
            }

        } else if (loaderid == R.id.categories_loader) {

            try {
                categoryResult = (Result<Category>) result;
            } catch (Exception e) {
            }
        }
        isLoading = false;
        updateUI();

    }

    @Override
    public void onDataFetchedError(int code, String message) {
        categoryResult = null;
        loanCouponResult = null;
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        if (isLoading)
            return;
        if (loanCouponResult != null && loanCouponResult.size() != 0) {

            noDataTextView.setVisibility(View.GONE);
            loanCouponsListView.setAdapter(new LoanCouponsAdapter(getActivity(), loanCouponResult));
        } else if (categoryResult != null && categoryResult.size() != 0) {
            loanCouponsListView.setAdapter(null);
            noDataTextView.setVisibility(View.GONE);
            categoriesView.setCategories(categoryResult);
        } else {
            loanCouponsListView.setAdapter(null);
            if (categoriesView.getCurrentCategory() != null) {
                noDataTextView.setText(R.string.no_data_loan_coupons_categories);
            } else {
                noDataTextView.setText(R.string.no_data_loan_coupons);
            }
            noDataTextView.setVisibility(View.VISIBLE);
        }
    }

}
