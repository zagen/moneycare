package ru.webmechanic.moneycare.ui.utils;

import android.os.Bundle;

/**
 * Created by a.bratusenko on 05.07.16.
 */
public class Arguments {
    public final static String ARG_ID = "id";
    public final static String ARG_LOADER = "loader id";
    public final static String ARG_PHONE = "phone";
    public final static String ARG_IDEXTERNAL = "idexternal";
    public final static String ARG_PASSWORD = "password";
    public final static String ARG_TOKEN = "token";
    public final static String ARG_PIN = "pin";
    public final static String ARG_QUERY = "query";
    public final static String ARG_CONTRACT = "contract";
    public final static String ARG_SHOW_ATM = "show atm";

    private Integer loader;
    private String id;
    private String phone;
    private String idexternal;
    private String password;
    private String token;
    private String pin;
    private String query;
    private String contract;
    private boolean showAtm;

    public Arguments(Bundle args) {
        parseBundle(args);
    }

    public Arguments() {
        resetArgs();
    }

    private void resetArgs() {
        loader = null;
        id = null;
        phone = null;
        idexternal = null;
        password = null;
        token = null;
        pin = null;
        query = null;
        contract = null;
        showAtm = false;
    }

    public void parseBundle(Bundle args) {
        if (args != null) {
            id = (args.containsKey(ARG_ID)) ? args.getString(ARG_ID) : null;
            loader = (args.containsKey(ARG_LOADER)) ? args.getInt(ARG_LOADER) : null;
            phone = (args.containsKey(ARG_PHONE)) ? args.getString(ARG_PHONE) : null;
            idexternal = (args.containsKey(ARG_IDEXTERNAL)) ? args.getString(ARG_IDEXTERNAL) : null;
            password = (args.containsKey(ARG_PASSWORD)) ? args.getString(ARG_PASSWORD) : null;
            token = (args.containsKey(ARG_TOKEN)) ? args.getString(ARG_TOKEN) : null;
            pin = (args.containsKey(ARG_PIN)) ? args.getString(ARG_PIN) : null;
            query = (args.containsKey(ARG_QUERY)) ? args.getString(ARG_QUERY) : null;
            contract = (args.containsKey(ARG_CONTRACT)) ? args.getString(ARG_CONTRACT) : null;
            showAtm = (args.containsKey(ARG_SHOW_ATM)) ? args.getBoolean(ARG_SHOW_ATM) : showAtm;
        } else {
            resetArgs();
        }
    }

    public Bundle fillBundle(Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        if (isSetLoader()) {
            bundle.putInt(ARG_LOADER, loader);
        }
        if (isSetId()) {
            bundle.putString(ARG_ID, id);
        }
        if (isSetPhone()) {
            bundle.putString(ARG_PHONE, phone);
        }
        if (isSetIdexternal()) {
            bundle.putString(ARG_IDEXTERNAL, idexternal);
        }
        if (isSetPassword()) {
            bundle.putString(ARG_PASSWORD, password);
        }
        if (isSetToken()) {
            bundle.putString(ARG_TOKEN, token);
        }
        if (isSetPin()) {
            bundle.putString(ARG_PIN, pin);
        }
        if (isSetQuery()) {
            bundle.putString(ARG_QUERY, query);
        }
        if (isSetContract()) {
            bundle.putString(ARG_CONTRACT, contract);
        }
        if (isSetShowAtm()) {
            bundle.putBoolean(ARG_SHOW_ATM, showAtm);
        }
        return bundle;
    }

    public Bundle getBundle() {
        return fillBundle(null);
    }

    public void setBundle(Bundle args) {
        parseBundle(args);
    }

    public boolean isSetLoader() {
        return loader != null && loader != 0;
    }

    public boolean isSetId() {
        return id != null && !id.isEmpty() && !id.equals("0");
    }

    public boolean isSetPhone() {
        return phone != null && !phone.isEmpty();
    }

    public boolean isSetIdexternal() {
        return idexternal != null && !idexternal.isEmpty();
    }

    public boolean isSetPassword() {
        return password != null && !password.isEmpty();
    }

    public boolean isSetToken() {
        return token != null && !token.isEmpty();
    }

    public boolean isSetPin() {
        return pin != null && !pin.isEmpty();
    }

    public boolean isSetQuery() {
        return query != null && !query.isEmpty();
    }

    public boolean isSetContract() {
        return contract != null && !contract.isEmpty();
    }

    public boolean isSetShowAtm() {
        return true;
    }

    public Integer getLoader() {
        return loader;
    }

    public void setLoader(Integer loader) {
        this.loader = loader;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdexternal() {
        return idexternal;
    }

    public void setIdexternal(String idexternal) {
        this.idexternal = idexternal;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public boolean isShowAtm() {
        return showAtm;
    }

    public void setShowAtm(boolean showAtm) {
        this.showAtm = showAtm;
    }
}
