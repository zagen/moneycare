package ru.webmechanic.moneycare.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.City;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.ui.utils.CityAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectCityFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener {

    @BindView(R.id.cityListView)
    ListView selectCityListView;

    @BindView(R.id.selectCityEditText)
    EditText selectCityEditText;

    @BindView(R.id.noDataTextView)
    TextView noDataTextView;

    private Result<City> cityResult;


    public SelectCityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_city, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.clearCityButton)
    void clear() {
        this.selectCityEditText.setText("");
    }

    @Override
    protected void fetchData() {
        isLoading = true;
        new DataFetcher(this, loaderContext, this).fetch();
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.your_city);
    }

    @OnItemClick(R.id.cityListView)
    public void citySelected(AdapterView<?> parent, int position) {
        City city = (City) parent.getAdapter().getItem(position);

        settings.setCurrentCity(city);
        gotoFragment(FragmentFactory.MAIN_MENU);
    }

    private void setListAdapter(Result<City> cities) {
        final CityAdapter adapter = new CityAdapter(getActivity(), cities, selectCityListView);

        selectCityListView.setAdapter(adapter);
        selectCityEditText.addTextChangedListener(new TextWatcher() {

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals(getString(R.string.clear_all_data))) {
                    settings.clearAll();
                    gotoFragment(FragmentFactory.MAIN_MENU);
                }
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        if (loaderid == R.id.cities_loader) {
            try {
                cityResult = (Result<City>) result;
            } catch (Exception e) {
            }
        }
        isLoading = false;
        updateUI();
    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        if (cityResult != null) {
            if (cityResult.size() != 0) {
                noDataTextView.setVisibility(View.GONE);
                setListAdapter(cityResult);
            } else {
                noDataTextView.setVisibility(View.VISIBLE);
            }
        }
    }
}
