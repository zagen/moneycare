package ru.webmechanic.moneycare;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.lang.ref.WeakReference;

import ru.webmechanic.moneycare.content.Bank;
import ru.webmechanic.moneycare.content.City;
import ru.webmechanic.moneycare.content.LoanCoupon;
import ru.webmechanic.moneycare.content.NearestDepartment;
import ru.webmechanic.moneycare.content.NearestOutlet;
import ru.webmechanic.moneycare.content.NearestTerminal;
import ru.webmechanic.moneycare.content.Outlet;
import ru.webmechanic.moneycare.content.PaymentSystem;
import ru.webmechanic.moneycare.content.SalesCoupon;
import ru.webmechanic.moneycare.database.realm.DBHelper;
import ru.webmechanic.moneycare.ui.utils.imageloader.ImageLoader;

/**
 * Created by a.bratusenko on 23.05.16.
 * Класс, содержащий настройки приложения
 */
public class Settings {
    public static final String API_ENDPOINT = "http://moncas.ru.dev/api/";
    public static final String IMAGE_PATH = "http://moncas.ru.dev/images/uploads/";
    public final static String CITY_NAME = "CITY_NAME";
    public final static String CITY_ID = "CITY_ID";
    public final static String TOKEN = "TOKEN";
    public final static String PHONE = "PHONE";
    public final static String LATITUDE = "LATITUDE";
    public final static String LONGITUDE = "LONGITUDE";
    public final static String USER_STATE = "USER STATE";
    public final static String USER_STATE_UNREGISTERED = "USER NOT REGISTERED";
    public final static String USER_STATE_LOGGED = "USER LOGGED";
    public final static String USER_STATE_PIN_AWAIT = "PIN AWAIT";
    private final static String PREF_NAME = "MONEYCARE_APP_PREFERENCES";

    private static WeakReference<Context> context;

    private static Settings ourInstance = null;
    private SharedPreferences preferences = null;
    private LocalStorage storage;

    private Settings(WeakReference<Context> context) {
        Settings.context = context;
        try {
            this.preferences = context.get().getSharedPreferences(PREF_NAME, 0);
            this.storage = new LocalStorage(context);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static Settings getInstance() {
        return getInstance(context.get());
    }

    public static Settings getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new Settings(new WeakReference<>(context));

        }

        return ourInstance;
    }

    public Context getContext() {
        return context.get();
    }

    public static void setContext(Context context) {
        Settings.context = new WeakReference<>(context);
    }

    public City getCurrentCity() {
        City city = null;
        String cityId = preferences.getString(CITY_ID, "");
        String cityName = preferences.getString(CITY_NAME, "");
        if (cityId.length() > 0) {
            city = new City();
            city.setCity(cityName);
            city.setId(cityId);
        }
        return city;
    }

    public void setCurrentCity(City city) {
        if (city != null) {
            String id = city.getId();
            String name = city.getCity();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(CITY_ID, id);
            editor.putString(CITY_NAME, name);
            editor.apply();
            getStorage().invalidateCache(SalesCoupon.class.getSimpleName());
            getStorage().invalidateCache(LoanCoupon.class.getSimpleName());
            getStorage().invalidateCache(Outlet.class.getSimpleName());
            getStorage().invalidateCache(NearestOutlet.class.getSimpleName());
            getStorage().invalidateCache(Bank.class.getSimpleName());
            getStorage().invalidateCache(NearestDepartment.class.getSimpleName());
            getStorage().invalidateCache(NearestTerminal.class.getSimpleName());
            getStorage().invalidateCache(PaymentSystem.class.getSimpleName());
        }
    }

    public boolean isInternetAvailable() {
        boolean isAvailable = false;
        try {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) context.get().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            isAvailable = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isAvailable;
    }

    public String getUserToken() {
        String token = null;
        try {
            token = preferences.getString(TOKEN, token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return token;
    }

    public void setUserToken(String token) {
        try {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(TOKEN, token);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getPhone() {
        String phone = null;
        try {
            phone = preferences.getString(PHONE, phone);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return phone;
    }

    public void setPhone(String phone) {
        try {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(PHONE, phone);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getUserState() {
        String userState = USER_STATE_UNREGISTERED;
        try {
            userState = preferences.getString(USER_STATE, userState);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userState;
    }

    public void setUserState(String userState) {
        if (userState.equals(USER_STATE_UNREGISTERED) ||
                userState.equals(USER_STATE_LOGGED) ||
                userState.equals(USER_STATE_PIN_AWAIT)) {
            try {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(USER_STATE, userState);
                editor.apply();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getLatitude() {
        String latitude = "55.7522200";
        try {
            latitude = preferences.getString(LATITUDE, latitude);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return latitude;
    }

    public void setLatitude(String latitude) {
        try {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(LATITUDE, latitude);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getLongitude() {
        String longitude = "37.6155600";
        try {
            longitude = preferences.getString(LONGITUDE, longitude);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return longitude;
    }

    public void setLongitude(String longitude) {
        try {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(LONGITUDE, longitude);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String[] prepareParams() {
        String[] params = new String[5];
        params[0] = null;
        params[1] = null;
        params[2] = null;
        params[3] = null;
        params[4] = null;

        City city = getCurrentCity();
        if (city != null) {
            params[0] = getCurrentCity().getId();
        }
        String latitude = getLatitude();
        String longitude = getLongitude();
        String phone = getPhone();
        String token = getUserToken();
        if (latitude != null && latitude.length() > 0) {
            params[1] = latitude;
        }
        if (longitude != null && longitude.length() > 0) {
            params[2] = longitude;
        }
        if (phone != null && phone.length() > 0) {
            params[3] = phone;
        }
        if (token != null && token.length() > 0) {
            params[4] = token;
        }
        return params;
    }

    /**
     * Очистка всех настроек и данных
     */
    public void clearAll() {
        try {
            ImageLoader imageLoader = new ImageLoader(context.get());
            imageLoader.clearCache();
            preferences.edit().clear().commit();
            DBHelper.deleteAllDatabases(context.get());
            storage.clearAll();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public LocalStorage getStorage() {
        return storage;
    }

}
