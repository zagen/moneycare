package ru.webmechanic.moneycare.ui.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;

/**
 * Created by a.bratusenko on 27.05.16.
 */
public class NavigationDrawerListAdapter extends BaseAdapter {

    private Context context;
    private String[] stringValues;
    private TypedArray imageValues;

    public NavigationDrawerListAdapter(Context context, String[] stringValues, TypedArray imageValues) {
        this.context = context;
        this.stringValues = stringValues;
        this.imageValues = imageValues;
    }

    @Override
    public int getCount() {
        return stringValues.length;
    }

    @Override
    public Object getItem(int position) {
        return stringValues[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_item_image_text, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.txtTitle.setText(stringValues[position]);

        try {
            viewHolder.imgView.setImageDrawable(imageValues.getDrawable(position));
        } catch (Exception e) {
        }
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.listItemImageTextTextView)
        TextView txtTitle;
        @BindView(R.id.listItemImageTextImageView)
        ImageView imgView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
