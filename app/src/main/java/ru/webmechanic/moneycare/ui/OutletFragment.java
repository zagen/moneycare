package ru.webmechanic.moneycare.ui;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Outlet;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.maputils.MapDataProcessor;
import ru.webmechanic.moneycare.maputils.MapInfoAdapter;
import ru.webmechanic.moneycare.maputils.mapinfoadapters.MapInfoAdapterOutlets;

/**
 * A simple {@link Fragment} subclass.
 */
public class OutletFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener{

    @BindView(R.id.titleOutletTextView)
    TextView titleView;

    @BindView(R.id.organizationOutletTextView)
    TextView organizationTextView;

    @BindView(R.id.fulladdressOutletTextView)
    TextView fulladdressTextView;

    @BindView(R.id.phoneOutletTextView)
    TextView phoneTextView;

    private Outlet outlet = null;

    public OutletFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_outlet, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.outlet);
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        if (outlet != null) {
            titleView.setText(outlet.getTitle());
            organizationTextView.setText(outlet.getOrganization());
            fulladdressTextView.setText(outlet.getAddress().getFullAddress());
            if (outlet.getPhone() != null && outlet.getPhone().length() == 10) {
                phoneTextView.setText("+7 " + outlet.getPhone());
            } else {
                phoneTextView.setText(R.string.not_defined);
            }
        }
    }

    @Override
    protected void fetchData() {
        isLoading = true;
        new DataFetcher(this, loaderContext, this).one(arguments.getId()).fetch();
    }

    @OnClick(R.id.phoneOutletTextView)
    void call() {
        if (!phoneTextView.getText().equals(getString(R.string.not_defined))) {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + phoneTextView.getText()));
            startActivity(callIntent);
        }
    }

    @OnClick(R.id.onMapOutletButton)
    void showMap() {
        Result outlets = new DataFetcher(this, Outlet.class).fetchFromCache();
        MapInfoAdapter adapter = new MapInfoAdapterOutlets(getActivity(), outlet, outlets);
        gotoFragment(FragmentFactory.MAP, MapDataProcessor.prepareData(new Bundle(), adapter));
    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        if (loaderid == R.id.outlet_loader && result.first() != null) {
            this.outlet = (Outlet) result.first();
        }
        isLoading = false;
        updateUI();
    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }
}
