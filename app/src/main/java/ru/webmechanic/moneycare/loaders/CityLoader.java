package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;

import retrofit.Call;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.ApiFactory;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.City;

/**
 * Created by a.bratusenko on 23.05.16.
 */
public class CityLoader extends BaseLoader {
    private String latitude = null;
    private String longitude = null;
    private Settings settings;

    public CityLoader(Context context, Bundle args) {
        super(context);
        settings = Settings.getInstance(context);
        latitude = settings.getLatitude();
        longitude = settings.getLongitude();
    }

    @Override
    protected Response apiCall() throws IOException {
        City city = null;
        Call<City> call = service.city(latitude, longitude);

        retrofit.Response<City> retrofitResponse = call.execute();
        //в случае если сетевой статус пришел отличный от 200, пытаемся десериализовать объект города из errorBody
        if (retrofitResponse != null && !retrofitResponse.isSuccess() && retrofitResponse.errorBody() != null) {
            city = (City) ApiFactory.getRetrofit().responseConverter(
                    City.class, City.class.getAnnotations())
                    .convert(retrofitResponse.errorBody());
        } else {
            if (retrofitResponse != null)
                city = retrofitResponse.body();
        }


        if (city != null) {
            settings.setCurrentCity(city);
        }

        Response response = new Response();
        //Один город не кэшируем поскольку данные обо всех остальных объектах будут затираться
        response.setCache(false);
        return response.setRequestResult(RequestResult.SUCCESS)
                .setAnswer(city);
    }
}
