package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.maputils.Balloon;
import ru.webmechanic.moneycare.maputils.MapDataProcessor;
import ru.webmechanic.moneycare.maputils.MapInfo;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.overlay.balloon.OnBalloonListener;
import ru.yandex.yandexmapkit.utils.GeoPoint;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends BaseFragment implements OnBalloonListener {

    private MapInfo mapInfo;
    private MapController mapController;
    private OverlayManager overlayManager;

    public MapFragment() {
        // Required empty public constructor
    }

    public void setMapInfo(MapInfo mapInfo) {
        this.mapInfo = mapInfo;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void fetchData() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        final MapView mapView = (MapView) view.findViewById(R.id.map);
        setMapInfo(MapDataProcessor.extractData(getArguments()));

        mapView.showZoomButtons(true);
        mapController = mapView.getMapController();

        mapController.setZoomCurrent(12);
        overlayManager = mapController.getOverlayManager();

        overlayManager.getMyLocation().setEnabled(true);


        createOverlay();
        return view;
    }

    private void createOverlay() {
        Overlay overlay = new Overlay(mapController);

        for (Balloon balloon : mapInfo.getBalloons()) {
            OverlayItem item;
            if (mapInfo.isOneActive()) {
                if (balloon.getLatitude() == mapInfo.getLatitude() && balloon.getLongitude() == mapInfo.getLongitude()) {
                    item = new OverlayItem(new GeoPoint(balloon.getLatitude(), balloon.getLongitude()), ContextCompat.getDrawable(getActivity(), R.drawable.marker));
                } else {
                    item = new OverlayItem(new GeoPoint(balloon.getLatitude(), balloon.getLongitude()), ContextCompat.getDrawable(getActivity(), R.drawable.second_marker));
                }
            } else {
                item = new OverlayItem(new GeoPoint(balloon.getLatitude(), balloon.getLongitude()), ContextCompat.getDrawable(getActivity(), R.drawable.marker));
            }
            BalloonItem balloonItem = new BalloonItem(getActivity(), item.getGeoPoint());

            balloonItem.setText(Html.fromHtml(balloon.toString()));
            balloonItem.setOnBalloonListener(this);
            item.setBalloonItem(balloonItem);
            overlay.addOverlayItem(item);
        }
        overlayManager.addOverlay(overlay);
        if (mapInfo.isOneActive()) {
            mapController.setZoomCurrent(16.66f);
        }
        mapController.setPositionAnimationTo(new GeoPoint(mapInfo.getLatitude(), mapInfo.getLongitude()));

    }

    @Override
    public String getTitle() {
        return mapInfo.getName();
    }

    @Override
    public void onBalloonViewClick(BalloonItem balloonItem, View view) {

    }

    @Override
    public void onBalloonShow(BalloonItem balloonItem) {

    }

    @Override
    public void onBalloonHide(BalloonItem balloonItem) {

    }

    @Override
    public void onBalloonAnimationStart(BalloonItem balloonItem) {

    }

    @Override
    public void onBalloonAnimationEnd(BalloonItem balloonItem) {

    }
}
