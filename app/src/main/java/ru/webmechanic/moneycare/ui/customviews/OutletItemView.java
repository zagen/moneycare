package ru.webmechanic.moneycare.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Outlet;

/**
 * Created by a.bratusenko on 06.07.16.
 */
public class OutletItemView extends RelativeLayout {
    @BindView(R.id.detailsOutletItemTextView)
    TextView outletDesc;

    public OutletItemView(Context context) {
        super(context);
        init();
    }

    public OutletItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OutletItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.item_outlet, this);
        ButterKnife.bind(this);
    }

    public void setOutlet(Outlet outlet) {
        this.outletDesc.setText(String.format(getContext().getString(R.string.outlet_item_sales_coupon_description),
                outlet.getTitle(),
                outlet.getOrganization(),
                outlet.getAddress().getFullAddress()));
    }
}
