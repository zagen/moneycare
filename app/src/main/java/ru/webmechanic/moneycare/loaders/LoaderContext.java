package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import io.realm.RealmObject;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Bank;
import ru.webmechanic.moneycare.content.Category;
import ru.webmechanic.moneycare.content.City;
import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.content.LoanCoupon;
import ru.webmechanic.moneycare.content.LocalData.LoanCouponFixData;
import ru.webmechanic.moneycare.content.LocalData.SalesCouponFixData;
import ru.webmechanic.moneycare.content.NearestDepartment;
import ru.webmechanic.moneycare.content.NearestOutlet;
import ru.webmechanic.moneycare.content.NearestTerminal;
import ru.webmechanic.moneycare.content.Outlet;
import ru.webmechanic.moneycare.content.PaymentSystem;
import ru.webmechanic.moneycare.content.SalesCoupon;
import ru.webmechanic.moneycare.content.SubjectType;
import ru.webmechanic.moneycare.content.User;
import ru.webmechanic.moneycare.ui.utils.Arguments;

/**
 * Created by a.bratusenko on 05.07.16.
 */
public class LoaderContext {


    /**
     * словарь содержащий  id - class соответствия
     */
    private final HashMap<Integer, Class> loaders = new HashMap<>();
    private final HashMap<Integer, Class<? extends RealmObject>> dataClasses = new HashMap<>();
    private Arguments arguments;
    private Context context;

    {
        loaders.put(R.id.cities_loader, CitiesLoader.class);
        loaders.put(R.id.city_loader, CityLoader.class);
        loaders.put(R.id.outlet_loader, OutletLoader.class);
        loaders.put(R.id.outlets_loader, OutletsLoader.class);
        loaders.put(R.id.nearest_outlets_loader, NearestOutletsLoader.class);
        loaders.put(R.id.login_loader, LoginLoader.class);
        loaders.put(R.id.create_user_loader, CreateUserLoader.class);
        loaders.put(R.id.confirm_pin_loader, ConfirmPinLoader.class);
        loaders.put(R.id.resend_pin_loader, ResendPinLoader.class);
        loaders.put(R.id.sales_coupons_loader, SalesCouponsLoader.class);
        loaders.put(R.id.loan_coupons_loader, LoanCouponsLoader.class);
        loaders.put(R.id.banks_loader, BanksLoader.class);
        loaders.put(R.id.bank_loader, BankLoader.class);
        loaders.put(R.id.nearest_departments_loader, NearestDepartmentsLoader.class);
        loaders.put(R.id.payment_systems_loader, PaymentSystemsLoader.class);
        loaders.put(R.id.payment_system_loader, PaymentSystemLoader.class);
        loaders.put(R.id.nearest_terminals_loader, NearestTerminalsLoader.class);
        loaders.put(R.id.categories_loader, CategoriesLoader.class);
        loaders.put(R.id.subject_search_loader, SubjectTypesLoader.class);
        loaders.put(R.id.contracts_loader, ContractsLoader.class);
        loaders.put(R.id.fix_loan_coupon_loader, FixLoanCouponLoader.class);
        loaders.put(R.id.fix_sales_coupon_loader, FixSalesCouponLoader.class);
    }

    {
        dataClasses.put(R.id.cities_loader, City.class);
        dataClasses.put(R.id.city_loader, City.class);
        dataClasses.put(R.id.outlet_loader, Outlet.class);
        dataClasses.put(R.id.outlets_loader, Outlet.class);
        dataClasses.put(R.id.nearest_outlets_loader, NearestOutlet.class);
        dataClasses.put(R.id.login_loader, User.class);
        dataClasses.put(R.id.create_user_loader, User.class);
        dataClasses.put(R.id.confirm_pin_loader, User.class);
        dataClasses.put(R.id.resend_pin_loader, User.class);
        dataClasses.put(R.id.sales_coupons_loader, SalesCoupon.class);
        dataClasses.put(R.id.loan_coupons_loader, LoanCoupon.class);
        dataClasses.put(R.id.banks_loader, Bank.class);
        dataClasses.put(R.id.bank_loader, Bank.class);
        dataClasses.put(R.id.nearest_departments_loader, NearestDepartment.class);
        dataClasses.put(R.id.payment_systems_loader, PaymentSystem.class);
        dataClasses.put(R.id.payment_system_loader, PaymentSystem.class);
        dataClasses.put(R.id.nearest_terminals_loader, NearestTerminal.class);
        dataClasses.put(R.id.categories_loader, Category.class);
        dataClasses.put(R.id.subject_search_loader, SubjectType.class);
        dataClasses.put(R.id.contracts_loader, Contract.class);
        dataClasses.put(R.id.fix_loan_coupon_loader, LoanCouponFixData.class);
        dataClasses.put(R.id.fix_sales_coupon_loader, SalesCouponFixData.class);
    }

    public LoaderContext(Context context, @NonNull Arguments arguments) {
        this.context = context;
        this.arguments = arguments;
    }

    public int getLoader() {
        return arguments.getLoader();
    }

    public void setLoader(int loader) {
        this.arguments.setLoader(loader);
    }

    /**
     * Использует рефлексию для создания объекта загрузчика по его id
     *
     * @return объект загрузчика
     */
    public BaseLoader createLoader() {
        BaseLoader loader = null;
        if (loaders.containsKey(getLoader())) {
            Class loaderClass = loaders.get(getLoader());
            try {
                Constructor<?> constructor = loaderClass.getConstructor(Context.class, Bundle.class);
                loader = (BaseLoader) constructor.newInstance(context, arguments.getBundle());
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        return loader;
    }

    /**
     * @return Возвращает класс данных, соответствующий загрузчику
     */
    public Class<? extends RealmObject> getDataClass() {
        return dataClasses.get(getLoader());
    }

    public Arguments getArguments() {
        return this.arguments;
    }

    public void setArguments(Arguments arguments) {
        this.arguments = arguments;
    }
}
