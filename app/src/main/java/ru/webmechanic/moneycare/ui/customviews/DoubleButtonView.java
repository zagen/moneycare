package ru.webmechanic.moneycare.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;

/**
 * Created by a.bratusenko on 09.06.16.
 */
public class DoubleButtonView extends LinearLayout {
    @BindView(R.id.doubleButtonFirstButton)
    Button firstButton;

    @BindView(R.id.doubleButtonSecondButton)
    Button secondButton;

    public DoubleButtonView(Context context) {
        super(context);
        init();
    }

    public DoubleButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DoubleButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setOnFirstButtonClickListener(View.OnClickListener listener) {
        firstButton.setOnClickListener(listener);
    }

    public void setOnSecondButtonClickListener(View.OnClickListener listener) {
        secondButton.setOnClickListener(listener);
    }

    private void init() {
        inflate(getContext(), R.layout.customviews_doublebutton, this);
        ButterKnife.bind(this);
    }

    public void setStyle(DoubleButtonStyle style) {
        if (style == DoubleButtonStyle.GREEN) {
            firstButton.setBackgroundResource(R.drawable.lefthalf_green_button);
            secondButton.setBackgroundResource(R.drawable.righthalf_green_button);
        } else {
            firstButton.setBackgroundResource(R.drawable.lefthalf_red_button_states);
            secondButton.setBackgroundResource(R.drawable.righthalf_red_button_states);
        }
    }

    public void setFirstButtonText(int resId) {
        firstButton.setText(resId);
    }

    public void setSecondButtonText(int resId) {
        secondButton.setText(resId);
    }

    public enum DoubleButtonStyle {
        RED, GREEN
    }

}
