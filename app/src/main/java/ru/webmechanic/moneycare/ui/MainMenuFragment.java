package ru.webmechanic.moneycare.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TableLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.Settings;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class MainMenuFragment extends BaseFragment {

    @BindView(R.id.mainMenuUserAuthorizedContainer)
    RelativeLayout userAuthorizedContainer;

    @BindView(R.id.mainMenuUserNotAuthorizedContainer)
    RelativeLayout userNotAuthorizedContainer;

    @BindView(R.id.mainMenuConfirmPinContainer)
    RelativeLayout confirmPinContainer;

    @BindView(R.id.mainMenuButtonsUserConfirmedLayout)
    TableLayout userConfirmedButtonsContainer;

    public MainMenuFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.your_city);
    }

    @OnClick(R.id.loginMainMenuButton)
    void login() {
        gotoFragment(FragmentFactory.LOGIN);
    }

    @OnClick(R.id.salesCouponsMainMenuRelativeLayout)
    void showSalesCoupons() {
        gotoFragment(FragmentFactory.SALES_COUPONS);
    }

    @OnClick(R.id.outletsMainMenuRelativeLayout)
    void showOutlets() {
        gotoFragment(FragmentFactory.OUTLETS);
    }

    @OnClick(R.id.loanCouponsMainMenuRelativeLayout)
    void showLoanCoupons() {
        gotoFragment(FragmentFactory.LOAN_COUPONS);
    }

    @OnClick(R.id.enterPinMainMenuButton)
    void enterPin() {
        gotoFragment(FragmentFactory.PIN);
    }

    @OnClick(R.id.contractsMainMenuRelativeLayout)
    void showContracts() {
        gotoFragment(FragmentFactory.CONTRACTS);
    }

    @OnClick(R.id.paymentMethodsMainMenuRelativeLayout)
    void selectPayMethod() {
        gotoFragment(FragmentFactory.SELECT_PAY_METHOD);
    }

    @OnClick(R.id.contractsArchiveMenuRelativeLayout)
    void showContractsArchive() {
        gotoFragment(FragmentFactory.CONTRACTS_PAYED);
    }

    @OnClick({R.id.mainMenuSearchLayout, R.id.mainMenuUserAuthorizedSearchLayout, R.id.searchUserAuthorizedEditText, R.id.searchEditText})
    void search() {
        gotoFragment(FragmentFactory.SEARCH);
    }


    @Override
    protected void updateUI() {
        super.updateUI();
        switch (settings.getUserState()) {
            case Settings.USER_STATE_LOGGED:
                userAuthorizedContainer.setVisibility(View.VISIBLE);
                userNotAuthorizedContainer.setVisibility(View.GONE);
                confirmPinContainer.setVisibility(View.GONE);
                userConfirmedButtonsContainer.setVisibility(View.VISIBLE);
                break;

            case Settings.USER_STATE_PIN_AWAIT:
                userAuthorizedContainer.setVisibility(View.GONE);
                userNotAuthorizedContainer.setVisibility(View.GONE);
                confirmPinContainer.setVisibility(View.VISIBLE);
                userConfirmedButtonsContainer.setVisibility(View.GONE);
                break;

            case Settings.USER_STATE_UNREGISTERED:
                userAuthorizedContainer.setVisibility(View.GONE);
                userNotAuthorizedContainer.setVisibility(View.VISIBLE);
                confirmPinContainer.setVisibility(View.GONE);
                userConfirmedButtonsContainer.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    protected void fetchData() {

    }

}
