package ru.webmechanic.moneycare.maputils.mapinfoadapters;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Outlet;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.maputils.Balloon;
import ru.webmechanic.moneycare.maputils.MapInfoAdapter;

/**
 * Created by a.bratusenko on 15.06.16.
 * Извлечение данных из списка точек продаж для карты
 */
public class MapInfoAdapterOutlets extends MapInfoAdapter {
    private Context context;
    private double longitude;
    private double latitude;
    private boolean isOneActive = false;
    private Result<Outlet> outlets;

    public MapInfoAdapterOutlets(Context context, double longitude, double latitude, Result<Outlet> outlets) {
        this.context = context;
        this.longitude = longitude;
        this.latitude = latitude;
        this.outlets = outlets;
    }

    public MapInfoAdapterOutlets(Context context, Outlet outlet, Result<Outlet> outlets) {
        this.context = context;
        this.outlets = outlets;
        try {
            this.longitude = Double.parseDouble(outlet.getAddress().getLongitude());
            this.latitude = Double.parseDouble(outlet.getAddress().getLatitude());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        isOneActive = true;
    }

    @Override
    public double getLongitude() {
        return longitude;
    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    @Override
    public List<Balloon> getBalloons() {
        List<Balloon> balloons = new ArrayList<>();
        for (int i = 0; i < outlets.size(); i++) {
            Outlet outlet = outlets.getItem(i);
            Balloon balloon = new Balloon();
            balloon.setTitle(outlet.getOrganization());
            balloon.setPhone(outlet.getPhone());

            balloon.setAddress(outlet.getAddress().getFullAddress());
            try {
                balloon.setLongitude(Double.parseDouble(outlet.getAddress().getLongitude()));
                balloon.setLatitude(Double.parseDouble(outlet.getAddress().getLatitude()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            balloons.add(balloon);
        }
        return balloons;
    }


    @Override
    public String getName() {
        return context.getString(R.string.outlets);
    }

    @Override
    public boolean isOneActive() {
        return isOneActive;
    }

}
