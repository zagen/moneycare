package ru.webmechanic.moneycare.database.realm;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import ru.webmechanic.moneycare.content.ATM;
import ru.webmechanic.moneycare.content.Address;
import ru.webmechanic.moneycare.content.Bank;
import ru.webmechanic.moneycare.content.Category;
import ru.webmechanic.moneycare.content.City;
import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.content.Department;
import ru.webmechanic.moneycare.content.Goods;
import ru.webmechanic.moneycare.content.LoanCoupon;
import ru.webmechanic.moneycare.content.LocalData.LoanCouponFixData;
import ru.webmechanic.moneycare.content.LocalData.SalesCouponFixData;
import ru.webmechanic.moneycare.content.NearestDepartment;
import ru.webmechanic.moneycare.content.NearestOutlet;
import ru.webmechanic.moneycare.content.NearestTerminal;
import ru.webmechanic.moneycare.content.Outlet;
import ru.webmechanic.moneycare.content.PaymentSystem;
import ru.webmechanic.moneycare.content.SalesCoupon;
import ru.webmechanic.moneycare.content.SubjectType;
import ru.webmechanic.moneycare.content.Terminal;
import ru.webmechanic.moneycare.content.User;
import ru.webmechanic.moneycare.database.ListResult;
import ru.webmechanic.moneycare.database.RealmResult;
import ru.webmechanic.moneycare.database.Result;

/**
 * Created by a.bratusenko on 23.05.16.
 * Класс для взаимодействия с базой данных
 */
public class DBHelper {
    /**
     * Содержит ссылки на конфигурации базы данных для каждого класса. Несколько баз данных необходимо
     * для того чтобы данные не перекрывались.
     */
    private static HashMap<String, RealmConfiguration> dbConfigurations = null;

    /**
     * текущая база данных
     */
    private static Realm realm;

    /**
     * Инициализация конфигураций и проверка базы данных на необходимость миграции.
     *
     * @param context
     */
    public static void init(@NonNull Context context) {
        dbConfigurations = new HashMap<>();
        RealmConfiguration configurationBase = new RealmConfiguration.Builder(context)
                .name("base.realm")
                .deleteRealmIfMigrationNeeded()
                .build();
        RealmConfiguration configurationNearest = new RealmConfiguration.Builder(context)
                .name("nearest.realm")
                .deleteRealmIfMigrationNeeded()
                .build();
        dbConfigurations.put(LoanCoupon.class.getSimpleName(), configurationBase);
        dbConfigurations.put(Address.class.getSimpleName(), configurationBase);
        dbConfigurations.put(ATM.class.getSimpleName(), configurationBase);
        dbConfigurations.put(Bank.class.getSimpleName(), configurationBase);
        dbConfigurations.put(Category.class.getSimpleName(), configurationBase);
        dbConfigurations.put(City.class.getSimpleName(), configurationBase);
        dbConfigurations.put(Contract.class.getSimpleName(), configurationBase);
        dbConfigurations.put(Department.class.getSimpleName(), configurationBase);
        dbConfigurations.put(Goods.class.getSimpleName(), configurationBase);
        dbConfigurations.put(Outlet.class.getSimpleName(), configurationBase);
        dbConfigurations.put(PaymentSystem.class.getSimpleName(), configurationBase);
        dbConfigurations.put(SalesCoupon.class.getSimpleName(), configurationBase);
        dbConfigurations.put(SubjectType.class.getSimpleName(), configurationBase);
        dbConfigurations.put(Terminal.class.getSimpleName(), configurationBase);
        dbConfigurations.put(User.class.getSimpleName(), configurationBase);
        dbConfigurations.put(LoanCouponFixData.class.getSimpleName(), configurationBase);
        dbConfigurations.put(SalesCouponFixData.class.getSimpleName(), configurationBase);
        dbConfigurations.put(NearestOutlet.class.getSimpleName(), configurationNearest);
        dbConfigurations.put(NearestDepartment.class.getSimpleName(), configurationNearest);
        dbConfigurations.put(NearestTerminal.class.getSimpleName(), configurationNearest);

        try {
            Realm realm = Realm.getInstance(configurationBase);
            realm.close();

        } catch (Exception e) {
            Realm.deleteRealm(configurationBase);
        }
        try {
            Realm realm = Realm.getInstance(configurationNearest);
            realm.close();
        } catch (Exception e) {
            Realm.deleteRealm(configurationNearest);
        }

    }

    /**
     * Открывает соединение с необходимой классу базой данных
     *
     * @param tclass класс, в соответствие к которому необходимо подобрать базу данных
     * @return Ссылку на БД
     */
    private static Realm getRealmForClass(Class tclass) {
        if (dbConfigurations != null && dbConfigurations.containsKey(tclass.getSimpleName())) {
            realm = Realm.getInstance(dbConfigurations.get(tclass.getSimpleName()));
        }
        return realm;
    }


    /**
     * Сохраняет в базу данных список объектов, предварительно удалив все объекты в таблице
     *
     * @param savingObjects список сохраняемых объектов
     * @param tclass        Класс данных
     * @param <T>           Тип класса данных, наследующих от RealmObject
     */
    public static <T extends RealmObject> void save(List<T> savingObjects, Class<T> tclass) {
        save(savingObjects, tclass, true);
    }

    /**
     * Сохраняет в базу данных список объектов
     *
     * @param savingObjects список сохраняемых объектов
     * @param tclass        класс данных
     * @param clear         удаляет хранимые в БД объекты класса, если true
     * @param <T>           класс данных
     */
    public static <T extends RealmObject> void save(List<T> savingObjects, Class<T> tclass, boolean clear) {
        Realm realm = getRealmForClass(tclass);
        if (realm != null) {

            realm.beginTransaction();
            if (clear) {
                realm.clear(tclass);
            }
            realm.copyToRealm(savingObjects);
            realm.commitTransaction();
        }
    }

    /**
     * Сохраняет объект в базу данных
     *
     * @param savingObject сохраняемый объект
     * @param tclass       Класс данных
     * @param <T>          Название класса данных
     */
    public static <T extends RealmObject> void save(T savingObject, Class<T> tclass) {
        Realm realm = getRealmForClass(tclass);
        if (realm != null) {
            realm.beginTransaction();
            realm.clear(tclass);
            realm.copyToRealm(savingObject);
            realm.commitTransaction();
        }
    }

    /**
     * Возвращает все объекты заданного класса
     *
     * @param tClass Класс данных
     * @param <T>    Имя класса данных
     * @return
     */
    @NonNull
    public static <T extends RealmObject> Result<T> getAll(Class<T> tClass) {
        Realm realm = getRealmForClass(tClass);
        Result<T> result;
        if (realm != null) {
            result = new RealmResult(realm.where(tClass).findAll());
        } else {
            result = new ListResult<>(new ArrayList<T>());
        }
        return result;
    }

    /**
     * Ищет первый объект с полем id.
     *
     * @param context Android context
     * @param id      (String) значение ключа объекта
     * @param tClass  Класс данных
     * @param <T>     Имя класса данных
     * @return Result список объектов содержащий один элемент в случае удачного поиска.
     */
    public static <T extends RealmObject> Result<T> getById(@NonNull Context context, @NonNull String id, Class<T> tClass) {

        Realm realm = getRealmForClass(tClass);
        List<T> objectsList = new ArrayList<>();

        if (realm != null) {
            T object = realm.where(tClass)
                    .equalTo("id", id)
                    .findFirst();

            if (object != null) {
                objectsList.add(object);
            }
        }
        return new ListResult<>(objectsList);
    }

    /**
     * Ищет объекты, в которых значение поля field содержит value
     *
     * @param context Android context
     * @param field   имя поля в котором осуществляется поиск
     * @param value   значение, которое должно содержатся в поле
     * @param tClass  Класс данных
     * @param <T>     Имя класса данных
     * @return Result содержащий все соответствующие объекты
     */
    public static <T extends RealmObject> Result<T> getContains(@NonNull Context context, String field, String value, Class<T> tClass) {

        Realm realm = getRealmForClass(tClass);
        Result<T> result;
        if (realm != null) {
            RealmResults realmResult = realm.where(tClass)
                    .contains(field, value).findAll();
            result = new RealmResult<>(realmResult);
        } else {
            result = new ListResult<>(new ArrayList<T>());
        }
        return result;
    }

    /**
     * Выбирает все объекты в которых значение поля field равно value
     *
     * @param field  имя поля в котором осуществляется поиск
     * @param value  (Boolean) значение, с которым осуществляется сравнение
     * @param tClass Класс данных
     * @param <T>    Имя класса данных
     * @return Result содержащий все соответствующие объекты
     */
    public static <T extends RealmObject> Result<T> getEqual(String field, boolean value, Class<T> tClass) {
        Realm realm = getRealmForClass(tClass);
        Result<T> result = null;
        if (realm != null) {

            RealmResults<T> realmResults = realm.where(tClass)
                    .equalTo(field, value)
                    .findAll();
            result = new RealmResult<T>(realmResults);
        } else {
            result = new ListResult<>(new ArrayList<T>());
        }
        return result;
    }


    /**
     * Выбирает все объекты в которых значение поля field равно value. Перегруженная версия.
     *
     * @param field  имя поля в котором осуществляется поиск
     * @param value  (String) значение, с которым осуществляется сравнение
     * @param tClass Класс данных
     * @param <T>    Имя класса данных
     * @return Result содержащий все соответствующие объекты
     * @see DBHelper#getEqual(String, boolean, Class)
     */
    public static <T extends RealmObject> Result<T> getEqual(String field, String value, Class<T> tClass) {
        Realm realm = getRealmForClass(tClass);
        Result<T> result = null;
        if (realm != null) {

            RealmResults<T> realmResults = realm.where(tClass)
                    .equalTo(field, value)
                    .findAll();
            result = new RealmResult<T>(realmResults);
        } else {
            result = new ListResult<>(new ArrayList<T>());
        }
        return result;
    }

    /**
     * Выбирает все объекты в которых значение поля field равно одному из значений, содержащихся в списке values.
     *
     * @param field  имя поля в котором осуществляется поиск
     * @param values Cписок значений.
     * @param tClass Класс данных
     * @param <T>    Имя класса данных
     * @return Result содержащий все соответствующие объекты
     * @see DBHelper#getAllByNotEqualField(String, List, Class)
     */
    public static <T extends RealmObject> Result<T> getAllByField(String field, List<String> values, Class<T> tClass) {
        Realm realm = getRealmForClass(tClass);

        if (realm != null) {
            RealmQuery query = realm.where(tClass);
            for (int i = 0; i < values.size() - 1; i++) {
                query = query.equalTo(field, values.get(i)).or();
            }
            if (values.size() > 0) {
                query = query.equalTo(field, values.get(values.size() - 1));
                RealmResults realmResult = query.findAll();
                return new RealmResult<>(realmResult);
            } else {
                return new ListResult<>(new ArrayList<T>());
            }
        } else {
            return new ListResult<>(new ArrayList<T>());
        }

    }

    /**
     * Выбирает все объекты в которых значение поля field не равно ни одному из значений, содержащихся в списке values.
     *
     * @param field  имя поля в котором осуществляется поиск
     * @param values Cписок значений.
     * @param tClass Класс данных
     * @param <T>    Имя класса данных
     * @return Result содержащий все соответствующие объекты
     * @see DBHelper#getAllByField(String, List, Class)
     */
    public static <T extends RealmObject> Result<T> getAllByNotEqualField(String field, List<String> values, Class<T> tClass) {
        Realm realm = getRealmForClass(tClass);

        Result<T> result;
        if (realm != null) {
            RealmQuery query = realm.where(tClass);
            for (int i = 0; i < values.size() - 1; i++) {
                query = query.notEqualTo(field, values.get(i));
            }
            if (values.size() > 0) {
                query = query.notEqualTo(field, values.get(values.size() - 1));

            }
            RealmResults realmResults = query.findAll();
            result = new RealmResult<T>(realmResults);
        } else {
            result = new ListResult<>(new ArrayList<T>());
        }

        return result;
    }

    /**
     * Удаляет все данные из бд
     *
     * @param context
     */
    public static void deleteAllDatabases(Context context) {
        RealmConfiguration configurationBase = new RealmConfiguration.Builder(context)
                .name("base.realm")
                .deleteRealmIfMigrationNeeded()
                .build();
        RealmConfiguration configurationNearest = new RealmConfiguration.Builder(context)
                .name("nearest.realm")
                .deleteRealmIfMigrationNeeded()
                .build();

        try {
            Realm realm = Realm.getInstance(configurationBase);
            realm.beginTransaction();
            realm.clear(LoanCouponFixData.class);
            realm.clear(SalesCouponFixData.class);
            realm.clear(Address.class);
            realm.clear(ATM.class);
            realm.clear(Bank.class);
            realm.clear(Category.class);
            realm.clear(City.class);
            realm.clear(Contract.class);
            realm.clear(Department.class);
            realm.clear(Goods.class);
            realm.clear(LoanCoupon.class);
            realm.clear(Outlet.class);
            realm.clear(PaymentSystem.class);
            realm.clear(SalesCoupon.class);
            realm.clear(SubjectType.class);
            realm.clear(Terminal.class);
            realm.clear(User.class);
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("d", "Cant clear all data classes");
        }

        try {
            Realm realm = Realm.getInstance(configurationNearest);
            realm.beginTransaction();
            realm.clear(NearestDepartment.class);
            realm.clear(NearestOutlet.class);
            realm.clear(NearestTerminal.class);
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("d", "Cant clear all nearest classes");
        }

    }

}
