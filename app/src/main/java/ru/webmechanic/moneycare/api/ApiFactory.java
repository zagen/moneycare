package ru.webmechanic.moneycare.api;

import android.support.annotation.NonNull;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.realm.RealmObject;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.deserializers.ContractsDeserializer;
import ru.webmechanic.moneycare.api.deserializers.NearestDepartmentDeserializer;
import ru.webmechanic.moneycare.api.deserializers.NearestOutletDeserializer;
import ru.webmechanic.moneycare.api.deserializers.NearestTerminalsDeserializer;
import ru.webmechanic.moneycare.api.deserializers.OutletDeserializer;
import ru.webmechanic.moneycare.api.deserializers.PaymentSystemDeserializer;
import ru.webmechanic.moneycare.api.deserializers.PaymentSystemsDeserializer;
import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.content.NearestDepartment;
import ru.webmechanic.moneycare.content.NearestOutlet;
import ru.webmechanic.moneycare.content.NearestTerminal;
import ru.webmechanic.moneycare.content.Outlet;
import ru.webmechanic.moneycare.content.PaymentSystem;

/**
 * Created by a.bratusenko on 23.05.16.
 * Класс осуществляющий доступ к сервису API
 */
public class ApiFactory {

    private static final int TIMEOUT = 60;
    private static final int WRITE_TIMEOUT = 120;
    private static final int CONNECT_TIMEOUT = 10;

    private static final OkHttpClient CLIENT = new OkHttpClient();
    private static final Gson GSON = new GsonBuilder()
            .setExclusionStrategies(new ExclusionStrategy() {
                @Override
                public boolean shouldSkipField(FieldAttributes f) {
                    return f.getDeclaringClass().equals(RealmObject.class);
                }

                @Override
                public boolean shouldSkipClass(Class<?> clazz) {
                    return false;
                }
            })              //Регистрация всех кастомных десериализаторов
            .setDateFormat("yyyy-MM-dd' 'HH:mm:ss")
            .registerTypeAdapter(NearestDepartment.class, new NearestDepartmentDeserializer())
            .registerTypeAdapter(PaymentSystem.class, new PaymentSystemDeserializer())
            .registerTypeAdapter(Outlet.class, new OutletDeserializer())
            .registerTypeAdapter(NearestOutlet.class, new NearestOutletDeserializer())
            .registerTypeAdapter(new TypeToken<List<PaymentSystem>>() {}.getType(), new PaymentSystemsDeserializer())
            .registerTypeAdapter(new TypeToken<List<Contract>>() {}.getType(), new ContractsDeserializer())
            .registerTypeAdapter(new TypeToken<List<NearestTerminal>>() {}.getType(), new NearestTerminalsDeserializer())
            .create();

    static {
        CLIENT.setConnectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS);
        CLIENT.setWriteTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS);
        CLIENT.setReadTimeout(TIMEOUT, TimeUnit.SECONDS);
    }

    @NonNull
    public static ApiService getApiService() {
        return getRetrofit().create(ApiService.class);
    }

    @NonNull
    public static Retrofit getRetrofit() {
        CLIENT.interceptors().add(new Interceptor() {
                                      @Override
                                      public Response intercept(Chain chain) throws IOException {
                                          Request originalRequest = chain.request(); //Current Request

                                          Response response = chain.proceed(originalRequest); //Get response of the request

                                          return response;
                                      }
                                  }
        );
        return new Retrofit.Builder()
                .baseUrl(Settings.API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(GSON))
                .client(CLIENT)
                .build();
    }

}