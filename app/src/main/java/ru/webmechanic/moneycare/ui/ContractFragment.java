package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.ui.customviews.ContractView;
import ru.webmechanic.moneycare.ui.utils.Arguments;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class ContractFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener {


    @BindView(R.id.contractView)
    ContractView contractView;
    private Contract contract = null;

    public ContractFragment() {
    }


    @Override
    protected void fetchData() {
        isLoading = true;
        new DataFetcher(this, loaderContext, this).one(arguments.getId()).fetch();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contract, container, false);
        ButterKnife.bind(this, view);

        contractView.setButtonsShown(true);
        contractView.setExpansible(true);
        contractView.setOnGoodsClickListener(null);
        arguments.setContract(arguments.getId());
        return view;
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        if (contract != null) {
            contractView.setContract(contract);
            contractView.setVisibility(View.VISIBLE);
            boolean isPayed = settings.getStorage().isContractPayed(arguments.getId());
            contractView.setButtonsShown(!isPayed);
            contractView.setDetailsVisible(true);
            contractView.setFirstButtonOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoFragment(FragmentFactory.CONTRACTS_PAYED, arguments);
                }
            });
            contractView.setSecondButtonOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoFragment(FragmentFactory.SELECT_PAY_METHOD, arguments);
                }
            });
        }
    }

    @Override
    public String getTitle() {
        String number = "****";
        if (getArguments() != null && getArguments().containsKey(Arguments.ARG_ID)) {
            number = getArguments().getString(Arguments.ARG_ID);
        }
        return settings.getContext().getString(R.string.credit_num) + number;
    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        if (loaderid == R.id.contracts_loader) {
            try {
                this.contract = (Contract) result.first();
            } catch (Exception e) {

            }
        }
        isLoading = false;
        updateUI();

    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }
}
