package ru.webmechanic.moneycare.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Bank;
import ru.webmechanic.moneycare.ui.utils.Utils;
import ru.webmechanic.moneycare.ui.utils.imageloader.ImageLoader;

/**
 * Created by a.bratusenko on 07.06.16.
 */
public class BankView extends LinearLayout {

    @BindView(R.id.bankDepartmentValueTextView)
    TextView departmentsValueTextView;

    @BindView(R.id.bankDepartmentTitleTextView)
    TextView departmentsTitleTextView;

    @BindView(R.id.bankAtmValueTextView)
    TextView atmsValueTextView;

    @BindView(R.id.bankAtmTitleTextView)
    TextView atmsTitleTextView;

    @BindView(R.id.bankCommissionValueTextView)
    TextView commissionsValueTextView;

    @BindView(R.id.bankCommissionTitleTextView)
    TextView commissionsTitleTextView;

    @BindView(R.id.bankDepartmentContainer)
    LinearLayout departmentContainer;

    @BindView(R.id.bankAtmContainer)
    LinearLayout atmContainer;

    @BindView(R.id.bankCommissionContainer)
    LinearLayout commissionContainer;

    @BindView(R.id.bankLogoImageView)
    ImageView logoImageView;

    private ImageLoader loader;

    public BankView(Context context) {
        super(context);
        init();
    }

    public BankView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BankView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.customviews_bank, this);
        ButterKnife.bind(this, this);
        loader = new ImageLoader(getContext(), R.drawable.bank);
    }

    public void setBank(Bank bank) {
        String departmentCount = Integer.toString(bank.getDepartments().size());

        departmentsValueTextView.setText(departmentCount);
        departmentsTitleTextView.setText(Utils.formatDepartments(departmentCount));

        String atmsCount = Integer.toString(bank.getAtms().size());
        atmsTitleTextView.setText(Utils.formatAtms(atmsCount));
        atmsValueTextView.setText(atmsCount);

        commissionsValueTextView.setText(String.format(getResources().getString(R.string.commission_from), bank.getPercent()));
        commissionsTitleTextView.setText(getResources().getString(R.string.commissions));

        loader.DisplayImage(bank.getLogo(), logoImageView);

    }

    public void setOnDepartmentClickListener(View.OnClickListener listener) {
        departmentContainer.setOnClickListener(listener);
    }

    public void setOnAtmClickListener(View.OnClickListener listener) {
        atmContainer.setOnClickListener(listener);
    }

    public void setOnCommissionClickListener(View.OnClickListener listener) {
        commissionContainer.setOnClickListener(listener);
    }
}
