package ru.webmechanic.moneycare.ui.utils;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.ui.BaseFragment;
import ru.webmechanic.moneycare.ui.FragmentFactory;

/**
 * Created by a.bratusenko on 21.06.16.
 * Класс для навигации по фрагментам
 */
public class FragmentsStack {
    private FragmentManager fragmentManager;
    private BaseFragment.OnLoaderFragmentFinishedListener listener;
    private Stack<BaseFragment> fragmentsStack;

    public FragmentsStack(FragmentManager fragmentManager, BaseFragment.OnLoaderFragmentFinishedListener listener) {
        this.fragmentManager = fragmentManager;
        this.fragmentsStack = new Stack<>();
        this.listener = listener;
    }

    /**
     * Осуществляет поиск всех включенных в активити фрагментов, и помещает их в стэк
     *
     * @param tags
     */
    public void restoreByTags(List<String> tags) {
        for (String tag : tags) {
            //поиск всех подключенных фрагментов
            Fragment fragment = fragmentManager.findFragmentByTag(tag);
            if (fragment != null && fragment instanceof BaseFragment) {
                BaseFragment baseFragment = (BaseFragment) fragment;
                baseFragment.setOnLoaderFragmentFinishedListener(listener);
                fragmentsStack.push(baseFragment);
            }
        }
        if (!fragmentsStack.empty()) {
            //скрываем все найденные фрагменты кроме последнего
            FragmentTransaction ft = fragmentManager.beginTransaction();

            for (Fragment fragment : fragmentsStack) {
                ft.hide(fragment);
            }

            ft.show(fragmentsStack.lastElement());
            ft.commit();
        }
    }

    /**
     * отобразить последний элемент в стеке
     */
    public void showLast() {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        fragmentsStack.lastElement().onResume();
        ft.show(fragmentsStack.lastElement());
        ft.commit();
    }

    /**
     * Метод для проверки существования фрагмента в стеке
     *
     * @param fragmentName имя фрагмента
     * @return true если фрагмент в стеке
     */
    private boolean isFragmentInStack(String fragmentName) {
        boolean result = false;
        for (BaseFragment fragment :
                fragmentsStack) {
            if (fragment.getTag() != null && fragment.getTag().equals(fragmentName)) {
                result = true;
                break;
            }
        }
        return result;
    }

    private void clear() {
        clearTo("");
    }

    /**
     * Отключает и удаляет все элементы стека, до тех пор пока не будет пуст, или не будет найден элемент с тэгом
     *
     * @param fragmentName имя фрагмента, до которого осуществляется очистка
     */
    private void clearTo(String fragmentName) {
        if (fragmentName == null) {
            fragmentName = "";
        }
        FragmentTransaction ft = fragmentManager.beginTransaction();
        while (!fragmentsStack.empty() && !fragmentName.equals(fragmentsStack.lastElement().getTag())) {
            fragmentsStack.lastElement().onPause();
            ft.hide(fragmentsStack.lastElement());
            ft.remove(fragmentsStack.pop());
        }
        if (!fragmentsStack.empty()) {
            ft.show(fragmentsStack.lastElement());
            ft.commit();
            fragmentsStack.lastElement().onResume();
        } else {
            ft.commit();
        }
    }

    /**
     * @return true если ни одного фрагмента нет
     */
    public boolean empty() {
        return fragmentsStack.empty();
    }

    /**
     * Помещает фрагмент в стек
     *
     * @param fragment     добавляемый фрагмент
     * @param fragmentName имя фрагмента
     */
    public void push(BaseFragment fragment, String fragmentName) {

        if (!fragmentsStack.empty()) {
            if (fragmentName.equals(FragmentFactory.MAIN_MENU)) { //если переходим к главному экрану, то подчищаем историю
                clear();
            }
        }

        if (isFragmentInStack(fragmentName)) {//если такой фрагмент уже есть, то ищем его и открываем
            clearTo(fragmentName);
        } else {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            fragment.setOnLoaderFragmentFinishedListener(listener);
            if (!fragmentsStack.empty()) {
                fragmentsStack.lastElement().onPause();
                ft.hide(fragmentsStack.lastElement());
            }
            ft.add(R.id.mainActivityContainer, fragment, fragmentName);
            fragmentsStack.push(fragment);
            ft.commit();
        }
    }

    /**
     * Удаление верхнего фрагмента
     */
    public void pop() {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        fragmentsStack.lastElement().onPause();
        ft.remove(fragmentsStack.pop());
        ft.show(fragmentsStack.lastElement());
        ft.commit();
        fragmentsStack.lastElement().onResume();
    }

    /**
     * @return текущее количество фрагментов в стеке
     */
    public int size() {
        return fragmentsStack.size();
    }

    /**
     * Собирает теги всех элементов в стеке в один список
     *
     * @return
     */
    public ArrayList<String> getTags() {
        Iterator<BaseFragment> fragmentIterator = fragmentsStack.iterator();
        ArrayList<String> tags = new ArrayList<>();

        while (fragmentIterator.hasNext()) {
            tags.add(fragmentIterator.next().getTag());
        }
        return tags;
    }

    /**
     * @return верхний фрагмент
     */
    public BaseFragment lastElement() {
        return fragmentsStack.lastElement();
    }
}
