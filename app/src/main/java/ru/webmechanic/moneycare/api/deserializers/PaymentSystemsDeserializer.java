package ru.webmechanic.moneycare.api.deserializers;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ru.webmechanic.moneycare.content.PaymentSystem;

/**
 * Created by a.bratusenko on 27.05.16.
 * Класс для десериализации списка объектов типа PaymentSystem.
 *
 * @see PaymentSystemBasicDeserializer
 */
public class PaymentSystemsDeserializer implements JsonDeserializer<List<PaymentSystem>> {
    @Override
    public List<PaymentSystem> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<PaymentSystem> pss = new ArrayList<>();
        try {
            JsonArray jsonPss = ((JsonObject) json).get("pss").getAsJsonArray();

            PaymentSystemBasicDeserializer paymentSystemBasicDeserializer = new PaymentSystemBasicDeserializer();
            for (int i = 0; i < jsonPss.size(); i++) {
                PaymentSystem paymentSystem = paymentSystemBasicDeserializer.deserialize(jsonPss.get(i).getAsJsonObject(), typeOfT, context);
                pss.add(paymentSystem);
            }
        } catch (Exception e) {
        }
        return pss;
    }
}
