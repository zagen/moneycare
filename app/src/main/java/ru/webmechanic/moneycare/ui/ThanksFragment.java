package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.webmechanic.moneycare.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ThanksFragment extends BaseFragment {

    public ThanksFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void fetchData() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_thanks, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.coupon);
    }

    @OnClick(R.id.thanksButton)
    void showMainMenu() {
        gotoFragment(FragmentFactory.MAIN_MENU);
    }
}
