package ru.webmechanic.moneycare.api.deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;

import ru.webmechanic.moneycare.content.Address;

/**
 * Created by a.bratusenko on 27.05.16.
 * Десериализатор для класса Address. Необходим для кастомной десериализации других классов, его содержащие
 */
public class AddressDeserializer implements JsonDeserializer<Address> {
    @Override
    public Address deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Address address = new Address();
        JsonObject jsAddress = (JsonObject) json;
        try {
            address.setId(jsAddress.get("id").getAsString());
        } catch (Exception e) {
            address.setId("");
        }
        try {
            address.setLongitude(jsAddress.get("longitude").getAsString());
        } catch (Exception e) {
            address.setLongitude(null);
        }
        try {
            address.setLatitude(jsAddress.get("latitude").getAsString());
        } catch (Exception e) {
            address.setLatitude(null);
        }
        try {
            address.setAddress(jsAddress.get("address").getAsString());
        } catch (Exception e) {
            address.setAddress(null);
        }
        try {
            address.setCity(jsAddress.get("city").getAsString());
        } catch (Exception e) {
            address.setCity(null);
        }
        try {
            address.setFullAddress(jsAddress.get("fulladdress").getAsString());
        } catch (Exception e) {
            address.setFullAddress("");
        }


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
        try {
            address.setCreatingDate(sdf.parse(jsAddress.get("created_at").getAsString()));
        } catch (Exception e) {
            address.setCreatingDate(null);
        }
        try {
            address.setUpdatingDate(sdf.parse(jsAddress.get("updated_at").getAsString()));
        } catch (Exception e) {
            address.setUpdatingDate(null);
        }
        return address;
    }
}
