package ru.webmechanic.moneycare.ui.utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.NearestOutlet;
import ru.webmechanic.moneycare.database.Result;

/**
 * Created by a.bratusenko on 30.05.16.
 */
public class NearestOutletsAdapter extends BaseAdapter {
    private Result<NearestOutlet> nearestOutletResult;
    private LayoutInflater inflater;

    public NearestOutletsAdapter(Context context, Result<NearestOutlet> nearestOutletResult) {
        this.nearestOutletResult = nearestOutletResult;
        this.inflater = ((Activity) context).getLayoutInflater();
    }

    @Override
    public int getCount() {
        return nearestOutletResult.size();
    }

    @Override
    public Object getItem(int position) {
        return nearestOutletResult.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_outlets_nearest_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        NearestOutlet nearestOutlet = nearestOutletResult.getItem(position);

        viewHolder.titleView.setText(nearestOutlet.getOutlet().getOrganization());
        viewHolder.descView.setText(nearestOutlet.getOutlet().getTitle());

        viewHolder.distanceTextView.setText(Utils.asDistance(nearestOutlet.getDistance()));
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.outletTitleTextViewItem)
        TextView titleView;
        @BindView(R.id.outletDescTextViewItem)
        TextView descView;
        @BindView(R.id.stickerFlagOutletDistanceTextViewItem)
        TextView distanceTextView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
