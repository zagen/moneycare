package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;
import java.util.List;

import retrofit.Call;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.SalesCoupon;

/**
 * Created by a.bratusenko on 26.05.16.
 */
public class SalesCouponsLoader extends BaseLoader {
    public SalesCouponsLoader(Context context, Bundle args) {
        super(context);
    }

    @Override
    protected Response apiCall() throws IOException {
        Response response = new Response();
        List<SalesCoupon> salesCoupons = null;
        Settings settings = Settings.getInstance(getContext());
        String[] params = settings.prepareParams();

        Call<List<SalesCoupon>> call = service.salesCoupons(params[0], params[1], params[2], params[3], params[4]);
        retrofit.Response<List<SalesCoupon>> retrofitResponse = call.execute();

        if (retrofitResponse != null) {
            if (!retrofitResponse.isSuccess()) {
                response.setCode(retrofitResponse.code());
                if (retrofitResponse.errorBody() != null) {
                    response.setErrorMessage(retrofitResponse.message());
                }
            } else {
                salesCoupons = retrofitResponse.body();
            }
        }
        response.setRequestResult(RequestResult.SUCCESS)
                .setAnswer(salesCoupons);
        return response;
    }
}
