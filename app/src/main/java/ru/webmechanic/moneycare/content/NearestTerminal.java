package ru.webmechanic.moneycare.content;

import io.realm.RealmObject;

/**
 * Created by a.bratusenko on 09.06.16.
 * Содержит информацию о ближайшем терминале оплаты платежной системы.
 */
public class NearestTerminal extends RealmObject {
    private Terminal terminal;
    private int distance;
    private String psTitle;

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getPsTitle() {
        return psTitle;
    }

    public void setPsTitle(String psTitle) {
        this.psTitle = psTitle;
    }
}
