package ru.webmechanic.moneycare.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.User;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.ui.utils.RussiaCellPhoneNumberTextWatcher;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class LoginFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener {


    @BindView(R.id.doLoginButton)
    Button loginButton;

    @BindView(R.id.phoneLoginEditText)
    EditText phoneEditText;

    @BindView(R.id.passwordLoginEditText)
    EditText passwordEditText;

    public LoginFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);

        phoneEditText.addTextChangedListener(new RussiaCellPhoneNumberTextWatcher(phoneEditText));

        if (arguments.isSetPhone()) {
            phoneEditText.setText(arguments.getPhone());
        }
        if (arguments.isSetPassword()) {
            passwordEditText.setText(arguments.getPassword());
        }
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        arguments.setPhone(phoneEditText.getText().toString());
        arguments.setPassword(passwordEditText.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @OnClick(R.id.registerLoginButton)
    void register() {
        gotoFragment(FragmentFactory.REGISTER);
    }

    @OnClick(R.id.doLoginButton)
    void login() {
        if (isInputCorrect()) {
            hideKeyboard();
            arguments.setPhone(phoneEditText.getText().toString().replaceAll("[^\\d]", "").substring(1));
            arguments.setPassword(passwordEditText.getText().toString());
            isLoading = true;
            updateUI();
            new DataFetcher(this, loaderContext, this).fetchFromWeb();
        } else {
            Toast.makeText(getActivity(), getString(R.string.fill_fields), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        boolean isInputEnabled = !isLoading && settings.isInternetAvailable();
        loginButton.setEnabled(isInputEnabled);
    }

    @Override
    protected void fetchData() {

    }

    private boolean isInputCorrect() {
        String phone = phoneEditText.getText().toString().replaceAll("[^\\d]", "");
        return phone.trim().length() == 11 && passwordEditText.getText().toString().trim().length() >= 1;
    }


    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.app_name);
    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {

        User user = (User) result.first();
        if (user != null) {

            gotoFragment(FragmentFactory.MAIN_MENU);
        } else {
            Toast.makeText(getActivity(), "Не удалось войти, проверьте правильность телефона и пароля", Toast.LENGTH_SHORT).show();
        }
        arguments.setPassword(null);
        arguments.setPhone(null);
        isLoading = false;
        updateUI();
    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }
}
