package ru.webmechanic.moneycare.api.deserializers;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;

import io.realm.RealmList;
import ru.webmechanic.moneycare.content.PaymentSystem;
import ru.webmechanic.moneycare.content.Terminal;

/**
 * Created by a.bratusenko on 27.05.16.
 * Класс для десериализации платежной системы из элемента json,
 * содержащего всю необходимую информацию для формирования объекта с данными
 */
public class PaymentSystemBasicDeserializer implements JsonDeserializer<PaymentSystem> {
    @Override
    public PaymentSystem deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        PaymentSystem paymentSystem = new PaymentSystem();

        JsonObject jsonPs = (JsonObject) json;
        try {
            paymentSystem.setId(jsonPs.get("id").getAsString());
        } catch (Exception e) {
            paymentSystem.setId(null);
        }
        try {
            paymentSystem.setLogo(jsonPs.get("logo").getAsString());
        } catch (Exception e) {
            paymentSystem.setLogo("");
        }
        try {
            paymentSystem.setPercent(jsonPs.get("percent").getAsString());
        } catch (Exception e) {
            paymentSystem.setPercent("N/A");
        }
        try {
            paymentSystem.setTitle(jsonPs.get("title").getAsString());
        } catch (Exception e) {
            paymentSystem.setTitle(null);
        }


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
        try {
            paymentSystem.setCreatingDate(sdf.parse(jsonPs.get("created_at").getAsString()));
        } catch (Exception e) {
            paymentSystem.setCreatingDate(null);
        }
        try {
            paymentSystem.setUpdatingDate(sdf.parse(jsonPs.get("updated_at").getAsString()));
        } catch (Exception e) {
            paymentSystem.setUpdatingDate(null);
        }

        try {
            JsonArray jsonTerminals = jsonPs.get("terminals").getAsJsonArray();
            TerminalDeserializer terminalJsonDeserializer = new TerminalDeserializer();
            RealmList<Terminal> terminals = new RealmList<>();
            for (int i = 0; i < jsonTerminals.size(); i++) {
                Terminal terminal = terminalJsonDeserializer.deserialize(jsonTerminals.get(i).getAsJsonObject(), typeOfT, context);
                terminals.add(terminal);
            }
            paymentSystem.setTerminals(terminals);
        } catch (Exception e) {
            paymentSystem.setTerminals(null);
        }

        return paymentSystem;

    }
}
