package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;

import retrofit.Call;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.User;
import ru.webmechanic.moneycare.ui.utils.Arguments;

/**
 * Created by a.bratusenko on 26.05.16.
 */
public class CreateUserLoader extends BaseLoader {
    private String phone = null;
    private String password = null;

    public CreateUserLoader(Context context, Bundle args) {
        super(context);
        if (args.containsKey(Arguments.ARG_PHONE) && args.containsKey(Arguments.ARG_PASSWORD)) {
            this.password = args.getString(Arguments.ARG_PASSWORD);
            this.phone = args.getString(Arguments.ARG_PHONE);

        }
    }

    @Override
    protected Response apiCall() throws IOException {
        User user = null;
        Response response = new Response();
        try {

            Call<User> call = service.createUser(phone, password);
            retrofit.Response<User> retrofitResponse = call.execute();

            if (retrofitResponse != null) {
                if (!retrofitResponse.isSuccess() && retrofitResponse.errorBody() != null) {
                    response.setCode(retrofitResponse.code());
                    response.setErrorMessage(retrofitResponse.message());
                } else {
                    user = retrofitResponse.body();
                }
            }
        } catch (IOException e) {
            //DO NETWORK ERROR HANDLING HERE
        }


        response.setCache(false);
        return response.setRequestResult(RequestResult.SUCCESS)
                .setCache(false)
                .setAnswer(user);
    }
}
