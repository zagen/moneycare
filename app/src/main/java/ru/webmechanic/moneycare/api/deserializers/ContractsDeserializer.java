package ru.webmechanic.moneycare.api.deserializers;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ru.webmechanic.moneycare.content.Contract;

/**
 * Created by a.bratusenko on 27.05.16.
 * Класс для сериализации списка договоров
 */
public class ContractsDeserializer implements JsonDeserializer<List<Contract>> {
    @Override
    public List<Contract> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<Contract> contracts = new ArrayList<>();
        JsonArray jsonContracts = ((JsonObject) json).get("contracts").getAsJsonArray();
        ContractDeserializer contractDeserializer = new ContractDeserializer();
        for (int i = 0; i < jsonContracts.size(); i++) {
            Contract contract = contractDeserializer.deserialize(jsonContracts.get(i), typeOfT, context);
            contracts.add(contract);
        }
        return contracts;
    }
}
