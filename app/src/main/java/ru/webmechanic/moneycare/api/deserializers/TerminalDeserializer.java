package ru.webmechanic.moneycare.api.deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;

import ru.webmechanic.moneycare.content.Terminal;

/**
 * Created by a.bratusenko on 27.05.16.
 * Десериализатор для терминалов. Используется для десериализации ближайших терминалов.
 *
 * @author a.bratusenko
 * @see NearestTerminalsDeserializer
 */
public class TerminalDeserializer implements JsonDeserializer<Terminal> {
    @Override
    public Terminal deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Terminal terminal = new Terminal();
        JsonObject jsonTerminal = (JsonObject) json;

        try {
            terminal.setId(jsonTerminal.get("id").getAsString());
        } catch (Exception e) {
            terminal.setId(null);
        }
        try {
            terminal.setAddress(new AddressDeserializer().deserialize(jsonTerminal.get("address").getAsJsonObject(), typeOfT, context));
        } catch (Exception e) {
            terminal.setAddress(null);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
        try {
            terminal.setCreatingDate(sdf.parse(jsonTerminal.get("created_at").getAsString()));
        } catch (Exception e) {
            terminal.setCreatingDate(null);
        }
        try {
            terminal.setUpdatingDate(sdf.parse(jsonTerminal.get("updated_at").getAsString()));
        } catch (Exception e) {
            terminal.setUpdatingDate(null);
        }

        return terminal;
    }
}
