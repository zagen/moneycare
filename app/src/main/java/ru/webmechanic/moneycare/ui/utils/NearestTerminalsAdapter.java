package ru.webmechanic.moneycare.ui.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.NearestTerminal;
import ru.webmechanic.moneycare.database.Result;

/**
 * Created by a.bratusenko on 09.06.16.
 */
public class NearestTerminalsAdapter extends BaseAdapter {
    private Context context;
    private Result<NearestTerminal> result;

    private NearestTerminalAdapterListener listener;

    public NearestTerminalsAdapter(Context context, Result<NearestTerminal> result) {
        this.result = result;
        this.context = context;
    }

    @Override
    public int getCount() {
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        return result.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_nearest_departments_atms, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final NearestTerminal nearestTerminal = result.getItem(position);
        viewHolder.titleTextView.setText(nearestTerminal.getPsTitle());
        viewHolder.addressTextView.setText(nearestTerminal.getTerminal().getAddress().getFullAddress());
        viewHolder.workTimeTextView.setText(String.format(context.getString(R.string.time_of_work), "N/A"));
        viewHolder.distanceTextView.setText(Utils.asDistance(nearestTerminal.getDistance()));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onNearestTerminalsClicked(nearestTerminal);
            }
        });
        return convertView;
    }

    public void setListener(NearestTerminalAdapterListener listener) {
        this.listener = listener;
    }

    public interface NearestTerminalAdapterListener {
        void onNearestTerminalsClicked(NearestTerminal terminal);
    }

    static class ViewHolder {
        @BindView(R.id.nearestDepATMTitleTextViewItem)
        TextView titleTextView;
        @BindView(R.id.nearestDepATMAddressTextViewItem)
        TextView addressTextView;
        @BindView(R.id.stickerFlagNearestDepATMDistanceTextViewItem)
        TextView distanceTextView;
        @BindView(R.id.nearestDepATMWorkTimeTextViewItem)
        TextView workTimeTextView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
