package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.ui.customviews.ContractView;


public class PaymentMethodsFragment extends BaseFragment {


    @BindView(R.id.paymentMethodContractView)
    ContractView contractView;

    public PaymentMethodsFragment() {
    }

    @Override
    protected void fetchData() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_methods, container, false);

        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (arguments.isSetContract()) {
            String id = arguments.getContract();
            Contract contract = null;
            try {
                contract = (Contract) new DataFetcher(this, Contract.class).one(id).fetchFromCache().first();
            } catch (Exception e) {
            }
            if (contract != null) {
                contractView.setContract(contract);
                contractView.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.payment_methods);
    }


    @OnClick(R.id.paymentMethodBankLinearLayout)
    void showBanks() {
        gotoFragment(FragmentFactory.BANKS, arguments);
    }

    @OnClick(R.id.paymentMethodPaymentSystemLinearLayout)
    void showPaymentSystem() {
        gotoFragment(FragmentFactory.PAYMENT_SYSTEMS, arguments);

    }

}
