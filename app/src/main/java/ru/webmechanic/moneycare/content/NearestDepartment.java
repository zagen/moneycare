package ru.webmechanic.moneycare.content;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by a.bratusenko on 26.05.16.
 * Класс данных, хранит информацию о ближайшем отделении будь то отделение банка или банкомат.
 */
public class NearestDepartment extends RealmObject {

    @SerializedName("department")
    private Department department;

    @SerializedName("atm")
    private ATM atm;

    @SerializedName("distance")
    private int distance;

    @SerializedName("bank")
    private String bankName;

    private String bankId;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public ATM getAtm() {
        return atm;
    }

    public void setAtm(ATM atm) {
        this.atm = atm;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }
}
