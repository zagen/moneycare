package ru.webmechanic.moneycare.ui.customviews;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Category;
import ru.webmechanic.moneycare.database.Result;

/**
 * Created by a.bratusenko on 07.06.16.
 */
public class CategoriesView extends LinearLayout {

    @BindView(R.id.categoriesContainer)
    LinearLayout categoryListContainer;
    private OnCategorySelectedListener categorySelectedListener = null;
    private OnShowAllCategoriesListener showAllCategoriesListener = null;
    private String currentCategory = null;
    private Result<Category> categories;
    public CategoriesView(Context context) {
        super(context);
        init();
    }


    public CategoriesView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CategoriesView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setShowAllCategoriesListener(OnShowAllCategoriesListener showAllCategoriesListener) {
        this.showAllCategoriesListener = showAllCategoriesListener;
    }

    public void setCategorySelectedListener(OnCategorySelectedListener categorySelectedListener) {
        this.categorySelectedListener = categorySelectedListener;
    }

    private void init() {
        inflate(getContext(), R.layout.customviews_categories, this);
        ButterKnife.bind(this, this);
    }

    private View createCategoryItem(final Category category) {
        View element = inflate(getContext(), R.layout.list_item_category, null);
        final TextView categoryTextView = (TextView) element.findViewById(R.id.categoryTextView);

        categoryTextView.setText(category.getTitle());

        if (category.getTitle().equals(currentCategory)) {
            categoryTextView.setTextColor(Color.BLACK);
        }
        categoryTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (categoryTextView.getText().equals(getContext().getString(R.string.all))) {
                    setVisibility(View.GONE);
                    currentCategory = null;
                } else {
                    currentCategory = categoryTextView.getText().toString();
                    showCurrentCategory();
                }
                categories = null;
                if (categorySelectedListener != null) {
                    categorySelectedListener.onCategorySelected(currentCategory);
                }
            }
        });
        return element;
    }

    private void showCurrentCategory() {
        categoryListContainer.removeAllViews();
        View element = inflate(getContext(), R.layout.list_item_category, null);
        TextView categoryTextView = (TextView) element.findViewById(R.id.categoryTextView);

        categoryTextView.setText(currentCategory);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        categoryTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showAllCategoriesListener != null) {
                    showAllCategoriesListener.onShowAllCategory();
                }
            }
        });
        categoryListContainer.addView(element, 0, layoutParams);
        setVisibility(View.VISIBLE);
    }

    private void showCategories() {

        categoryListContainer.removeAllViews();
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        for (int i = 0; i < categories.size(); i++) {
            categoryListContainer.addView(createCategoryItem(categories.getItem(i)), layoutParams);

        }
        Category allCategory = new Category();
        allCategory.setTitle(getContext().getString(R.string.all));
        categoryListContainer.addView(createCategoryItem(allCategory), 0, layoutParams);

        setVisibility(View.VISIBLE);

    }

    public boolean isVisible() {
        return (getVisibility() != GONE);
    }

    public void setVisible(boolean visible) {
        setVisibility(visible ? VISIBLE : GONE);
    }

    public String getCurrentCategory() {
        return currentCategory;
    }

    public void setCurrentCategory(String category) {
        this.currentCategory = category;
        if (categorySelectedListener != null) {
            categorySelectedListener.onCategorySelected(category);
        }
        showCurrentCategory();
    }

    public Result<Category> getCategories() {
        return categories;
    }

    public void setCategories(Result<Category> categories) {
        this.categories = categories;
        showCategories();
    }

    public interface OnCategorySelectedListener {
        void onCategorySelected(String category);
    }

    public interface OnShowAllCategoriesListener {
        void onShowAllCategory();
    }

}
