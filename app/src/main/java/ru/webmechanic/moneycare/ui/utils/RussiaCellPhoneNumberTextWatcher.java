package ru.webmechanic.moneycare.ui.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Created by a.bratusenko on 31.05.16.
 * Класс необходимый для форматированного ввода мобильного телефона в России
 */
public class RussiaCellPhoneNumberTextWatcher implements TextWatcher {
    private EditText editText;
    private boolean isInnerChange = true;

    public RussiaCellPhoneNumberTextWatcher(EditText editText) {
        this.editText = editText;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (isInnerChange) {
            String clean = s.toString().replaceAll("[^\\d]", "");
            if (clean.length() >= 1)
                clean = clean.substring(1);
            clean = clean + "           ";
            clean = clean.substring(0, 10);
            String phone = String.format("+7 (%s) %s-%s-%s", clean.substring(0, 3).trim(), clean.substring(3, 6).trim(), clean.substring(6, 8).trim(), clean.substring(8, 10).trim());

            StringBuilder builder = new StringBuilder(phone);

            while (builder.length() > 0 && !Character.isDigit(builder.charAt(builder.length() - 1))) {
                builder.setLength(builder.length() - 1);
            }
            phone = builder.toString();
            isInnerChange = false;
            editText.setText(phone);
            isInnerChange = true;
            editText.setSelection(editText.getText().toString().length());
        }

    }
}
