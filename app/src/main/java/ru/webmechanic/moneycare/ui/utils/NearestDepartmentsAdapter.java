package ru.webmechanic.moneycare.ui.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.NearestDepartment;
import ru.webmechanic.moneycare.database.Result;

/**
 * Created by a.bratusenko on 09.06.16.
 */
public class NearestDepartmentsAdapter extends BaseAdapter {
    private Context context;
    private Result<NearestDepartment> result;
    private NearestDepartmentsAdapterListener listener;

    public NearestDepartmentsAdapter(Context context, Result<NearestDepartment> result) {
        this.context = context;
        this.result = result;
    }

    public void setListener(NearestDepartmentsAdapterListener listener) {
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        return result.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_nearest_departments_atms, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final NearestDepartment nearestDepartment = result.getItem(position);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onNeedShowMapForObject(nearestDepartment);
                }
            }
        });
        if (nearestDepartment.getDepartment() != null) {
            viewHolder.titleTextView.setText(String.format(context.getString(R.string.department_of_bank), nearestDepartment.getBankName()));
            viewHolder.addressTextView.setText(nearestDepartment.getDepartment().getAddress().getFullAddress());
            viewHolder.workTimeTextView.setText(String.format(context.getString(R.string.time_of_work), "N/A"));

        } else if (nearestDepartment.getAtm() != null) {
            viewHolder.titleTextView.setText(String.format(context.getString(R.string.atm_of_bank), nearestDepartment.getBankName()));
            viewHolder.addressTextView.setText(nearestDepartment.getAtm().getAddress().getFullAddress());
            viewHolder.workTimeTextView.setText(String.format(context.getString(R.string.time_of_work), "N/A"));
        }
        viewHolder.distanceTextView.setText(Utils.asDistance(nearestDepartment.getDistance()));

        return convertView;
    }

    public interface NearestDepartmentsAdapterListener {
        void onNeedShowMapForObject(NearestDepartment nearestDepartment);

    }

    static class ViewHolder {
        @BindView(R.id.nearestDepATMTitleTextViewItem)
        TextView titleTextView;
        @BindView(R.id.nearestDepATMAddressTextViewItem)
        TextView addressTextView;
        @BindView(R.id.nearestDepATMWorkTimeTextViewItem)
        TextView workTimeTextView;
        @BindView(R.id.stickerFlagNearestDepATMDistanceTextViewItem)
        TextView distanceTextView;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
