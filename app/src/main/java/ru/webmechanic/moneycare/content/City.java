package ru.webmechanic.moneycare.content;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by a.bratusenko on 23.05.16.
 * Класс, содержащих информацию о городе
 */

public class City extends RealmObject {

    @SerializedName("id")
    private String id;

    @SerializedName("city")
    private String city;

    @SerializedName("created_at")
    private Date сreationData;

    @SerializedName("updated_at")
    private Date updatingData;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getСreationData() {
        return сreationData;
    }

    public void setСreationData(Date сreationData) {
        this.сreationData = сreationData;
    }

    public Date getUpdatingData() {
        return updatingData;
    }

    public void setUpdatingData(Date updatingData) {
        this.updatingData = updatingData;
    }

}
