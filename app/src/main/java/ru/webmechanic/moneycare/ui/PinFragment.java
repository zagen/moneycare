package ru.webmechanic.moneycare.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.User;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class PinFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener {

    private final static int TIMER_TIME = 10;//seconds

    @BindView(R.id.resendPinButton)
    Button timerButton;

    @BindView(R.id.confirmPinButton)
    Button confirmPinButton;

    @BindView(R.id.pinEditText)
    EditText pinEditText;

    private Handler timerHandler = new Handler();
    private long startTime;
    private boolean isTimerWorking;
    private Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            long millis = System.currentTimeMillis() - startTime;
            int seconds = (int) (millis / 1000);
            seconds = seconds % 60;

            if (timerButton != null && seconds <= TIMER_TIME) {
                timerButton.setText(String.format(getActivity().getString(R.string.resend_pin_again_after_n_sec_label), TIMER_TIME - seconds));
                timerHandler.postDelayed(this, 500);
            } else {
                stopTimer();
            }
        }
    };


    public PinFragment() {
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.app_name);
    }

    private void startTimer() {
        isTimerWorking = true;
        startTime = System.currentTimeMillis();
        timerHandler.postDelayed(timerRunnable, 0);
        timerButton.setEnabled(!isTimerWorking);
        timerButton.setBackgroundColor(Color.TRANSPARENT);
    }

    private void stopTimer() {
        isTimerWorking = false;
        timerHandler.removeCallbacks(timerRunnable);
        this.timerButton.setText(R.string.resend_pin_again_label);
        timerButton.setEnabled(!isTimerWorking);
        timerButton.setBackgroundResource(R.drawable.dashed);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pin, container, false);
        ButterKnife.bind(this, view);
        startTimer();
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        stopTimer();
    }


    @Override
    protected void updateUI() {
        super.updateUI();
        timerButton.setEnabled(!isLoading);
        confirmPinButton.setEnabled(!isLoading);
    }

    @Override
    protected void fetchData() {
        isLoading = true;
        new DataFetcher(this, loaderContext, this).fetchFromWeb();
    }

    @OnClick(R.id.confirmPinButton)
    void confirm() {
        String pin = pinEditText.getText().toString().trim();
        if (pin.length() > 0) {
            arguments.setPin(pin);
            loaderContext.setLoader(R.id.confirm_pin_loader);
            fetchData();
        }
        updateUI();
    }

    @OnClick(R.id.resendPinButton)
    void resend() {
        if (!isTimerWorking && !isLoading) {
            loaderContext.setLoader(R.id.resend_pin_loader);
            fetchData();
            startTimer();
        }
        updateUI();
    }


    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {

        if (loaderid == R.id.confirm_pin_loader) {
            User user = (User) result.first();
            if (user != null && user.getStatus() == 1) {
                gotoFragment(FragmentFactory.MAIN_MENU);
            } else {
                Toast.makeText(getActivity(), getString(R.string.incorrect_pin), Toast.LENGTH_SHORT).show();
            }
        }
        isLoading = false;
        updateUI();
    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }
}
