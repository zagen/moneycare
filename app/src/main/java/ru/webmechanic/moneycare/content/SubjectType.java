package ru.webmechanic.moneycare.content;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by a.bratusenko on 27.05.16.
 * Класс данных, необходим для получения данных для поиска товаров
 */
public class SubjectType extends RealmObject {
    @SerializedName("title")
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
