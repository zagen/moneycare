package ru.webmechanic.moneycare.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;

import java.util.List;

import ru.webmechanic.moneycare.content.Outlet;

/**
 * Created by a.bratusenko on 06.07.16.
 */
public class OutletsDetailsView extends DetailsView {
    private OutletsView.OnContentViewClickListener listener;

    public OutletsDetailsView(Context context) {
        super(context);
        init();
    }

    public OutletsDetailsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OutletsDetailsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        OutletsView outletsView = new OutletsView(getContext());
        outletsView.setVisibility(GONE);
        setView(outletsView);
    }

    public void setOutlets(List<Outlet> outlets) {
        ((OutletsView) view).setOutlets(outlets);
    }

    public void setListener(OutletsView.OnContentViewClickListener listener) {
        this.listener = listener;
        if (this.view != null) {
            ((OutletsView) view).setListener(listener);
        }
    }
}
