package ru.webmechanic.moneycare.database;

import java.util.List;

import io.realm.RealmResults;

/**
 * Created by a.bratusenko on 30.05.16.
 * Класс для представления данных из базы данных Realm в понятиях общего интерфейса
 */
public class RealmResult<T> extends Result<T> {

    private RealmResults list;

    public RealmResult(RealmResults list) {
        this.list = list;
    }

    @Override
    public T getItem(int position) {
        return (T) list.get(position);
    }

    @Override
    public void removeAt(int position) {

    }

    @Override
    public List<T> getList() {
        return list.subList(0, list.size());
    }

    @Override
    public T first() {
        if (size() > 0)
            return getItem(0);
        return null;
    }

    @Override
    public int size() {
        return list.size();
    }

}
