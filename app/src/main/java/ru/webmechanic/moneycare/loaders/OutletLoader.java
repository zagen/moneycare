package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;

import retrofit.Call;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.Outlet;
import ru.webmechanic.moneycare.ui.utils.Arguments;

/**
 * Created by a.bratusenko on 26.05.16.
 */
public class OutletLoader extends BaseLoader {
    private String id;

    public OutletLoader(Context context, Bundle args) {
        super(context);
        if (args != null && args.containsKey(Arguments.ARG_ID)) {
            this.id = args.getString(Arguments.ARG_ID);
        }
    }

    @Override
    protected Response apiCall() throws IOException {
        Outlet outlet = null;
        Response response = new Response();
        String[] params = Settings.getInstance(getContext()).prepareParams();

        Call<Outlet> call = service.outlet(id, params[0], params[1], params[2], params[3], params[4]);
        retrofit.Response<Outlet> retrofitResponse = call.execute();

        if (retrofitResponse != null) {
            if (!retrofitResponse.isSuccess()) {
                response.setCode(retrofitResponse.code());
                if (retrofitResponse.errorBody() != null) {
                    response.setErrorMessage(retrofitResponse.message());
                }
            } else {
                outlet = retrofitResponse.body();
            }
        }


        return response.setRequestResult(RequestResult.SUCCESS)
                .setCache(false)
                .setAnswer(outlet);
    }

}
