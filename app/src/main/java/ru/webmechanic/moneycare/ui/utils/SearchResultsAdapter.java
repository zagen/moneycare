package ru.webmechanic.moneycare.ui.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.content.LoanCoupon;
import ru.webmechanic.moneycare.content.SalesCoupon;
import ru.webmechanic.moneycare.database.Result;

/**
 * Created by a.bratusenko on 14.06.16.
 */
public class SearchResultsAdapter extends BaseAdapter {
    private static final int TYPE_SALE_COUPON = 0;
    private static final int TYPE_LOAN_COUPON = 1;
    private static final int TYPE_LABEL = 2;
    private OnSearchResultsClickListener listener;
    private Context context;
    private Result<SalesCoupon> salesCouponResult;
    private Result<LoanCoupon> loanCouponResult;
    private LayoutInflater inflater;
    private Settings settings;

    public SearchResultsAdapter(Context context, Result<SalesCoupon> salesCouponResult, Result<LoanCoupon> loanCouponResult) {
        this.context = context;
        this.settings = Settings.getInstance(context);
        this.loanCouponResult = loanCouponResult;
        this.salesCouponResult = salesCouponResult;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        int count = 0;
        if (salesCouponResult.size() > 0) {
            count = salesCouponResult.size() + 1;
        }
        if (loanCouponResult.size() > 0) {
            count += loanCouponResult.size() + 1;
        }
        return count;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {

        int salesCount = salesCouponResult.size();
        if (salesCount != 0) {
            salesCount++;
            if (position == 0) {
                return TYPE_LABEL;
            } else if (position < salesCount) {
                return TYPE_SALE_COUPON;
            }
        }
        if (loanCouponResult.size() != 0) {
            if (position == salesCount) {
                return TYPE_LABEL;
            } else {
                return TYPE_LOAN_COUPON;
            }
        }
        return TYPE_LABEL;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int type = getItemViewType(position);
        if (convertView == null) {
            if (type == TYPE_SALE_COUPON) {
                convertView = inflater.inflate(R.layout.list_item_sales_coupon_layout, null);
                SalesCouponsAdapter.ViewHolder viewHolder = new SalesCouponsAdapter.ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else if (type == TYPE_LOAN_COUPON) {
                convertView = inflater.inflate(R.layout.list_item_loan_coupon_layout, null);
                LoanCouponsAdapter.ViewHolder viewHolder = new LoanCouponsAdapter.ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                convertView = inflater.inflate(R.layout.list_item_label, null);
                ViewHolder viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            }
        }

        if (type == TYPE_SALE_COUPON) {
            final SalesCoupon coupon = salesCouponResult.getItem(position - 1);
            final String couponId = coupon.getId();
            SalesCouponsAdapter.ViewHolder viewHolder = (SalesCouponsAdapter.ViewHolder) convertView.getTag();
            viewHolder.salesCouponView.setSalesCoupon(coupon);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null && !Utils.isFixed(settings, coupon)) {
                        listener.onSalesCouponClick(couponId);
                    }
                }
            });

        } else if (type == TYPE_LOAN_COUPON) {
            int offset = 1;
            if (salesCouponResult.size() > 0) {
                offset += salesCouponResult.size() + 1;
            }

            final LoanCoupon coupon = loanCouponResult.getItem(position - offset);
            final String loanCouponId = coupon.getId();
            LoanCouponsAdapter.ViewHolder viewHolder = (LoanCouponsAdapter.ViewHolder) convertView.getTag();
            viewHolder.loanCouponItemView.setLoanCoupon(coupon);
            viewHolder.loanCouponItemView.setFlagVisible(true);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null && !Utils.isFixed(settings, coupon)) {
                        listener.onLoanCouponClick(loanCouponId);
                    }
                }
            });


        } else {
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            if (salesCouponResult.size() != 0 && position == 0) {
                viewHolder.textView.setText(context.getString(R.string.sales_coupons));
            } else {
                viewHolder.textView.setText(context.getString(R.string.loan_coupons));
            }
            convertView.setEnabled(false);
            convertView.setOnClickListener(null);

        }
        return convertView;
    }

    public void setListener(OnSearchResultsClickListener listener) {
        this.listener = listener;
    }

    public interface OnSearchResultsClickListener {
        void onSalesCouponClick(String id);

        void onLoanCouponClick(String id);
    }

    static class ViewHolder {
        @BindView(R.id.labelItemTextView)
        TextView textView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
