package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;

import retrofit.Call;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.User;
import ru.webmechanic.moneycare.ui.utils.Arguments;

/**
 * Created by a.bratusenko on 26.05.16.
 */
public class ResendPinLoader extends BaseLoader {
    private String phone;
    private String token;

    public ResendPinLoader(Context context, Bundle args) {
        super(context);
        if (args != null
                && args.containsKey(Arguments.ARG_PHONE)
                && args.containsKey(Arguments.ARG_TOKEN)) {
            this.phone = args.getString(Arguments.ARG_PHONE);
            this.token = args.getString(Arguments.ARG_TOKEN);
        }
    }

    @Override
    protected Response apiCall() throws IOException {
        Response response = new Response();
        User user = null;

        Call<User> call = service.resendPin(phone, token);
        retrofit.Response<User> retrofitResponse = call.execute();

        if (retrofitResponse != null) {
            if (!retrofitResponse.isSuccess()) {
                response.setCode(retrofitResponse.code());
                if (retrofitResponse.errorBody() != null) {
                    response.setErrorMessage(retrofitResponse.message());
                }
            } else {
                user = retrofitResponse.body();
            }
        }

        return response.setCache(false)
                .setRequestResult(RequestResult.SUCCESS)
                .setCache(false)
                .setAnswer(user);
    }
}
