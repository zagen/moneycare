package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;

import retrofit.Call;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.PaymentSystem;
import ru.webmechanic.moneycare.ui.utils.Arguments;

/**
 * Created by a.bratusenko on 27.05.16.
 */
public class PaymentSystemLoader extends BaseLoader {
    private String id;

    public PaymentSystemLoader(Context context, Bundle args) {
        super(context);
        if (args != null && args.containsKey(Arguments.ARG_ID)) {
            this.id = args.getString(Arguments.ARG_ID);
        }
    }

    @Override
    protected Response apiCall() throws IOException {
        Response response = new Response();
        PaymentSystem paymentSystem = null;
        String[] params = Settings.getInstance(getContext()).prepareParams();

        Call<PaymentSystem> call = service.paymentSystem(id, params[0], params[1], params[2], params[3], params[4]);
        retrofit.Response<PaymentSystem> retrofitResponse = call.execute();

        if (retrofitResponse != null) {
            if (!retrofitResponse.isSuccess()) {
                response.setCode(retrofitResponse.code());
                if (retrofitResponse.errorBody() != null) {
                    response.setErrorMessage(retrofitResponse.message());
                }
            } else {
                paymentSystem = retrofitResponse.body();
            }
        }

        return response.setRequestResult(RequestResult.SUCCESS)
                .setCache(false)
                .setAnswer(paymentSystem);

    }
}
