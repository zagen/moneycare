package ru.webmechanic.moneycare.ui.customviews;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TextView;

import ru.webmechanic.moneycare.R;


/**
 * Created by a.bratusenko on 06.07.16.
 * Класс контрола для отображения информации содержащий один текстовый элемент
 */
public class SimpleTextDetailsView extends DetailsView {
    public SimpleTextDetailsView(Context context) {
        super(context);
        init();
    }

    public SimpleTextDetailsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SimpleTextDetailsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        TextView desc = new TextView(getContext());
        desc.setBackgroundColor(Color.WHITE);
        desc.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
        int padding = (int) getResources().getDimension(R.dimen.content_space_left);
        desc.setPadding(padding, padding, padding, padding);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            desc.setTextAppearance(android.R.style.TextAppearance_Small);
        } else {
            desc.setTextAppearance(getContext(), android.R.style.TextAppearance_Small);

        }
        desc.setVisibility(GONE);
        desc.setTextColor(Color.BLACK);
        setView(desc);
        this.view.setOnClickListener(clickListener);
    }

    public void setText(String details) {
        ((TextView) view).setText(details);
    }
}
