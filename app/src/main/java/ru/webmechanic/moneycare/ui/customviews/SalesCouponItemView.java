package ru.webmechanic.moneycare.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.content.SalesCoupon;
import ru.webmechanic.moneycare.ui.utils.Utils;
import ru.webmechanic.moneycare.ui.utils.imageloader.ImageLoader;

/**
 * Created by a.bratusenko on 22.06.16.
 */
public class SalesCouponItemView extends RelativeLayout {
    @BindView(R.id.stickerCouponButtonItem)
    ImageButton stickerImageButton;

    @BindView(R.id.firmCouponTextViewItem)
    TextView subjectTextView;

    @BindView(R.id.imageCouponImageViewItem)
    ImageView imageView;

    @BindView(R.id.titleCouponTextViewItem)
    TextView titleView;

    @BindView(R.id.validDateCouponTextViewItem)
    TextView validDateText;

    @BindView(R.id.stickerFlagCouponTextViewItem)
    TextView valueTextView;

    @BindView(R.id.fixedTextView)
    TextView fixedTextView;

    @BindView(R.id.flagViewCouponContainer)
    RelativeLayout flagViewContainer;

    @BindView(R.id.contentCouponContainer)
    LinearLayout contentContainer;

    private ImageLoader imageLoader;
    private Settings settings;

    public SalesCouponItemView(Context context) {
        super(context);
        init();
    }

    public SalesCouponItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SalesCouponItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        imageLoader = new ImageLoader(getContext());
        settings = Settings.getInstance(getContext());
        inflate(getContext(), R.layout.customviews_salescoupon_item, this);
        setDescendantFocusability(FOCUS_BLOCK_DESCENDANTS);
        ButterKnife.bind(this);
    }

    public ImageButton getSticker() {
        return stickerImageButton;
    }

    public void setSalesCoupon(SalesCoupon coupon) {
        titleView.setText(coupon.getSubjectType());
        subjectTextView.setText(coupon.getSubject());
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        validDateText.setText(getContext().getString(R.string.until_only) + " " + sdf.format(coupon.getEndDate()));

        valueTextView.setText(coupon.getValue());

        imageLoader.DisplayImage(coupon.getImagePath(), imageView);

        boolean isFixed = Utils.isFixed(settings, coupon);

        float opacity = isFixed ? .6f : 1f;
        int fixedTextViewVisibility = isFixed ? VISIBLE : GONE;
        fixedTextView.setVisibility(fixedTextViewVisibility);
        flagViewContainer.setAlpha(opacity);
        contentContainer.setAlpha(opacity);
        stickerImageButton.setAlpha(opacity);
        stickerImageButton.setEnabled(!isFixed);
    }
}
