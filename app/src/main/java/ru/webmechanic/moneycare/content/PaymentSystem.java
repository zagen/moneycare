package ru.webmechanic.moneycare.content;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by a.bratusenko on 25.05.16.
 * Содержит информацию о платежной системе.
 */
public class PaymentSystem extends RealmObject {
    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("created_at")
    private Date creatingDate;

    @SerializedName("updated_at")
    private Date updatingDate;

    @SerializedName("logo")
    private String logo;

    @SerializedName("percent")
    private String percent;

    @SerializedName("terminals")
    private RealmList<Terminal> terminals;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreatingDate() {
        return creatingDate;
    }

    public void setCreatingDate(Date creatingDate) {
        this.creatingDate = creatingDate;
    }

    public Date getUpdatingDate() {
        return updatingDate;
    }

    public void setUpdatingDate(Date updatingDate) {
        this.updatingDate = updatingDate;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public RealmList<Terminal> getTerminals() {
        return terminals;
    }

    public void setTerminals(RealmList<Terminal> terminals) {
        this.terminals = terminals;
    }
}
