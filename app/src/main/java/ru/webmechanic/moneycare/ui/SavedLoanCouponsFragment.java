package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.LoanCoupon;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.ui.utils.Arguments;
import ru.webmechanic.moneycare.ui.utils.SavedLoanCouponAdapter;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class SavedLoanCouponsFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener {
    @BindView(R.id.savedLoanCouponsListView)
    ListView savedCouponsListView;

    @BindView(R.id.noDataTextView)
    TextView noDataTextView;

    private Result<LoanCoupon> loanCouponResult;

    public SavedLoanCouponsFragment() {
    }

    @Override
    protected void fetchData() {
        isLoading = true;
        new DataFetcher(this, loaderContext, this)
                .inCollection("id", settings.getStorage().getSavedLoanCoupons(), false)
                .fetch();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_saved_loan_coupons, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.saved_loan_coupons);
    }


    @OnItemClick(R.id.savedLoanCouponsListView)
    void couponSelected(AdapterView<?> parent, int position) {
        String couponId = ((LoanCoupon) parent.getAdapter().getItem(position)).getId();
        Arguments args = new Arguments();
        args.setId(couponId);
        gotoFragment(FragmentFactory.LOAN_COUPON, args);
    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        if (loaderid == R.id.loan_coupons_loader) {
            try {
                loanCouponResult = (Result<LoanCoupon>) result;
            } catch (Exception e) {
            }
        }
        isLoading = false;
        updateUI();
    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        if (isLoading)
            return;
        if (loanCouponResult != null && loanCouponResult.size() != 0) {
            noDataTextView.setVisibility(View.GONE);
            savedCouponsListView.setAdapter(new SavedLoanCouponAdapter(getActivity(), loanCouponResult, new SavedLoanCouponAdapter.OnItemDeletedListener() {
                @Override
                public void onItemDeleted() {
                    fetchData();
                    updateUI();
                }
            }));
        } else {
            savedCouponsListView.setAdapter(null);
            noDataTextView.setVisibility(View.VISIBLE);
        }

    }
}
