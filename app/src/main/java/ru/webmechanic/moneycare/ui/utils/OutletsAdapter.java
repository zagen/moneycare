package ru.webmechanic.moneycare.ui.utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Outlet;
import ru.webmechanic.moneycare.database.Result;

/**
 * Created by a.bratusenko on 30.05.16.
 */
public class OutletsAdapter extends BaseAdapter {
    private Result<Outlet> result;
    private LayoutInflater inflater;

    public OutletsAdapter(Context context, Result<Outlet> result) {
        this.result = result;
        this.inflater = ((Activity) context).getLayoutInflater();
    }

    @Override
    public int getCount() {
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        return result.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_outlets_layout, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Outlet outlet = result.getItem(position);
        if (outlet != null) {

            viewHolder.titleTextView.setText(outlet.getOrganization());
            viewHolder.descTextView.setText(outlet.getTitle());
        }
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.outletTitleTextViewItem)
        TextView titleTextView;
        @BindView(R.id.outletDescTextViewItem)
        TextView descTextView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
