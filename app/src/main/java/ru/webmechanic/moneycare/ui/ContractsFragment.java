package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.ui.utils.ContractsAdapter;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class ContractsFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener {


    @BindView(R.id.contractsListView)
    ListView listView;
    @BindView(R.id.noDataTextView)
    TextView noDataTextView;

    private Result<Contract> contractResult;

    public ContractsFragment() {
        // Required empty public constructor
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.your_contracts);
    }

    @OnClick(R.id.contractsGoToArchiveButton)
    void gotoPaidContracts() {
        gotoFragment(FragmentFactory.CONTRACTS_PAYED);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contracts, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void fetchData() {
        isLoading = true;
        new DataFetcher(this, loaderContext, this).inCollection("id", settings.getStorage().getPayedContracts(), true).fetch();

    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {

        if (loaderid == R.id.contracts_loader) {
            try {
                contractResult = (Result<Contract>) result;
            } catch (Exception e) {

            }

        }
        isLoading = false;
        updateUI();

    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        if (contractResult != null && contractResult.size() != 0) {
            noDataTextView.setVisibility(View.GONE);
            try {
                listView.setAdapter(new ContractsAdapter(this, contractResult));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            noDataTextView.setVisibility(View.VISIBLE);
        }
    }
}
