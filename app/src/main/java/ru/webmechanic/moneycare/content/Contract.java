package ru.webmechanic.moneycare.content;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by a.bratusenko on 25.05.16.
 * Класс данных, содержащих информацию о договоре
 */
public class Contract extends RealmObject {

    @SerializedName("amount")
    private double amount;

    @SerializedName("begin_date")
    private Date creatingDate;

    @SerializedName("end_date")
    private Date endDate;

    @SerializedName("contract_num")
    private String id;

    @SerializedName("goods")
    private RealmList<Goods> goods;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getCreatingDate() {
        return creatingDate;
    }

    public void setCreatingDate(Date creatingDate) {
        this.creatingDate = creatingDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RealmList<Goods> getGoods() {
        return goods;
    }

    public void setGoods(RealmList<Goods> goods) {
        this.goods = goods;
    }
}
