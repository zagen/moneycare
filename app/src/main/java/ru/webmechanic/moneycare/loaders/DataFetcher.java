package ru.webmechanic.moneycare.loaders;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.realm.RealmObject;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.database.ListResult;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.database.realm.DBHelper;
import ru.webmechanic.moneycare.ui.utils.Arguments;

/**
 * Created by a.bratusenko on 16.06.16.
 * Класс представляющий интерфейс для получения данных
 */
public class DataFetcher implements LoaderManager.LoaderCallbacks<Response> {

    private LoaderManager loaderManager;
    private Settings settings;
    private OnDataFetchedListener listener;
    private Filter filter = null;
    private ActionGetFromDb actionDb = null;
    private LoaderContext loaderContext;

    public DataFetcher(Fragment fragment, LoaderContext loaderContext, OnDataFetchedListener listener) {
        this.loaderManager = fragment.getLoaderManager();
        this.settings = Settings.getInstance(fragment.getActivity());
        init(loaderContext, listener);
    }

    public DataFetcher(Activity activity, LoaderContext loaderContext, OnDataFetchedListener listener) {
        this.loaderManager = activity.getLoaderManager();
        this.settings = Settings.getInstance(activity);
        init(loaderContext, listener);
    }

    public DataFetcher(Fragment fragment, final Class<? extends RealmObject> dataClass) {
        this.loaderManager = fragment.getLoaderManager();
        this.settings = Settings.getInstance(fragment.getActivity());
        this.loaderContext = new LoaderContext(fragment.getActivity(), new Arguments()) {
            @Override
            public Class<? extends RealmObject> getDataClass() {
                return dataClass;
            }
        };
    }

    /**
     * Устанавливает фильтр для поиска объекта данных по id
     * Устанавливает команду выборки одного объекта из бд
     *
     * @param id - значение ключа поиска
     * @return this
     */
    public DataFetcher one(final String id) {
        if (id != null) {
            this.filter = new PropertyEqualFilter("getId", id);
            this.actionDb =
                    new ActionGetFromDb() {
                        @Override
                        public <T extends RealmObject> Result<T> getResultFromDb(Class<T> tClass) {
                            return DBHelper.getById(settings.getContext(), id, tClass);
                        }
                    };
        }
        return this;
    }

    /**
     * Устанавливает фильтр объектов с полем field(String) равным value
     * Устанавливает команду получения из бд объектов с полем field равным value
     *
     * @param field имя поля, по значениям которых осуществляется поиск
     * @param value значение поля, которому ищется соответствие
     * @return this
     */
    public DataFetcher equal(final String field, final String value) {
        if (field != null && value != null) {
            final String getter = "get" + field.substring(0, 1).toUpperCase() + field.substring(1);
            this.filter = new PropertyEqualFilter(getter, value);
            this.actionDb = new ActionGetFromDb() {
                @Override
                public <T extends RealmObject> Result<T> getResultFromDb(Class<T> tClass) {
                    return DBHelper.getEqual(field, value, tClass);
                }
            };
        }
        return this;
    }

    /**
     * Устанавливает фильтр для поиска объектов содержащихся в переданной коллекции или не содержащихся в ней
     * Устанавливает команду получения из бд объектов, значение поля field которых (не) содержится в коллекции
     *
     * @param field   имя поля, по значениям которых осуществляется поиск
     * @param values  значения полей, которому ищется соответствие
     * @param inverse если установлен в true осуществляется поиск объектов, значения полей которых не содержатся в переданной коллекции
     * @return this
     */
    public DataFetcher inCollection(final String field, final Collection<String> values, final boolean inverse) {
        if (field != null && values != null) {
            String getter = "get" + field.substring(0, 1).toUpperCase() + field.substring(1);
            this.filter = new PropertyInCollection(getter, values, inverse);
            this.actionDb = new ActionGetFromDb() {
                @Override
                public <T extends RealmObject> Result<T> getResultFromDb(Class<T> tClass) {
                    if (inverse) {
                        return DBHelper.getAllByNotEqualField(field, new ArrayList<>(values), tClass);
                    } else {
                        return DBHelper.getAllByField(field, new ArrayList<>(values), tClass);
                    }
                }
            };
        }
        return this;
    }

    /**
     * Устанавливает команду и фильтр для получения объектов, значение поля field которых содержит value
     *
     * @param field имя поля фильтруемых объектов
     * @param value значение которое ищется в поле field объектов
     * @return this
     */
    public DataFetcher containes(String field, String value) {
        if (field != null && value != null) {
            field = "get" + field.substring(0, 1).toUpperCase() + field.substring(1);
            this.filter = new PropertyContainsValue(field, value);
            this.actionDb = new ActionGetFromDb() {
                @Override
                public <T extends RealmObject> Result<T> getResultFromDb(Class<T> tClass) {
                    return filter.filter(DBHelper.getAll(tClass));
                }
            };
        }
        return this;
    }

    /**
     * Метод для получения данных из кэша в соответствии с установленными фильтрами и командами.
     * Если команда получения данных из БД не установлена, извлекаются все данные заданного класса
     *
     * @return Result содержащий объекты заданного класса
     */
    public Result fetchFromCache() {
        Result result = null;
        if (actionDb == null) {
            actionDb = new ActionGetFromDb() {
                @Override
                public <T extends RealmObject> Result<T> getResultFromDb(Class<T> tClass) {
                    return DBHelper.getAll(tClass);
                }
            };
        }

        result = actionDb.getResultFromDb(loaderContext.getDataClass());
        if (filter != null) {
            return filter.filter(result);
        } else {
            return result;
        }

    }

    /**
     * Метод запуска получения данных.
     * Если кэш валидный и/или отсутствует соединение с интернетом, данные получаются из кэша
     * Иначе пытается получить данные из сети.
     */
    public void fetch() {

        try {
            if (actionDb == null) {
                actionDb = new ActionGetFromDb() {
                    @Override
                    public <T extends RealmObject> Result<T> getResultFromDb(Class<T> tClass) {
                        return DBHelper.getAll(tClass);
                    }
                };
            }
            if (!settings.isInternetAvailable()) {
                if (listener != null) {
                    listener.onDataFetched(loaderContext.getLoader(), fetchFromCache());
                }
            } else {
                if (settings.getStorage().isCacheValid(loaderContext.getDataClass().getSimpleName())) {
                    if (listener != null) {
                        listener.onDataFetched(loaderContext.getLoader(), fetchFromCache());
                    }

                } else {
                    if (listener != null) {
                        fetchFromWeb();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchFromWeb() {
        loaderManager.initLoader(loaderContext.getLoader(), loaderContext.getArguments().getBundle(), this);
    }

    private void init(LoaderContext loaderContext, OnDataFetchedListener listener) {
        this.loaderContext = loaderContext;
        this.listener = listener;
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        return loaderContext.createLoader();
    }

    /**
     * Вызывается в случае получения данных из сети
     *
     * @param loader загрузчик данных
     * @param data   ответ с данными, либо с ошибкой
     */
    @Override
    public void onLoadFinished(final Loader<Response> loader, final Response data) {
        int id = loader.getId();
        if (id == loaderContext.getLoader()) {
            //проверяет статус ответа HTTP, в случае необходимости вызывает функцию обратного вызова
            if (data.getRequestResult() == RequestResult.ERROR || data.getCode() != 200) {
                if (listener != null)
                    listener.onDataFetchedError(data.getCode(), data.getErrorMessage());
            } else {
                Object result = data.getTypedAnswer();
                List<Object> resultList;
                if (result instanceof List<?>) {
                    try {
                        resultList = (List<Object>) result;
                    } catch (Exception e) {
                        resultList = new ArrayList<>();
                    }
                } else {
                    resultList = new ArrayList<>();
                    if (result != null) {
                        resultList.add(result);
                    }
                }
                final List<Object> finalResultList = resultList;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //кэшируем если необходимо
                        if (data.needCache()) {
                            data.save(settings.getContext(), loaderContext.getDataClass());
                            settings.getStorage().setCacheUpdateTime(loaderContext.getDataClass().getSimpleName());
                        }
                        final Result<Object> filtered;

                        //если установлен фильтр - фильтруем
                        if (filter != null) {
                            filtered = filter.filter(new ListResult<>(finalResultList));

                        } else {
                            filtered = new ListResult<>(finalResultList);
                        }
                        //Передаем данные слушателю

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (listener != null) {
                                    listener.onDataFetched(loaderContext.getLoader(), filtered);
                                }
                            }
                        });
                    }
                }).run();

            }
        }
        loaderManager.destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }


    /**
     * Интерфейс листенера в случае получения данных
     */
    public interface OnDataFetchedListener {
        <T> void onDataFetched(int loaderid, Result<T> result);

        void onDataFetchedError(int code, String message);
    }

    /**
     * Интерфейс команды получения данных из бд
     */
    public interface ActionGetFromDb {
        <T extends RealmObject> Result<T> getResultFromDb(Class<T> tClass);
    }

    /**
     * Интерфейс фильтра.
     */
    public interface Filter {
        <T> Result<T> filter(Result<T> objects);
    }

    /**
     * Осуществляет фильтр результата где поле равно переданному значению
     */
    private class PropertyEqualFilter implements Filter {
        private String getter;
        private String value;

        public PropertyEqualFilter(String getter, String value) {
            this.getter = getter;
            this.value = value;
        }

        @Override
        public <T> Result<T> filter(Result<T> objects) {
            List<T> filtered = new ArrayList<>();
            for (int i = 0; i < objects.size(); i++)
                try {
                    T object = objects.getItem(i);
                    Class tclass = object.getClass();

                    if (tclass.getName().endsWith("Proxy")) {
                        tclass = tclass.getSuperclass();
                    }

                    Method method = tclass.getDeclaredMethod(getter);

                    String currentValue = (String) method.invoke(object, new Class[]{});
                    if (currentValue.equals(value)) {
                        filtered.add(object);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            return new ListResult<>(filtered);
        }
    }

    /**
     * Осуществляет фильтрацию по полю, содержащему переданное значение
     */
    private class PropertyContainsValue implements Filter {
        private String getter;
        private String value;

        public PropertyContainsValue(String getter, String value) {
            this.getter = getter;
            this.value = value;
        }

        @Override
        public <T> Result<T> filter(Result<T> objects) {
            List<T> filtered = new ArrayList<>();
            for (int i = 0; i < objects.size(); i++) {
                try {
                    T object = objects.getItem(i);
                    Class tclass = object.getClass();

                    if (tclass.getName().endsWith("Proxy")) {
                        tclass = tclass.getSuperclass();
                    }
                    Method method = tclass.getDeclaredMethod(getter);

                    String currentValue = (String) method.invoke(object, new Class[]{});
                    if (currentValue.toLowerCase().contains(value.toLowerCase())) {
                        filtered.add(object);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
            return new ListResult<>(filtered);
        }
    }

    /**
     * Фильтрация элементов, значение поля которых (не) содержится в переданной коллекции
     */
    private class PropertyInCollection implements Filter {
        private Collection<String> values;
        private String getter;
        private boolean inverse;

        public PropertyInCollection(String getter, Collection<String> values, boolean inverse) {
            this.getter = getter;
            this.values = values;
            this.inverse = inverse;
        }

        @Override
        public <T> Result<T> filter(Result<T> objects) {
            List<T> filtered = new ArrayList<>();
            for (int i = 0; i < objects.size(); i++)
                try {
                    T object = objects.getItem(i);
                    Class tclass = object.getClass();

                    if (tclass.getName().endsWith("Proxy")) {
                        tclass = tclass.getSuperclass();
                    }
                    Method method = tclass.getDeclaredMethod(getter);

                    String currentValue = (String) method.invoke(object, new Class[]{});
                    if (!inverse && values.contains(currentValue)) {
                        filtered.add(object);
                    } else if (inverse && !values.contains(currentValue)) {
                        filtered.add(object);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            return new ListResult<>(filtered);
        }
    }
}
