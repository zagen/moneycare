package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.content.LocalData.SalesCouponFixData;
import ru.webmechanic.moneycare.content.SalesCoupon;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.ui.customviews.OutletsDetailsView;
import ru.webmechanic.moneycare.ui.customviews.OutletsView;
import ru.webmechanic.moneycare.ui.customviews.SimpleTextDetailsView;
import ru.webmechanic.moneycare.ui.utils.Arguments;
import ru.webmechanic.moneycare.ui.utils.Utils;
import ru.webmechanic.moneycare.ui.utils.imageloader.ImageLoader;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class SalesCouponFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener, OutletsView.OnContentViewClickListener {

    @BindView(R.id.saveSalesCouponButton)
    Button saveMarkedButton;

    @BindView(R.id.deleteSalesCouponButton)
    Button deleteSavedButton;

    @BindView(R.id.repaySalesCouponButton)
    Button repayButton;

    @BindView(R.id.imageCouponImageViewItem)
    ImageView imageView;

    @BindView(R.id.titleCouponTextViewItem)
    TextView titleTextView;

    @BindView(R.id.validDateCouponTextViewItem)
    TextView validTextView;

    @BindView(R.id.valueCouponTextView)
    TextView valueTextView;

    @BindView(R.id.conditionsSalesCouponTextView)
    TextView conditionTextView;

    @BindView(R.id.codeSalesCouponEditText)
    EditText codeEditText;

    @BindView(R.id.descriptionDetailsView)
    SimpleTextDetailsView descriptionView;

    @BindView(R.id.outletsDetailsView)
    OutletsDetailsView outletsView;

    private SalesCoupon coupon;
    private ImageLoader imageLoader;

    public SalesCouponFragment() {
        // Required empty public constructor
    }

    @Override
    protected void fetchData() {
        isLoading = true;
        new DataFetcher(this, loaderContext, this).one(arguments.getId()).fetch();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sales_coupon, container, false);
        ButterKnife.bind(this, view);
        outletsView.setListener(this);
        imageLoader = new ImageLoader(getActivity());
        return view;
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.coupon);
    }


    @OnClick(R.id.saveSalesCouponButton)
    void save() {
        if (arguments.isSetId()) {
            settings.getStorage().saveSalesCoupon(arguments.getId(), true);
        }
        updateUI();

    }

    @OnClick(R.id.deleteSalesCouponButton)
    void delete() {
        if (arguments.isSetId()) {
            settings.getStorage().saveSalesCoupon(arguments.getId(), false);
            updateUI();
        }
    }

    @OnClick(R.id.repaySalesCouponButton)
    void repay() {
        String idexternal = codeEditText.getText().toString().trim();

        if (idexternal.length() > 0) {
            loaderContext.setLoader(R.id.fix_sales_coupon_loader);
            arguments.setIdexternal(idexternal);
            isLoading = true;
            new DataFetcher(this, loaderContext, this).fetchFromWeb();
            updateUI();
        }
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        boolean inputEnabled = loaderContext.getLoader() != R.id.fix_sales_coupon_loader;
        if (!settings.getUserState().equals(Settings.USER_STATE_LOGGED)) {
            inputEnabled = false;
            conditionTextView.setText(getString(R.string.input_coupon_label_user_unauth));
        } else if (!settings.isInternetAvailable()) {
            inputEnabled = false;
            conditionTextView.setText(getString(R.string.input_coupon_label_no_internet));
        } else {

        }
        if (coupon != null) {
            titleTextView.setText(coupon.getSubject());
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            validTextView.setText(getString(R.string.until_only) + " " + sdf.format(coupon.getEndDate()));
            valueTextView.setText(coupon.getValue());

            imageLoader.DisplayImage(coupon.getImagePath(), imageView);

            if (coupon.getDescription() != null && coupon.getDescription().length() > 0) {
                descriptionView.setVisibility(View.VISIBLE);
                descriptionView.setText(
                        String.format(getString(R.string.sales_coupon_description),
                                coupon.getSubjectType(),
                                coupon.getSubject(),
                                coupon.getDescription()));
            }
            if (settings.getStorage().isSalesCouponSaved(arguments.getId())) {
                saveMarkedButton.setEnabled(false);
                deleteSavedButton.setEnabled(true);
            } else {
                saveMarkedButton.setEnabled(true);
                deleteSavedButton.setEnabled(false);
            }
            inputEnabled = inputEnabled && !Utils.isFixed(settings, coupon);
            if (coupon.getOutlets().size() > 0) {

                outletsView.setVisibility(View.VISIBLE);
                outletsView.setOutlets(coupon.getOutlets());
            } else {
                outletsView.setVisibility(View.GONE);
            }
        }
        codeEditText.setEnabled(inputEnabled);
        repayButton.setEnabled(inputEnabled);
    }

    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {

        if (loaderContext.getLoader() == R.id.fix_sales_coupon_loader) {
            loaderContext.setLoader(R.id.sales_coupons_loader);
            SalesCouponFixData fixLocalData = (SalesCouponFixData) result.first();
            if (fixLocalData != null) {
                gotoBack();
                gotoFragment(FragmentFactory.THANKS);

            } else {
                AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
                adb.setMessage(R.string.wrong_code)
                        .setPositiveButton("OK", null)
                        .show();
            }
        } else {
            if (result.size() > 0) {
                try {
                    this.coupon = (SalesCoupon) result.first();
                } catch (Exception e) {
                }
            }
        }
        isLoading = false;
        updateUI();
    }


    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onContentViewClicked(String id) {
        Arguments arguments = new Arguments();
        arguments.setId(id);
        gotoFragment(FragmentFactory.OUTLET, arguments);
    }
}
