package ru.webmechanic.moneycare.ui.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.SalesCoupon;
import ru.webmechanic.moneycare.database.Result;

/**
 * Created by a.bratusenko on 03.06.16.
 */
public class SavedSalesCouponAdapter extends SalesCouponsAdapter {
    private OnItemDeletedListener deletedListener;

    public SavedSalesCouponAdapter(Context context, Result<SalesCoupon> result, OnItemDeletedListener listener) {
        super(context, result);
        this.deletedListener = listener;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public Object getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = super.getView(position, convertView, parent);
        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        final SalesCoupon salesCoupon = (SalesCoupon) super.getItem(position);

        viewHolder.salesCouponView.getSticker().setImageResource(R.drawable.sticker_remove);

        viewHolder.salesCouponView.getSticker().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String couponId = salesCoupon.getId();
                settings.getStorage().saveSalesCoupon(couponId, !settings.getStorage().isSalesCouponSaved(couponId));
                if (deletedListener != null) {
                    deletedListener.onItemDeleted();
                }
            }
        });

        return convertView;
    }

    public interface OnItemDeletedListener {
        void onItemDeleted();
    }

}
