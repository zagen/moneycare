package ru.webmechanic.moneycare.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.content.LoanCoupon;
import ru.webmechanic.moneycare.ui.utils.Utils;
import ru.webmechanic.moneycare.ui.utils.imageloader.ImageLoader;

/**
 * Created by a.bratusenko on 22.06.16.
 */
public class LoanCouponItemView extends RelativeLayout {
    @BindView(R.id.imageLoanCouponImageViewItem)
    ImageView imageView;

    @BindView(R.id.titleLoanCouponTextViewItem)
    TextView titleView;

    @BindView(R.id.firmLoanCouponTextViewItem)
    TextView subjectTextView;

    @BindView(R.id.validDateLoanCouponTextViewItem)
    TextView validDateText;

    @BindView(R.id.stickerLoanCouponButtonItem)
    ImageButton stickerImageButton;

    @BindView(R.id.stickerFlagLoanCouponRelativeLayout)
    RelativeLayout flagViewContainer;

    @BindView(R.id.contentLoanCouponContainer)
    LinearLayout contentContainer;

    @BindView(R.id.fixedTextView)
    TextView fixedTextView;

    private Settings settings;
    private ImageLoader imageLoader;

    public LoanCouponItemView(Context context) {
        super(context);
        init();
    }

    public LoanCouponItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LoanCouponItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.customviews_loancoupon_item, this);
        ButterKnife.bind(this);
        this.imageLoader = new ImageLoader(getContext());
        this.settings = Settings.getInstance(getContext());
    }

    public ImageButton getSticker() {
        return stickerImageButton;
    }

    public void setFlagVisible(boolean visible) {
        flagViewContainer.setVisibility(visible ? VISIBLE : GONE);
    }

    public void setLoanCoupon(LoanCoupon coupon) {
        titleView.setText(coupon.getSubjectType());
        subjectTextView.setText(coupon.getSubject());
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        validDateText.setText(getContext().getString(R.string.until_only) + " " + sdf.format(coupon.getEndDate()));

        imageLoader.DisplayImage(coupon.getImagePath(), imageView);
        boolean isFixed = Utils.isFixed(settings, coupon);
        float opacity = isFixed ? .6f : 1f;
        int fixedTextViewVisibility = isFixed ? VISIBLE : GONE;
        fixedTextView.setVisibility(fixedTextViewVisibility);
        flagViewContainer.setAlpha(opacity);
        contentContainer.setAlpha(opacity);
        stickerImageButton.setAlpha(opacity);
        stickerImageButton.setEnabled(!isFixed);

    }
}
