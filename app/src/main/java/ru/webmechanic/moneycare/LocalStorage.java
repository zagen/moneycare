package ru.webmechanic.moneycare;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import ru.webmechanic.moneycare.content.ATM;
import ru.webmechanic.moneycare.content.Department;
import ru.webmechanic.moneycare.content.NearestDepartment;
import ru.webmechanic.moneycare.content.NearestTerminal;
import ru.webmechanic.moneycare.content.Terminal;
import ru.webmechanic.moneycare.database.Result;

/**
 * Created by a.bratusenko on 17.06.16.
 * Локальное хранилище данных
 * Хранит информацию о помеченных купонах/акциях, сохраненных купонах/акциях, погашенных купонах/акциях,
 * оплаченных контрактах, и дистанциях до различных объектов, определенных в последний раз
 */
public class LocalStorage {

    private final static String CACHE_DATES_MAP = "map";
    private final static String SALES_COUPONS_SAVED = "scs";
    private final static String LOAN_COUPONS_SAVED = "lcs";
    private final static String SALES_COUPONS_MARKED = "scm";
    private final static String LOAN_COUPONS_MARKED = "lcm";
    private final static String PAYED_CONTRACTS = "pcs";
    private final static String SALES_COUPONS_FIX = "scf";
    private final static String LOAN_COUPONS_FIX = "lcf";
    private final static String NEAREST_DISTANCES = "ndm";
    private final static long CACHE_LIVE_TIME = 3600000;//ms

    private WeakReference<Context> context;
    private HashMap<String, Date> cacheDates;

    private Set<String> markedSalesCoupons;
    private Set<String> savedSalesCoupons;
    private Set<String> markedLoanCoupons;
    private Set<String> savedLoanCoupons;
    private Set<String> payedContracts;
    private Set<String> fixLoanCoupons;
    private Set<String> fixSalesCoupons;

    private HashMap<String, HashMap<String, Integer>> nearestDistances;

    public LocalStorage(WeakReference<Context> context) {
        this.context = context;
        this.cacheDates = loadFromFile(CACHE_DATES_MAP);
        if (this.cacheDates == null) {
            this.cacheDates = new HashMap<>();
        }
        markedSalesCoupons = loadFromFile(SALES_COUPONS_MARKED);
        markedLoanCoupons = loadFromFile(LOAN_COUPONS_MARKED);
        savedLoanCoupons = loadFromFile(LOAN_COUPONS_SAVED);
        savedSalesCoupons = loadFromFile(SALES_COUPONS_SAVED);
        fixLoanCoupons = loadFromFile(LOAN_COUPONS_FIX);
        fixSalesCoupons = loadFromFile(SALES_COUPONS_FIX);

        payedContracts = loadFromFile(PAYED_CONTRACTS);
        nearestDistances = loadFromFile(NEAREST_DISTANCES);

        //пытается загрузить объекты из файловой системы, в противном случае создаем пустые коллекции
        markedLoanCoupons = (markedLoanCoupons == null) ? new HashSet<String>() : markedLoanCoupons;
        savedSalesCoupons = (savedSalesCoupons == null) ? new HashSet<String>() : savedSalesCoupons;
        markedSalesCoupons = (markedSalesCoupons == null) ? new HashSet<String>() : markedSalesCoupons;
        savedLoanCoupons = (savedLoanCoupons == null) ? new HashSet<String>() : savedLoanCoupons;
        payedContracts = (payedContracts == null) ? new HashSet<String>() : payedContracts;
        fixSalesCoupons = (fixSalesCoupons == null) ? new HashSet<String>() : fixSalesCoupons;
        fixLoanCoupons = (fixLoanCoupons == null) ? new HashSet<String>() : fixLoanCoupons;
        nearestDistances = (nearestDistances == null) ? new HashMap<String, HashMap<String, Integer>>() : nearestDistances;
    }

    public Collection<String> getSavedSalesCoupons() {
        return savedSalesCoupons;
    }

    public Collection<String> getSavedLoanCoupons() {
        return savedLoanCoupons;
    }

    public Collection<String> getPayedContracts() {
        return payedContracts;
    }

    /**
     * загружает объект из файла
     *
     * @param name имя файла объекта
     * @param <T>  Класс объекта
     * @return объект если удалось прочитать его из файловой системы, и null в противном случае
     */
    private <T> T loadFromFile(String name) {

        T cacheDates = null;
        try {
            File file = new File(context.get().getDir("data", Context.MODE_PRIVATE), name);
            ObjectInputStream objectInputStream =
                    new ObjectInputStream(new FileInputStream(file));

            // read and print an object and cast it as string
            cacheDates = (T) objectInputStream.readObject();

        } catch (Exception e) {
            e.printStackTrace();

        }
        return cacheDates;
    }

    /**
     * Сохраняет объект в файл
     *
     * @param savedObject сохраняемый объект
     * @param dataName    имя файла для сохраняемого объекта
     * @param <T>         Класс объеката
     */
    private <T> void saveToFile(T savedObject, String dataName) {
        try {
            File file = new File(context.get().getDir("data", Context.MODE_PRIVATE), dataName);
            new FileOutputStream(file).close();
            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file));
            outputStream.writeObject(savedObject);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Проверка валидности кэша для класса объектов с переданным именем
     *
     * @param className имя класса
     * @return истина если кэш актуален
     */
    public boolean isCacheValid(String className) {
        boolean valid = false;
        if (cacheDates.containsKey(className)) {
            Date lastUpdatingDate = cacheDates.get(className);
            valid = (new Date().getTime() - lastUpdatingDate.getTime()) < CACHE_LIVE_TIME;
        }
        return valid;
    }

    public void setContext(WeakReference<Context> context) {
        this.context = context;
    }

    public void setCacheUpdateTime(String className, Date date) {
        cacheDates.put(className, date);
        saveToFile(cacheDates, CACHE_DATES_MAP);
    }

    public boolean isContractPayed(String id) {
        return payedContracts.contains(id);
    }

    public void markContractAsPayed(String id, boolean mark) {
        if (!mark) {
            payedContracts.remove(id);
        } else {
            payedContracts.add(id);
        }
        saveToFile(payedContracts, PAYED_CONTRACTS);
    }

    public boolean hasMarkedSalesCoupons() {
        return markedSalesCoupons.size() != 0;
    }

    public boolean hasMarkedLoanCoupons() {
        return markedLoanCoupons.size() != 0;
    }

    public boolean hasSavedSalesCoupons() {
        return savedSalesCoupons.size() != 0;
    }

    public boolean hasSavedLoanCoupons() {
        return savedLoanCoupons.size() != 0;
    }

    public boolean isSalesCouponMarked(String id) {
        return markedSalesCoupons.contains(id);
    }

    public void markSalesCoupon(String id, boolean mark) {
        if (!mark) {
            markedSalesCoupons.remove(id);
        } else {
            markedSalesCoupons.add(id);
        }
        saveToFile(this.markedSalesCoupons, SALES_COUPONS_MARKED);
    }

    public boolean isLoanCouponMarked(String id) {
        return markedLoanCoupons.contains(id);
    }

    public void markLoanCoupon(String id, boolean mark) {
        if (!mark) {
            markedLoanCoupons.remove(id);
        } else {
            markedLoanCoupons.add(id);
        }
        saveToFile(this.markedLoanCoupons, LOAN_COUPONS_MARKED);
    }

    public boolean isSalesCouponSaved(String id) {
        return savedSalesCoupons.contains(id);
    }

    public void saveSalesCoupon(String id, boolean save) {
        if (save) {
            savedSalesCoupons.add(id);
        } else {
            savedSalesCoupons.remove(id);
        }
        if (markedSalesCoupons.contains(id)) {
            markedSalesCoupons.remove(id);
        }
        saveToFile(this.savedSalesCoupons, SALES_COUPONS_SAVED);
    }

    public boolean isLoanCouponSaved(String id) {
        return savedLoanCoupons.contains(id);
    }

    public void saveLoanCoupon(String id, boolean save) {
        if (save) {
            savedLoanCoupons.add(id);
        } else {
            savedLoanCoupons.remove(id);
        }
        if (markedLoanCoupons.contains(id)) {
            markedLoanCoupons.remove(id);
        }
        saveToFile(this.savedLoanCoupons, LOAN_COUPONS_SAVED);
    }

    public boolean isLoanCouponFixed(String id) {
        return fixLoanCoupons.contains(id);
    }

    public void fixLoanCoupon(String id) {
        fixLoanCoupons.add(id);
        saveToFile(this.fixLoanCoupons, LOAN_COUPONS_FIX);
    }

    public boolean isSalesCouponFixed(String id) {
        return fixSalesCoupons.contains(id);
    }

    public void fixSalesCoupon(String id) {
        fixSalesCoupons.add(id);
        saveToFile(this.fixSalesCoupons, SALES_COUPONS_FIX);
    }

    /**
     * clear all data from local storage. Does'nt clean database of cached object!
     */
    public void clearAll() {
        cacheDates = new HashMap<>();
        savedLoanCoupons = new HashSet<>();
        markedLoanCoupons = new HashSet<>();
        savedSalesCoupons = new HashSet<>();
        markedSalesCoupons = new HashSet<>();
        fixLoanCoupons = new HashSet<>();
        fixSalesCoupons = new HashSet<>();
        payedContracts = new HashSet<>();
        nearestDistances = new HashMap<>();

        saveToFile(cacheDates, CACHE_DATES_MAP);
        saveToFile(savedLoanCoupons, LOAN_COUPONS_SAVED);
        saveToFile(markedLoanCoupons, LOAN_COUPONS_MARKED);
        saveToFile(savedSalesCoupons, SALES_COUPONS_SAVED);
        saveToFile(markedSalesCoupons, SALES_COUPONS_MARKED);
        saveToFile(fixLoanCoupons, LOAN_COUPONS_FIX);
        saveToFile(fixSalesCoupons, SALES_COUPONS_FIX);
        saveToFile(payedContracts, PAYED_CONTRACTS);
        saveToFile(nearestDistances, NEAREST_DISTANCES);
    }

    public void saveAllMarkedSalesCoupons() {
        this.savedSalesCoupons.addAll(this.markedSalesCoupons);
        this.markedSalesCoupons.clear();
        saveToFile(this.savedSalesCoupons, SALES_COUPONS_SAVED);
        saveToFile(this.markedSalesCoupons, SALES_COUPONS_MARKED);
    }

    public void saveAllMarkedLoanCoupons() {
        this.savedLoanCoupons.addAll(this.markedLoanCoupons);
        this.markedLoanCoupons.clear();
        saveToFile(this.savedLoanCoupons, LOAN_COUPONS_SAVED);
        saveToFile(this.markedLoanCoupons, LOAN_COUPONS_MARKED);
    }

    public void setCacheUpdateTime(String className) {
        setCacheUpdateTime(className, new Date());
    }

    public void addNearestDistance(String className, String id, int distance) {
        HashMap<String, Integer> distances;
        if (!nearestDistances.containsKey(className)) {
            distances = new HashMap<>();
        } else {
            distances = nearestDistances.get(className);
        }
        distances.put(id, distance);
        nearestDistances.put(className, distances);
        saveToFile(nearestDistances, NEAREST_DISTANCES);
    }

    /**
     * @param className class type object (Department, ATM, Terminal)
     * @param id of object
     * @return nearest distance to object (Department, atm or terminal)
     */
    public int getNearestDistance(String className, String id) {
        if (nearestDistances.containsKey(className)) {
            HashMap<String, Integer> distances = nearestDistances.get(className);
            if (distances.containsKey(id))
                return distances.get(id);
        }
        return 0;
    }

    /**
     * Save received distance to terminals in local data for further displaying in Payment system fragment.
     *
     * @param terminalResult result contains nearest terminal data
     */
    public void saveNearestTerminalsDistances(Result<NearestTerminal> terminalResult) {
        nearestDistances = new HashMap<>();
        for (int i = 0; i < terminalResult.size(); i++) {
            NearestTerminal terminal = terminalResult.getItem(i);
            addNearestDistance(Terminal.class.getSimpleName(), terminal.getTerminal().getId(), terminal.getDistance());
        }
    }

    /**
     * Save received distance to departments in local data for further displaying in Bank fragment.
     *
     * @param departmentResult result contains nearest department data
     * @see LocalStorage#saveNearestTerminalsDistances(Result)
     */
    public void saveNearestDepartmentsDistances(Result<NearestDepartment> departmentResult) {
        nearestDistances = new HashMap<>();
        for (int i = 0; i < departmentResult.size(); i++) {
            NearestDepartment department = departmentResult.getItem(i);
            if (department.getAtm() != null) {
                addNearestDistance(ATM.class.getSimpleName(), department.getAtm().getId(), department.getDistance());
            } else if (department.getDepartment() != null) {
                addNearestDistance(Department.class.getSimpleName(), department.getDepartment().getId(), department.getDistance());
            }
        }
    }

    /**
     * Invalidate cache for data class
     *
     * @param className data class name
     */
    public void invalidateCache(String className) {
        cacheDates.remove(className);
        saveToFile(cacheDates, CACHE_DATES_MAP);
    }

}
