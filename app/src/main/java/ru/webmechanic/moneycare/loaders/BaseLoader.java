package ru.webmechanic.moneycare.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.io.IOException;

import ru.webmechanic.moneycare.api.ApiFactory;
import ru.webmechanic.moneycare.api.ApiService;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;

/**
 * Created by a.bratusenko on 23.05.16.
 * Базовый класс для выполения Api запросов асинхронно
 */
public abstract class BaseLoader extends AsyncTaskLoader<Response> {

    protected ApiService service;

    public BaseLoader(Context context) {
        super(context);
        this.service = ApiFactory.getApiService();
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public Response loadInBackground() {
        try {
            Response response = apiCall();
            if (response.getRequestResult() == RequestResult.SUCCESS) {
                onSuccess();
            } else {
                onError();
            }

            return response;
        } catch (IOException e) {
            onError();
            return new Response()
                    .setCache(false)
                    .setRequestResult(RequestResult.ERROR);
        }
    }

    protected void onSuccess() {
    }

    protected void onError() {
    }

    protected abstract Response apiCall() throws IOException;
}


