package ru.webmechanic.moneycare.maputils.mapinfoadapters;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.NearestOutlet;
import ru.webmechanic.moneycare.content.Outlet;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.maputils.Balloon;
import ru.webmechanic.moneycare.maputils.MapInfoAdapter;

/**
 * Created by a.bratusenko on 15.06.16.
 * Извлечение данных из списка ближайших точек продаж необходимых для карты
 */
public class MapInfoAdapterNearestOutlets extends MapInfoAdapter {

    private double longitude;
    private double latitude;
    private boolean isOneActive = false;
    private Context context;
    private Result<NearestOutlet> nearestOutlets;

    public MapInfoAdapterNearestOutlets(Context context, double longitude, double latitude, Result<NearestOutlet> nearestOutlets) {
        this.context = context;
        this.longitude = longitude;
        this.latitude = latitude;
        this.nearestOutlets = nearestOutlets;
    }

    @Override
    public double getLongitude() {
        return longitude;
    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    @Override
    public List<Balloon> getBalloons() {
        List<Balloon> balloons = new ArrayList<>();
        for (int i = 0; i < nearestOutlets.size(); i++) {
            Outlet outlet = nearestOutlets.getItem(i).getOutlet();
            Balloon balloon = new Balloon();
            balloon.setTitle(outlet.getOrganization());
            balloon.setPhone(outlet.getPhone());

            balloon.setAddress(outlet.getAddress().getFullAddress());
            try {
                balloon.setLongitude(Double.parseDouble(outlet.getAddress().getLongitude()));
                balloon.setLatitude(Double.parseDouble(outlet.getAddress().getLatitude()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            balloons.add(balloon);
        }
        return balloons;
    }

    @Override
    public boolean isOneActive() {
        return false;
    }

    @Override
    public String getName() {
        return context.getString(R.string.nearest_outlets);
    }
}
