package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.webmechanic.moneycare.LocationManager;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.Bank;
import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.content.NearestDepartment;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.maputils.MapDataProcessor;
import ru.webmechanic.moneycare.maputils.mapinfoadapters.MapInfoAdapterBanks;
import ru.webmechanic.moneycare.maputils.mapinfoadapters.MapInfoAdapterNearestDepartments;
import ru.webmechanic.moneycare.ui.customviews.ContractView;
import ru.webmechanic.moneycare.ui.utils.Arguments;
import ru.webmechanic.moneycare.ui.utils.BanksAdapter;
import ru.webmechanic.moneycare.ui.utils.NearestDepartmentsAdapter;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class BanksFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener {

    @BindView(R.id.banksListView)
    ListView listView;

    @BindView(R.id.switchNearestAllBanksButton)
    Button switchNearestButton;

    @BindView(R.id.banksContractView)
    ContractView contractView;

    @BindView(R.id.noDataTextView)
    TextView noDataTextView;

    private Result<Bank> bankResult;
    private Result<NearestDepartment> nearestDepartmentResult;

    public BanksFragment() {
        // Required empty public constructor
    }

    @Override
    protected void fetchData() {
        isLoading = true;
        if (loaderContext.getLoader() == R.id.nearest_departments_loader && settings.isInternetAvailable()) {
            new DataFetcher(this, loaderContext, this).fetchFromWeb();
        } else {
            new DataFetcher(this, loaderContext, this).fetch();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_banks, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.banks);
    }

    private void setNearestDepartmentsAdapter(Result<NearestDepartment> nearestDepartmentResult) {
        NearestDepartmentsAdapter nearestDepartmentsAdapter = new NearestDepartmentsAdapter(getActivity(), nearestDepartmentResult);
        final Result result = new DataFetcher(this, NearestDepartment.class).fetchFromCache();
        nearestDepartmentsAdapter.setListener(new NearestDepartmentsAdapter.NearestDepartmentsAdapterListener() {
            @Override
            public void onNeedShowMapForObject(NearestDepartment nearestDepartment) {
                gotoFragment(FragmentFactory.MAP,
                        MapDataProcessor.prepareData(
                                getArguments(),
                                new MapInfoAdapterNearestDepartments(
                                        getActivity(),
                                        nearestDepartment,
                                        result
                                )));
            }
        });
        listView.setAdapter(nearestDepartmentsAdapter);
    }

    private void setBankAdapter(Result<Bank> resultBanks) {
        listView.setAdapter(new BanksAdapter(this, resultBanks));
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        new LocationManager(getActivity()).requestBestLocation();
        if (arguments.isSetContract()) {
            Contract contract = (Contract) new DataFetcher(this, Contract.class).one(arguments.getContract()).fetchFromCache().first();
            contractView.setContract(contract);
            contractView.setVisibility(View.VISIBLE);
        }
        if (getArguments().containsKey(Arguments.ARG_LOADER)) {
            if (getArguments().getInt(Arguments.ARG_LOADER) == R.id.banks_loader) {
                switchNearestButton.setText(getString(R.string.nearest));
            } else {
                switchNearestButton.setText(getString(R.string.all));
            }
        }

        if (isLoading) {
            return;
        }
        if (nearestDepartmentResult != null && nearestDepartmentResult.size() != 0) {
            noDataTextView.setVisibility(View.GONE);
            settings.getStorage().saveNearestDepartmentsDistances(nearestDepartmentResult);
            setNearestDepartmentsAdapter(nearestDepartmentResult);
        } else if (bankResult != null && bankResult.size() != 0) {
            noDataTextView.setVisibility(View.GONE);
            setBankAdapter(bankResult);
        } else {
            if (loaderContext.getLoader() == R.id.banks_loader) {
                noDataTextView.setText(R.string.no_data_banks);
            } else {
                noDataTextView.setText(R.string.no_data_nearestdepartments);
            }
            noDataTextView.setVisibility(View.VISIBLE);
            listView.setAdapter(null);
        }
    }


    @OnClick(R.id.switchNearestAllBanksButton)
    void switchNearestAll() {

        if (getArguments() == null) {
            setArguments(new Bundle());
        }
        if (loaderContext.getLoader() == R.id.banks_loader) {
            loaderContext.setLoader(R.id.nearest_departments_loader);
        } else if (loaderContext.getLoader() == R.id.nearest_departments_loader) {
            loaderContext.setLoader(R.id.banks_loader);
        }

        fetchData();
        updateUI();
    }

    @OnClick(R.id.mapBanksButton)
    void showMap() {
        if (switchNearestButton.getText().toString().equals(getString(R.string.all))) {
            Result result = new DataFetcher(this, NearestDepartment.class).fetchFromCache();
            gotoFragment(FragmentFactory.MAP,
                    MapDataProcessor.prepareData(
                            getArguments(),
                            new MapInfoAdapterNearestDepartments(
                                    getActivity(),
                                    Double.parseDouble(settings.getLongitude()),
                                    Double.parseDouble(settings.getLatitude()),
                                    result)));
        } else {
            Result result = new DataFetcher(this, Bank.class).fetchFromCache();
            gotoFragment(FragmentFactory.MAP,
                    MapDataProcessor.prepareData(
                            getArguments(),
                            new MapInfoAdapterBanks(
                                    getActivity(),
                                    Double.parseDouble(settings.getLongitude()),
                                    Double.parseDouble(settings.getLatitude()),
                                    result)));
        }
    }


    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        nearestDepartmentResult = null;
        bankResult = null;
        if (loaderid == R.id.nearest_departments_loader) {
            try {
                nearestDepartmentResult = (Result<NearestDepartment>) result;
            } catch (Exception e) {
            }

        } else {
            try {
                bankResult = (Result<Bank>) result;
            } catch (Exception e) {

            }

        }
        isLoading = false;
        updateUI();

    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }
}
