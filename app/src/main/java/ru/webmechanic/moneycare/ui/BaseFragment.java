package ru.webmechanic.moneycare.ui;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import butterknife.BindView;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.loaders.LoaderContext;
import ru.webmechanic.moneycare.ui.utils.Arguments;

/**
 * Created by a.bratusenko on 31.05.16.
 * Базовый класс для всех фрагментов
 */
public abstract class BaseFragment extends Fragment {

    protected final int MSG_BACK = 0;
    protected final int MSG_GOTO = 1;
    protected final String FRAGMENT_NAME = "fragment name";

    @Nullable
    @BindView(R.id.swipeToRefresh)
    protected SwipeRefreshLayout swipeToRefresh = null;
    @Nullable
    @BindView(R.id.noInternetWarningTextView)
    protected TextView noInternetTextView = null;

    /**
     * Флаг установлен в истину, если осуществляется загрузка из сети/бд
     */
    protected boolean isLoading;
    protected Settings settings = Settings.getInstance();

    public Arguments getArgs() {
        return arguments;
    }

    protected Arguments arguments;
    protected LoaderContext loaderContext;
    private OnLoaderFragmentFinishedListener onLoaderFragmentFinishedListener = null;

    protected Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MSG_BACK) {
                if (onLoaderFragmentFinishedListener != null) {
                    onLoaderFragmentFinishedListener.onLoaderFragmentFinishedAndGoBack();
                }

            } else if (msg.what == MSG_GOTO) {
                String fragmentName = msg.getData().getString(FRAGMENT_NAME);
                msg.getData().remove(FRAGMENT_NAME);
                if (onLoaderFragmentFinishedListener != null) {
                    onLoaderFragmentFinishedListener.onLoaderFragmentFinishedAndGoTo(fragmentName, msg.getData());
                }
            }
        }
    };

    public BaseFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            arguments = new Arguments(savedInstanceState);
        } else {
            arguments = new Arguments(getArguments());
        }
        settings = Settings.getInstance(getActivity());
        loaderContext = new LoaderContext(getActivity(), arguments);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        arguments.fillBundle(outState);
    }

    protected void updateUI() {
        if (swipeToRefresh != null) {
            swipeToRefresh.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        swipeToRefresh.setRefreshing(isLoading);
                    } catch (Exception e) {
                    }
                }
            });
        }
        if (noInternetTextView != null) {
            int visibility = settings.isInternetAvailable() ? View.GONE : View.VISIBLE;
            try {
                noInternetTextView.setVisibility(visibility);
            } catch (Exception e) {
            }
        }
    }

    protected abstract void fetchData();

    @Override
    public void onResume() {
        super.onResume();
        if (swipeToRefresh != null) {
            swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    settings.getStorage().invalidateCache(loaderContext.getDataClass().getSimpleName());
                    fetchData();
                }
            });
        }
        fetchData();
        updateUI();
    }

    public void setOnLoaderFragmentFinishedListener(OnLoaderFragmentFinishedListener onLoaderFragmentFinishedListener) {
        this.onLoaderFragmentFinishedListener = onLoaderFragmentFinishedListener;
    }

    public void gotoFragment(String fragmentName) {
        hideKeyboard();
        gotoFragment(fragmentName, new Arguments());
    }

    public void gotoFragment(String fragmentName, @NonNull Bundle args) {
        hideKeyboard();
        args.putString(FRAGMENT_NAME, fragmentName);
        Message message = new Message();
        message.what = MSG_GOTO;
        message.setData(args);
        handler.sendMessage(message);
    }

    public void gotoFragment(String fragmentName, @NonNull Arguments args) {
        hideKeyboard();
        Bundle bundle = args.getBundle();
        bundle.putString(FRAGMENT_NAME, fragmentName);
        Message message = new Message();
        message.what = MSG_GOTO;
        message.setData(bundle);
        handler.sendMessage(message);
    }

    protected void hideKeyboard() {

        try {
            View view = getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {

        }
    }

    public void gotoBack() {
        handler.sendEmptyMessage(MSG_BACK);
    }

    public abstract String getTitle();

    public interface OnLoaderFragmentFinishedListener {
        void onLoaderFragmentFinishedAndGoTo(String goToFragmentName, Bundle args);

        void onLoaderFragmentFinishedAndGoBack();
    }


}

