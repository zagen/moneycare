package ru.webmechanic.moneycare.api.deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;

import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.content.Goods;

/**
 * Created by a.bratusenko on 27.05.16.
 * Класс для десериализации класса договора.
 * Если отсутствует сумма договора, она считается по сложению всех сумм отдельных товаров, принадлежащих этому договору
 */
public class ContractDeserializer implements JsonDeserializer<Contract> {
    @Override
    public Contract deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Contract contract = new Contract();
        JsonObject jsonContract = (JsonObject) json;
        try {
            contract.setAmount(jsonContract.get("amount").getAsDouble());
        } catch (Exception e) {
            contract.setAmount(0);
        }
        try {
            contract.setGoods(new GoodsDeserializer().deserialize(jsonContract.get("goods"), typeOfT, context));
        } catch (Exception e) {
            contract.setGoods(null);
        }
        try {
            contract.setId(jsonContract.get("contract_num").getAsString());
        } catch (Exception e) {
            contract.setId("");
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            contract.setCreatingDate(sdf.parse(jsonContract.get("begin_date").getAsString()));
        } catch (Exception e) {
            contract.setCreatingDate(null);
        }
        try {
            contract.setEndDate(sdf.parse(jsonContract.get("end_date").getAsString()));
        } catch (Exception e) {
            contract.setEndDate(null);
        }

        if (contract.getAmount() == 0 && contract.getGoods() != null) {

            for (Goods good : contract.getGoods()) {
                double amount = good.getAmount();
                contract.setAmount(contract.getAmount() + amount);
            }
        }

        return contract;
    }
}
