package ru.webmechanic.moneycare.ui.utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.content.LoanCoupon;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.ui.customviews.LoanCouponItemView;

/**
 * Created by a.bratusenko on 01.06.16.
 */
public class LoanCouponsAdapter extends BaseAdapter {
    protected Settings settings;
    private Result<LoanCoupon> loanCoupons;
    private LayoutInflater inflater;

    public LoanCouponsAdapter(Context context, Result<LoanCoupon> result) {
        this.loanCoupons = result;
        this.inflater = ((Activity) context).getLayoutInflater();
        this.settings = Settings.getInstance(context);
    }

    @Override
    public int getCount() {
        return loanCoupons.size();
    }

    @Override
    public Object getItem(int position) {
        return loanCoupons.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_loan_coupon_layout, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        LoanCoupon coupon = this.loanCoupons.getItem(position);

        viewHolder.loanCouponItemView.setLoanCoupon(coupon);
        final String couponId = coupon.getId();
        viewHolder.loanCouponItemView.setFlagVisible(true);

        updateSticker(viewHolder.loanCouponItemView.getSticker(), couponId);

        viewHolder.loanCouponItemView.getSticker().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!settings.getStorage().isLoanCouponSaved(couponId)) {
                    settings.getStorage().markLoanCoupon(couponId, !settings.getStorage().isLoanCouponMarked(couponId));
                }
                updateSticker((ImageButton) v, couponId);
            }
        });

        return convertView;
    }

    private void updateSticker(ImageButton stickerImageButton, String id) {
        int stickerDrawableId = R.drawable.sticker;
        if (settings.getStorage().isLoanCouponSaved(id)) {
            stickerDrawableId = R.drawable.sticker_saved;
        } else if (settings.getStorage().isLoanCouponMarked(id)) {
            stickerDrawableId = R.drawable.sticker_marked;
        }
        stickerImageButton.setImageResource(stickerDrawableId);
    }

    static class ViewHolder {
        @BindView(R.id.loanCouponView)
        LoanCouponItemView loanCouponItemView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
