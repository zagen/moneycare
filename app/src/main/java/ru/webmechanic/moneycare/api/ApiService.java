package ru.webmechanic.moneycare.api;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import ru.webmechanic.moneycare.content.Bank;
import ru.webmechanic.moneycare.content.Category;
import ru.webmechanic.moneycare.content.City;
import ru.webmechanic.moneycare.content.Contract;
import ru.webmechanic.moneycare.content.LoanCoupon;
import ru.webmechanic.moneycare.content.LocalData.LoanCouponFixData;
import ru.webmechanic.moneycare.content.LocalData.SalesCouponFixData;
import ru.webmechanic.moneycare.content.NearestDepartment;
import ru.webmechanic.moneycare.content.NearestOutlet;
import ru.webmechanic.moneycare.content.NearestTerminal;
import ru.webmechanic.moneycare.content.Outlet;
import ru.webmechanic.moneycare.content.PaymentSystem;
import ru.webmechanic.moneycare.content.SalesCoupon;
import ru.webmechanic.moneycare.content.SubjectType;
import ru.webmechanic.moneycare.content.User;

/**
 * Created by a.bratusenko on 23.05.16.
 * Интерфейс, включающий в себя все API points
 */
public interface ApiService {

    @POST("client/login")
    Call<User> login(@Query("phone") String phone, @Query("password") String password);

    @POST("client/create/")
    Call<User> createUser(@Query("phone") String phone, @Query("password") String password);

    @POST("client/pin/")
    Call<User> confirmPin(@Query("phone") String phone, @Query("token") String token, @Query("pin") String pin);

    @POST("client/new-pin/")
    Call<User> resendPin(@Query("phone") String phone, @Query("token") String token);

    @POST("client/contracts/")
    Call<List<Contract>> contracts(@Query("phone") String phone, @Query("token") String token);

    @GET("city/active-cities/")
    Call<List<City>> activeCities();

    @GET("city")
    Call<City> city(@Query("latitude") String latitude, @Query("longitude") String longitude);

    @GET("sales_coupon")
    Call<List<SalesCoupon>> salesCoupons(@Query("city") String city, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("phone") String phone, @Query("token") String token);


    @POST("sales_coupon/fix/")
    Call<SalesCouponFixData> fixSalesCoupon(@Query("id") String id, @Query("idexternal") String idexternal, @Query("phone") String phone, @Query("token") String token);

    @POST("loan_coupon/fix/")
    Call<LoanCouponFixData> fixLoanCoupon(@Query("id") String id, @Query("idexternal") String idexternal, @Query("phone") String phone, @Query("token") String token);

    @GET("loan_coupon/")
    Call<List<LoanCoupon>> loanCoupons(@Query("city") String city, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("phone") String phone, @Query("token") String token);

    @GET("outlet/nearest")
    Call<List<NearestOutlet>> outletsNearest(@Query("city") String city, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("phone") String phone, @Query("token") String token);

    @GET("outlet/item/{id}")
    Call<Outlet> outlet(@Path("id") String urlId, @Query("city") String city, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("phone") String phone, @Query("token") String token);

    @GET("outlet")
    Call<List<Outlet>> outlets(@Query("city") String city, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("phone") String phone, @Query("token") String token);

    @GET("bank")
    Call<List<Bank>> banks(@Query("city") String city, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("phone") String phone, @Query("token") String token);

    @GET("bank/item/{id}")
    Call<Bank> bank(@Path("id") String urlId, @Query("city") String city, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("phone") String phone, @Query("token") String token);

    @GET("bank/nearest")
    Call<List<NearestDepartment>> nearestDepartments(@Query("city") String city, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("phone") String phone, @Query("token") String token);

    @GET("payment_system/")
    Call<List<PaymentSystem>> paymentSystems(@Query("city") String city, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("phone") String phone, @Query("token") String token);

    @GET("payment_system/item/{id}")
    Call<PaymentSystem> paymentSystem(@Path("id") String urlId, @Query("city") String city, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("phone") String phone, @Query("token") String token);

    @GET("payment_system/nearest")
    Call<List<NearestTerminal>> nearestTerminals(@Query("city") String city, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("phone") String phone, @Query("token") String token);

    @GET("subject/categories")
    Call<List<Category>> categories(@Query("city") String city, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("phone") String phone, @Query("token") String token);

    @GET("subject/subject-type-search")
    Call<List<SubjectType>> searchType(@Query("search") String search, @Query("city") String city, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("phone") String phone, @Query("token") String token);


}

