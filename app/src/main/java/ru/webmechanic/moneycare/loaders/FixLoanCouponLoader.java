package ru.webmechanic.moneycare.loaders;

import android.content.Context;
import android.os.Bundle;

import java.io.IOException;

import retrofit.Call;
import ru.webmechanic.moneycare.Settings;
import ru.webmechanic.moneycare.api.response.RequestResult;
import ru.webmechanic.moneycare.api.response.Response;
import ru.webmechanic.moneycare.content.LocalData.LoanCouponFixData;
import ru.webmechanic.moneycare.ui.utils.Arguments;

/**
 * Created by a.bratusenko on 03.06.16.
 */
public class FixLoanCouponLoader extends BaseLoader {

    private String id = null;
    private String idexternal = null;
    private String phone = null;
    private String token = null;
    private Settings settings;

    public FixLoanCouponLoader(Context context, Bundle args) {
        super(context);
        this.settings = Settings.getInstance(context);
        if (args != null
                && args.containsKey(Arguments.ARG_ID)
                && args.containsKey(Arguments.ARG_IDEXTERNAL)) {
            this.id = args.getString(Arguments.ARG_ID);
            this.idexternal = args.getString(Arguments.ARG_IDEXTERNAL);


        }
        String[] params = settings.prepareParams();
        phone = params[3];
        token = params[4];
    }

    @Override
    protected Response apiCall() throws IOException {
        LoanCouponFixData fixLocalData = null;
        Response response = new Response();

        Call<LoanCouponFixData> call = service.fixLoanCoupon(id, idexternal, phone, token);
        retrofit.Response<LoanCouponFixData> retrofitResponse = call.execute();

        if (retrofitResponse != null) {
            if (!retrofitResponse.isSuccess()) {
                response.setCode(retrofitResponse.code());
                if (retrofitResponse.errorBody() != null) {
                    response.setErrorMessage(retrofitResponse.message());
                }
            } else {
                fixLocalData = retrofitResponse.body();
            }
        }


        //Полученные данные в случае успешного запроса необходимо сохранить в локальном
        //хранилище на случай если пользователь уже покинул фрагмент откуда был осуществлен запрос.
        if (fixLocalData != null) {
            settings.getStorage().fixLoanCoupon(fixLocalData.getCouponId());
            settings.getStorage().saveLoanCoupon(fixLocalData.getCouponId(), false);
            settings.getStorage().markLoanCoupon(fixLocalData.getCouponId(), false);
        }
        return response.setCache(false)
                .setRequestResult(RequestResult.SUCCESS)
                .setAnswer(fixLocalData);

    }
}
