package ru.webmechanic.moneycare.ui;


import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import ru.webmechanic.moneycare.R;
import ru.webmechanic.moneycare.content.LoanCoupon;
import ru.webmechanic.moneycare.content.SalesCoupon;
import ru.webmechanic.moneycare.content.SubjectType;
import ru.webmechanic.moneycare.database.Result;
import ru.webmechanic.moneycare.loaders.DataFetcher;
import ru.webmechanic.moneycare.ui.customviews.SearchAutocompleteView;
import ru.webmechanic.moneycare.ui.utils.Arguments;
import ru.webmechanic.moneycare.ui.utils.SearchResultsAdapter;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class SearchFragment extends BaseFragment implements DataFetcher.OnDataFetchedListener {

    private final static String SUBJECT_TITLE = "subject title";

    @BindView(R.id.searchEditText)
    EditText searchEdittext;

    @BindView(R.id.searchResultsList)
    ListView resultsListView;

    @BindView(R.id.noDataTextView)
    TextView noDataTextView;

    @BindView(R.id.searchAutocomplete)
    SearchAutocompleteView autocompleteList;

    private Result<SalesCoupon> salesCouponResult;
    private Result<LoanCoupon> loanCouponResult;
    private Result<SubjectType> subjectTypeResult;

    private String subjectTitle = "";

    private TextWatcher editSearchQueryWatcher = new TextWatcher() {
        private final static int TIME_BETWEEN_LOAD = 300;
        private final Handler handler = new Handler();
        private long lastTimeChanging = 0;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            handler.removeCallbacksAndMessages(null);
            subjectTitle = s.toString();
            arguments.setQuery(subjectTitle);
            loaderContext.setLoader(R.id.subject_search_loader);
            long currentTime = System.currentTimeMillis();
            if (currentTime - lastTimeChanging > TIME_BETWEEN_LOAD) {
                lastTimeChanging = currentTime;
                fetchData();
            } else {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fetchData();
                    }
                }, TIME_BETWEEN_LOAD);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onPause() {
        searchEdittext.removeTextChangedListener(editSearchQueryWatcher);
        super.onPause();
    }

    @Override
    protected void fetchData() {
        isLoading = true;
        if (loaderContext.getLoader() == R.id.subject_search_loader) {
            if (settings.isInternetAvailable()) {
                new DataFetcher(this, loaderContext, this).containes("title", subjectTitle).fetchFromWeb();
            } else {
                new DataFetcher(this, loaderContext, this).containes("title", subjectTitle).fetch();
            }
        } else {
            new DataFetcher(this, loaderContext, this).containes("subjectType", subjectTitle).fetch();
        }
    }

    @Override
    public void onResume() {
        searchEdittext.addTextChangedListener(editSearchQueryWatcher);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onResume();
    }

    @Override
    public String getTitle() {
        return settings.getContext().getString(R.string.search_by_goods);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(SUBJECT_TITLE, subjectTitle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);

        if (savedInstanceState != null) {
            subjectTitle = savedInstanceState.getString(SUBJECT_TITLE);
            searchEdittext.setText(subjectTitle);
        }

        autocompleteList.setOnSearchItemClickListener(new SearchAutocompleteView.OnSearchItemClickListener() {
            @Override
            public void onSearchItemClick(String value) {
                hideKeyboard();
                subjectTitle = value;
                searchEdittext.removeTextChangedListener(editSearchQueryWatcher);
                searchEdittext.setText(subjectTitle);
                searchEdittext.addTextChangedListener(editSearchQueryWatcher);
                autocompleteList.setVisible(false);
                loaderContext.setLoader(R.id.sales_coupons_loader);
                fetchData();
                updateUI();
            }
        });

        return view;
    }

    @OnClick(R.id.searchLoupeButton)
    void loupeClick() {
        hideKeyboard();
        subjectTitle = searchEdittext.getText().toString();
        arguments.setQuery(subjectTitle);

        if (autocompleteList.isVisible()) {
            autocompleteList.setVisible(false);
            loaderContext.setLoader(R.id.sales_coupons_loader);
            fetchData();
            updateUI();
        } else {
            loaderContext.setLoader(R.id.subject_search_loader);
            fetchData();
            updateUI();
        }
    }

    @OnTouch(R.id.searchEditText)
    boolean search() {
        loaderContext.setLoader(R.id.subject_search_loader);
        fetchData();
        return false;
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        if (subjectTypeResult != null) {
            noDataTextView.setVisibility(View.GONE);
            resultsListView.setVisibility(View.GONE);
            autocompleteList.setResult(subjectTypeResult);
            autocompleteList.setVisible(true);
        } else if (salesCouponResult != null && loanCouponResult != null) {
            boolean noOneFound = salesCouponResult.size() == 0;
            noOneFound = noOneFound && loanCouponResult.size() == 0;
            if (noOneFound) {
                noDataTextView.setVisibility(View.VISIBLE);
            } else {
                noDataTextView.setVisibility(View.GONE);
                autocompleteList.setVisible(false);
                SearchResultsAdapter adapter = new SearchResultsAdapter(getActivity(), salesCouponResult, loanCouponResult);
                adapter.setListener(new SearchResultsAdapter.OnSearchResultsClickListener() {
                    @Override
                    public void onSalesCouponClick(String id) {
                        Arguments args = new Arguments();
                        args.setId(id);
                        gotoFragment(FragmentFactory.SALES_COUPON, args);
                    }

                    @Override
                    public void onLoanCouponClick(String id) {
                        Arguments args = new Arguments();
                        args.setId(id);
                        gotoFragment(FragmentFactory.LOAN_COUPON, args);
                    }
                });
                resultsListView.setAdapter(adapter);
                resultsListView.setVisibility(View.VISIBLE);
            }
        }
    }


    @Override
    public <T> void onDataFetched(int loaderid, Result<T> result) {
        if (loaderid == R.id.loan_coupons_loader) {
            loaderContext.setLoader(R.id.sales_coupons_loader);
            try {
                this.loanCouponResult = (Result<LoanCoupon>) result;
            } catch (Exception e) {

            }
            subjectTypeResult = null;
        } else if (loaderid == R.id.sales_coupons_loader) {
            loaderContext.setLoader(R.id.loan_coupons_loader);
            try {
                salesCouponResult = (Result<SalesCoupon>) result;
            } catch (Exception e) {
            }

            fetchData();
        } else {
            try {
                subjectTypeResult = (Result<SubjectType>) result;
            } catch (Exception e) {
            }

        }
        isLoading = false;
        updateUI();

    }

    @Override
    public void onDataFetchedError(int code, String message) {
        isLoading = false;
        updateUI();
        Toast.makeText(getActivity(), R.string.error_getdata_network, Toast.LENGTH_SHORT).show();
    }
}
