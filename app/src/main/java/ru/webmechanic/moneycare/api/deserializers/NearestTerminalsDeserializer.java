package ru.webmechanic.moneycare.api.deserializers;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ru.webmechanic.moneycare.content.NearestTerminal;
import ru.webmechanic.moneycare.content.Terminal;

/**
 * Created by a.bratusenko on 27.05.16.
 * Класс, осуществляющий десериализацию ближайших терминалов оплаты платежных систем.
 */
public class NearestTerminalsDeserializer implements JsonDeserializer<List<NearestTerminal>> {
    @Override
    public List<NearestTerminal> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<NearestTerminal> terminals = new ArrayList<>();

        JsonArray jsonTerminals = ((JsonObject) json).get("pss").getAsJsonArray();
        TerminalDeserializer terminalDeserializer = new TerminalDeserializer();

        for (int i = 0; i < jsonTerminals.size(); i++) {
            NearestTerminal nearestTerminal = new NearestTerminal();
            Terminal terminal;
            try {
                terminal = terminalDeserializer.deserialize(jsonTerminals.get(i).getAsJsonObject(), typeOfT, context);
            } catch (Exception e) {
                terminal = null;
            }
            nearestTerminal.setTerminal(terminal);
            int distance;
            try {
                distance = jsonTerminals.get(i).getAsJsonObject().get("distance").getAsInt();
            } catch (Exception e) {
                distance = 0;
            }
            nearestTerminal.setDistance(distance);
            String psTitle;
            try {
                psTitle = jsonTerminals.get(i).getAsJsonObject().get("payment_system").getAsJsonObject().get("title").getAsString();
            } catch (Exception e) {
                psTitle = null;
            }
            nearestTerminal.setPsTitle(psTitle);
            terminals.add(nearestTerminal);
        }
        return terminals;
    }
}
